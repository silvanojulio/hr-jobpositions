# Compiling client
FROM node:carbon AS base
WORKDIR /app

# ---- Client Dependencies ----
COPY client/package*.json ./

# ---- Now installing client app dependencies ---------
RUN npm install

# ---- Copy Files/Build ----
FROM base AS client-build
WORKDIR /app
COPY client ./
# Build front-end static files
RUN npm run build

# --- Release with Alpine ----
FROM node:12.18.2-alpine AS release
WORKDIR /app
COPY server/package*.json ./

RUN npm install

COPY server ./

COPY --from=client-build /app/build ./dist/public

RUN npm run build

# start app
CMD ["npm", "start"]
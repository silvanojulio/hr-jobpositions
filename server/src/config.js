import { env } from "process";

const roles = {
    superAdmin: {id: 'superAdmin', label: 'Super Admin'},
    companyAdmin: {id: 'companyAdmin', label: 'Administrador'},
    employee: {id: 'employee', label: 'Colaborador'}
};

var config = {
    server:{
        port: process.env.PORT || 8080,
    },
    appSession:{
        hours: process.env.HHRR_APP_SESSION_HOURS || 24,
        secretKey: process.env.HHRR_APP_JWT_KEY || "HHRR2020",
        passwordSaltRounds: 10
    },
    database: {
        mongoUrl: process.env.HHRR_MONGO_STRING || "mongodb+srv://hr-job-positions:g77nOYJqvyKXJhpa@hr-job-positions.lrver.gcp.mongodb.net?retryWrites=true&w=majority",
        name: process.env.HHRR_MONGO_DATABASE || "HHRR_DB_Dev",
    },
    userAdmin:{
        email: process.env.HHRR_USER_ADMIN_EMAIL || "admin@admin.com",
        password: process.env.HHRR_USER_ADMIN_PASSWORD || "Admin2020",
    },
    publicConfig:{
        timezone: process.env.HHRR_TIMEZONE || -3,
        companyName: process.env.HHRR_COMPANY_NAME || 'Gretel'
    },
    roles,
    userMenu:[
        {id: 'HOME', label:'Home', icon: '', roles: [roles.companyAdmin.id, roles.employee.id]},
        {id: 'EMPLOYEES', label: 'Empleados', icon: '', roles: [roles.companyAdmin.id]},
        {id: 'POSITIONS', label: 'Puestos', icon: '', roles: [roles.companyAdmin.id]},
        {id: 'PERFORMANCE', label: 'Desempeño', icon: '', roles: [roles.companyAdmin.id]},
        {id: 'USERS', label: 'Usuarios', icon: '', roles: [roles.companyAdmin.id]},
        {id: 'COMPANIES', label: 'Empresas', icon: ''},
    ]
}

export default config;
import { Request } from 'express';
import AppSession  from '../entities/appSession';
import { AppUser } from '../entities/appUser';
const contextService = require('request-context');

export default class ContextService{

    public static getCurrentRequest(): Request{
        return contextService.get('CURRENT_REQUEST') && contextService.get('CURRENT_REQUEST').request;
    }

    public static getHeaderValue(headerName: string):any{

        const request = this.getCurrentRequest();
        return request.headers[headerName];
    }

    public static getCurrentSession(): AppSession {
        const appSession = contextService.get('CURRENT_REQUEST') && contextService.get('CURRENT_REQUEST').appSession;
        return  (appSession as AppSession);
    }

    public static getCurrentUser(): AppUser {
        return this.getCurrentSession().appUser;
    }
}
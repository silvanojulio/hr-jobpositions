import * as dotenv from "dotenv";
dotenv.config();
import express, { Application, NextFunction, Response, Request } from 'express';
import SessionRouter from './routers/sessionRouter';
import UserRouter from './routers/userRouter';
import CompanyRouter from './routers/companyRouter';
import JobPositionRouter from './routers/jobPositionRouter';
import AssignmentRouter from './routers/assignmentRouter';
import DocumentsRouter from './routers/documentsRouter'
import { ContextRouter } from './routers/contextRouter'
import cors from "cors";
import helmet from "helmet";
import path from 'path';
import config from './config';
import SecurityMiddleware from "./middlewares/securityMiddleware";
import errorHandler from "./middlewares/appErrorMiddleware";
import performanceReviewRouter from "./routers/performanceReviewRouter";
const contextService = require('request-context');

const app: Application = express();

//--React--
app.use(express.static(path.join(__dirname, 'public')));
app.get('/', (req: Request, res: Response, next: NextFunction)=>{
    //console.log('>>> Sending index.html')
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.use(helmet());
app.use(cors());
app.use(express.json());

app.use(contextService.middleware('CURRENT_REQUEST'));
app.use((req:Request, res:Response, next:NextFunction)=>{
    contextService.set('CURRENT_REQUEST:request', req);
    next();
});

//--React--
app.use(express.static(path.join(__dirname, 'public')));
app.get('/', (req: Request, res: Response, next: NextFunction)=>{
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
}); 

//--Public Endpoints
app.use('/api/session', SessionRouter);

//Private Endopints
app.use(SecurityMiddleware);

//--Private Endpoints
app.use('/api/current-session', SessionRouter);
app.use('/api/users', UserRouter);
app.use('/api/companies', CompanyRouter);
app.use('/api/jobpositions', JobPositionRouter);
app.use('/api/assignments', AssignmentRouter);
app.use('/api/performance', performanceReviewRouter);
app.use('/api/documents', DocumentsRouter);
app.use('/api/contexts', ContextRouter);

//ErrorHendler Endpoint
app.use(errorHandler);

console.clear()
app.listen(process.env.PORT || 8080, ()=> console.log('Server running on '+config.server.port))
import { NextFunction, Response, Request } from "express";
import {validationResult} from 'express-validator';
import {AppError} from "./appErrorMiddleware"

const SchemeValidatorMiddleware = (req: Request, res: Response, next: NextFunction)=>{
    try {
        
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return next(AppError.SchemaValidationFailed.setExtraData({errors:errors.array()}))
        }
        next();
    } catch (error) {
        return res.status(410).send(error);
    }
}

export default SchemeValidatorMiddleware;
import { MongoEntityBase } from "../db/mongoEntityBase";

export class AppAssignment extends MongoEntityBase{
    constructor(
        public jobPositionID:string,
        public employeeID:string,
        public assignmentDate:Date,
        public suggestedReviewDate:Date,
        public employeeEvaluatorID:string,
        public inactiveDate:Date,
        public active:Boolean=true
    ){
        super();
    }
}
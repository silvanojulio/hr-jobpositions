import { MongoEntityBase } from "../db/mongoEntityBase";
import { EnumEntityBase } from "./appEnumEntityBase";

export class AppPerformanceReview extends MongoEntityBase{
    constructor(
        public employeeID:string ,
        public evaluatorID:string ,
        public jobPositionID:string ,
        public companyID:string ,
        public date:Date,
        public period:string,
        public result:AppPerformanceReviewResult,
        public resultComment?:string
    ){
        super();
    }
}

export class AppPerformanceReviewResult extends EnumEntityBase{
    public static NotEvaluated:AppPerformanceReviewResult=new AppPerformanceReviewResult(0, "Not Evaluated");
    public static CompletedByEmployee:AppPerformanceReviewResult=new AppPerformanceReviewResult(1, "Completed by Employee");
    public static Unsatisfactory:AppPerformanceReviewResult=new AppPerformanceReviewResult(2, "Unsatisfactory");
    public static NeedsImprovements:AppPerformanceReviewResult=new AppPerformanceReviewResult(3, "Needs Improvements");
    public static MeetsExpectations:AppPerformanceReviewResult=new AppPerformanceReviewResult(4, "Meets Expectations");
    public static ExceedsExpectations:AppPerformanceReviewResult=new AppPerformanceReviewResult(5, "Exceeds Expectations");
    public static Outstanding:AppPerformanceReviewResult=new AppPerformanceReviewResult(6, "Outstanding");
    
    public static getAll = ():Array<AppPerformanceReviewResult> =>new Array(
        AppPerformanceReviewResult.NotEvaluated,
        AppPerformanceReviewResult.CompletedByEmployee,
        AppPerformanceReviewResult.Unsatisfactory,
        AppPerformanceReviewResult.NeedsImprovements,
        AppPerformanceReviewResult.MeetsExpectations,
        AppPerformanceReviewResult.ExceedsExpectations,
        AppPerformanceReviewResult.Outstanding
    );
    
    public static getById = (_id:number):AppPerformanceReviewResult|null => AppPerformanceReviewResult.getAll().find(x=>x.id === _id)??null;

    public isEqual = (priodicity:AppPerformanceReviewResult|null):boolean => priodicity!==null && priodicity.id===this.id;
}
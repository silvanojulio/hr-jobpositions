import { MongoEntityBase } from "../db/mongoEntityBase";
import { EnumEntityBase } from "./appEnumEntityBase";

export class AppJobPositionResponsibility extends MongoEntityBase{
    constructor(
        public appJobPositionID:string|  null,
        public companyID:string ,
        public details:string,
        public subProcess:string,
        public isHomeOffice:Boolean,
        public controlledBy:string,
        public weighing:number,
        public risk:AppJobPositionResponsibilityRisk|null,
        public referenceComments:string="",
        public periodicity:AppJobPositionResponsibilityPeriodicity|null,
        public indicator:string="",
        public documentIDs:string[]

    ){
        super();
    }
}

export class AppJobPositionResponsibilityPeriodicity extends EnumEntityBase{
    public static Other=new AppJobPositionResponsibilityPeriodicity(0,"Otra",0);
    public static Daily=new AppJobPositionResponsibilityPeriodicity(1,"Diaria",1);
    public static Weekly=new AppJobPositionResponsibilityPeriodicity(2,"Semanal",7);
    public static Monthly=new AppJobPositionResponsibilityPeriodicity(3,"Mensual",30);
    public static Quarterly=new AppJobPositionResponsibilityPeriodicity(4,"Trimestral",90);
    public static Biannual=new AppJobPositionResponsibilityPeriodicity(5,"Semestral",180);
    public static Yearly=new AppJobPositionResponsibilityPeriodicity(6,"Anual",360);
    constructor(public id:number, public description: string, public days?:number){
        super(id,description)
    }
    public static getAll = ():Array<AppJobPositionResponsibilityPeriodicity> =>new Array(
            AppJobPositionResponsibilityPeriodicity.Other,
            AppJobPositionResponsibilityPeriodicity.Daily,
            AppJobPositionResponsibilityPeriodicity.Weekly,
            AppJobPositionResponsibilityPeriodicity.Monthly,
            AppJobPositionResponsibilityPeriodicity.Quarterly,
            AppJobPositionResponsibilityPeriodicity.Biannual,
            AppJobPositionResponsibilityPeriodicity.Yearly
        )
    
    public static getById = (_id:number):AppJobPositionResponsibilityPeriodicity|null => AppJobPositionResponsibilityPeriodicity.getAll().find(x=>x.id === _id)??null;

    public isEqual = (priodicity:AppJobPositionResponsibilityPeriodicity|null):boolean => priodicity!==null && priodicity.id===this.id;
}

export class AppJobPositionResponsibilityRisk extends EnumEntityBase{
    public static None=new AppJobPositionResponsibilityRisk(0,"None");
    public static VeryLow=new AppJobPositionResponsibilityRisk(1,"Very Low");
    public static Low=new AppJobPositionResponsibilityRisk(2,"Low");
    public static Medium=new AppJobPositionResponsibilityRisk(3,"Medium");
    public static High=new AppJobPositionResponsibilityRisk(4,"High");
    public static VeryHigh=new AppJobPositionResponsibilityRisk(5,"Very High");

    public static getAll = ():Array<AppJobPositionResponsibilityRisk> =>new Array(
            AppJobPositionResponsibilityRisk.None,
            AppJobPositionResponsibilityRisk.VeryLow,
            AppJobPositionResponsibilityRisk.Low,
            AppJobPositionResponsibilityRisk.Medium,
            AppJobPositionResponsibilityRisk.High,
            AppJobPositionResponsibilityRisk.VeryHigh
        )
    
    public static getById = (_id:number):AppJobPositionResponsibilityRisk|null => AppJobPositionResponsibilityRisk.getAll().find(x=>x.id === _id)??null;

    public isEqual = (priodicity:AppJobPositionResponsibilityRisk|null):boolean => priodicity!==null && priodicity.id===this.id;
}

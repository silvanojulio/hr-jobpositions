import { MongoEntityBase }  from "../db/mongoEntityBase";

export class AppDocument extends MongoEntityBase{
    constructor(
        public link:string,
        public title:string,
        public last_update:Date,
        public responsableFullName:string,
        public companyID:string ,
        public global:boolean
    ){
        super()
    }
}
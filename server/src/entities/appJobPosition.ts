import { MongoEntityBase } from "../db/mongoEntityBase";

export class AppJobPosition extends MongoEntityBase{
    constructor(
        public name:string, 
        public process:string,
        public companyID:string,
        public managerID:string | null,
        public helperID:string | null,
    ){
        super();
    }
}
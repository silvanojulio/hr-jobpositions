import { MongoEntityBase } from "../db/mongoEntityBase";

export class AppCompany extends MongoEntityBase {
    constructor(public name: string, public active: Boolean){
        super();
    }
}
import { AppUser } from "./appUser"

export default class AppSession {
    constructor(
        public appUser: AppUser, 
        public expiration:Date,
        public securityToken:string
        ){}
}
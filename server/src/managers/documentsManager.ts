import { AppDocument } from '../entities/appDocuments'
import ContextService from '../context/contextService';
import { AppCompanyRepository } from '../repositories/appCompanyRepository';
import { AppDocumentRepository } from '../repositories/appDocumentRepository'
import { AppAssignmentRepository } from '../repositories/appAssignmentRepository'
import { AppUserRepository } from '../repositories/appUserRepository'
import { AppJobPositionResponsibilityRepository } from '../repositories/appJobPositionResponsibilityRepository'
import { AppCompany } from '../entities/appCompany';
import { AppError } from '../middlewares/appErrorMiddleware';
import { Roles } from '../entities/appUser';

export class DocumentsManager{
    private static repo=new AppDocumentRepository()

    public static async addDocument(new_doc:AppDocument):Promise<AppDocument>{
        const userLogged = ContextService.getCurrentUser()
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            new_doc.companyID=userLogged.company._id as string;
        }
        new_doc.last_update=new Date()
        new_doc.responsableFullName=userLogged.fullName

        await this.validate(new_doc)
        return await this.repo.insert(new_doc)
    }
    public static async updateDocument(doc:AppDocument):Promise<AppDocument>{
        const userLogged = ContextService.getCurrentUser()
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            doc.companyID=userLogged.company._id as string;
        }
        
        doc.last_update=new Date()
        doc.responsableFullName=userLogged.fullName
        await this.validate(doc)
        return await this.repo.update(doc)
    }
    public static async deleteDocument(id:string):Promise<AppDocument>{
        const userLogged = ContextService.getCurrentUser()
        const doc=await this.repo.getById(id)
        if(!doc) throw AppError.DoesntExist.setExtraData({Document_id:id})

        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==doc.companyID) throw AppError.WrongCompany.setExtraData({})
        }
        return await this.repo.delete(doc)
    }
    public static async getByCompany(companyID:string):Promise<AppDocument[]>{
        const userLogged = ContextService.getCurrentUser()        
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            return await this.repo.getManyByFilter({companyID:userLogged.company._id})
        }
        return await this.repo.getManyByFilter({companyID})
    }
    public static async getById(id:string):Promise<AppDocument>{
        const userLogged = ContextService.getCurrentUser()
        const doc=await this.repo.getById(id)
        if(!doc) throw AppError.DoesntExist.setExtraData({Document_id:id})
        
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==doc.companyID) throw AppError.WrongCompany.setExtraData({})
        }
        return doc
    }
    public static async getRelatedToUser():Promise<AppDocument[]>{
        const userLogged = ContextService.getCurrentUser();
        
        const assignmentsRepo=new AppAssignmentRepository()
        const assignments=await assignmentsRepo.getManyByFilter({employeeID:userLogged._id})
        
        const respRepo=new AppJobPositionResponsibilityRepository()
        const resps=await respRepo.getManyByFilter({
            $or:[
                {appJobPositionID:{$in:assignments.map(assignment=>assignment.jobPositionID)}},
                {appJobPositionID:null}
            ]
        })
        const respDocs= resps.map(res=>res.documentIDs).reduce((prev,curr)=>[...new Set([...prev,...curr])],[])
        
        const userRepo=new AppUserRepository()
        const userDocs=(await userRepo.getById(userLogged._id))?.documentIDs??[]
        console.log("userDocs",userDocs)
        const relatedDocuments=await this.repo.getManyByFilter({
            $or:[
                {global:true},
                {_id:{$in:respDocs}},
                {_id:{$in:userDocs}}
            ]
        })

        return relatedDocuments
    }
    private static async validate(doc:AppDocument):Promise<void>{
        const alreadyExist=await this.repo.getByFilter(doc._id?{
            _id:doc._id
        }:{
            companyID:doc.companyID,
            title:doc.title
        })

        if(!doc._id){
            if(alreadyExist) throw AppError.AlreadyExist.setExtraData(alreadyExist)
        }else{
            if(!alreadyExist) throw AppError.DoesntExist.setExtraData(doc)
            if(alreadyExist.companyID!==doc.companyID) throw AppError.WrongCompany.setExtraData({
                oldCompanyID:alreadyExist.companyID,
                newCompanyID:doc.companyID
            })
        }
        
        const appCompanyRepository:AppCompanyRepository=new AppCompanyRepository();
        const appCompany:AppCompany | null = (await appCompanyRepository.getById(doc.companyID));
        if(!appCompany) throw AppError.DoesntExist.setExtraData({companyID:doc.companyID});

        
    }
}
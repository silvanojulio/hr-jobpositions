
import { AppPerformanceReviewDetailRepository } from '../repositories/appPerformanceReviewDetailRepository';
import { AppPerformanceReviewRepository } from '../repositories/appPerformanceReviewRepository';
import contextService from '../context/contextService';
import { AppUser, Roles } from '../entities/appUser';
import { AppError } from '../middlewares/appErrorMiddleware';
import { AppPerformanceReviewDetail } from '../entities/appPerformanceReviewDetail';
import { AppUserRepository } from '../repositories/appUserRepository';
import { AppJobPositionRepository } from '../repositories/appJobPositionRepository';
import { AppCompanyRepository } from '../repositories/appCompanyRepository';
import { AppPerformanceReview } from '../entities/appPerformanceReview';
import { AppJobPosition } from '../entities/appJobPosition';

export class PerformanceReviewDetailManager{
    private static repo:AppPerformanceReviewDetailRepository = new AppPerformanceReviewDetailRepository();

    public static async getList(performanceReview:AppPerformanceReview):Promise<Array<AppPerformanceReviewDetail>>{
        const userLogged:AppUser = contextService.getCurrentUser();
        if( !Roles.SuperAdmin.isEqual(userLogged.role)){
            if( userLogged.company===null ) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if( userLogged.company._id!==performanceReview.companyID) throw AppError.WrongCompany.setExtraData({
                userCompany:userLogged.company,
                performanceReviewCompanyID:performanceReview.companyID
            });
        }

        return this.repo.getByReviewID(performanceReview._id as string)
    }
    public static async getPerformanceReviewDetail(_id:string):Promise<AppPerformanceReviewDetail>{
        const performanceReviewDetail:AppPerformanceReviewDetail | null = await this.repo.getById(_id)
        if(performanceReviewDetail===null) throw AppError.DoesntExist.setExtraData({performanceReviewDetailID:_id});
        
        const performanceReviewRepo:AppPerformanceReviewRepository = new AppPerformanceReviewRepository();
        const performanceReview:AppPerformanceReview | null = await performanceReviewRepo.getById(performanceReviewDetail.performanceReviewID);
        if(performanceReview===null) throw AppError.DoesntExist.setExtraData({performanceReviewID:performanceReviewDetail.performanceReviewID});

        const userLogged:AppUser = contextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==performanceReview.companyID) throw AppError.WrongCompany.setExtraData({
                userLogged_company:userLogged.company,
                performanceReviewCompanyID:performanceReview.companyID
            });
        }

        return performanceReviewDetail;
    }
    public static async addPerformanceReviewDetail(performanceReviewDetail:AppPerformanceReviewDetail):Promise<AppPerformanceReviewDetail>{
        await this.validateDetail(performanceReviewDetail);
        return this.repo.insert(performanceReviewDetail);
    }
    public static async updatePerformanceReviewDetail(performanceReviewDetail:AppPerformanceReviewDetail):Promise<AppPerformanceReviewDetail>{
        const userLogged:AppUser = contextService.getCurrentUser();        
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});

        await this.validateDetail(performanceReviewDetail);
        return this.repo.update(performanceReviewDetail);
    }
    public static async deletePerformancereviewdetail(_id:string):Promise<AppPerformanceReviewDetail>{
        const performanceReviewDetail:AppPerformanceReviewDetail | null = await this.repo.getById(_id);
        if(performanceReviewDetail===null) throw AppError.DoesntExist.setExtraData({performanceReviewDetailID:_id});
        return this.repo.delete(performanceReviewDetail);
    }
    private static async validateDetail(performanceReviewDetail:AppPerformanceReviewDetail):Promise<void>{        
        const userRepository:AppUserRepository = new AppUserRepository();
        const jobPositionRepository:AppJobPositionRepository = new AppJobPositionRepository();
        const companyRepository:AppCompanyRepository = new AppCompanyRepository();
        const performanceReviewRepository:AppPerformanceReviewRepository = new AppPerformanceReviewRepository();

        const userLogged:AppUser = contextService.getCurrentUser();
        const performanceReview:AppPerformanceReview | null = await performanceReviewRepository.getById(performanceReviewDetail.performanceReviewID);
        if(performanceReview===null) throw AppError.DoesntExist.setExtraData({performanceReviewID:performanceReviewDetail.performanceReviewID});
          
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==performanceReview.companyID) throw AppError.WrongCompany.setExtraData({
                userLogged_company:userLogged.company,
                performanceReviewCompanyID:performanceReview.companyID
            });
        }
        
        if(performanceReviewDetail._id!==""){
            const exist:AppPerformanceReviewDetail | null = await this.repo.getById(performanceReviewDetail._id as string);
            if(exist===null) throw AppError.DoesntExist.setExtraData({performanceReviewDetailID:performanceReviewDetail._id});
        }
    }
}
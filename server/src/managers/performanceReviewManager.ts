import { AppPerformanceReviewRepository } from '../repositories/appPerformanceReviewRepository';
import { AppJobPositionRepository } from '../repositories/appJobPositionRepository';
import { AppCompanyRepository } from '../repositories/appCompanyRepository';
import { AppPerformanceReview, AppPerformanceReviewResult } from '../entities/appPerformanceReview';
import { AppPerformanceReviewDetail } from '../entities/appPerformanceReviewDetail';
import { AppUserRepository } from '../repositories/appUserRepository';
import contextService from '../context/contextService';
import { AppUser, Roles } from '../entities/appUser';
import { AppError } from '../middlewares/appErrorMiddleware';
import { AppJobPosition } from '../entities/appJobPosition';
import { AppCompany } from '../entities/appCompany';
import { AppPerformanceReviewDetailRepository } from '../repositories/appPerformanceReviewDetailRepository';


export class PerformanceReviewManager{
    private static repo:AppPerformanceReviewRepository = new AppPerformanceReviewRepository();

    public static async getList():Promise<AppPerformanceReview[]>{
        const userLogged:AppUser = contextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            return this.repo.getManyByFilter({companyID:userLogged.company._id});
        }
        return this.repo.getAll();
    }
    public static async getByCompanyID(companyID:string):Promise<AppPerformanceReview[]>{
        const userLogged:AppUser = contextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(userLogged.role)) {
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==companyID) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, companyID});
        }
        return this.repo.getManyByFilter({companyID})
    }
    public static async getPerformanceReview(_id:string):Promise<AppPerformanceReview>{
        const performanceReview:AppPerformanceReview | null = await this.repo.getById(_id);
        const userLogged:AppUser = contextService.getCurrentUser();

        if(performanceReview===null) throw AppError.DoesntExist.setExtraData({performanceReviewID:_id});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)) {
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==performanceReview.companyID) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, performanceReviewCompanyID:performanceReview.companyID});
        }

        return performanceReview;
    }
    public static async addPerformanceReview(performanceReview:AppPerformanceReview):Promise<AppPerformanceReview>{
        const userLogged:AppUser = contextService.getCurrentUser();
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            performanceReview.companyID=userLogged.company._id as string;
        }

        await this.validateperformanceReview(performanceReview);

        return this.repo.insert(performanceReview);
    }
    public static async updatePerformanceReview(performanceReview:AppPerformanceReview):Promise<AppPerformanceReview>{
        const userLogged:AppUser = contextService.getCurrentUser();
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            performanceReview.companyID=userLogged.company._id as string;
        }

        await this.validateperformanceReview(performanceReview);

        return this.repo.update(performanceReview);
    }
    public static async deletePerformanceReview(_id:string):Promise<AppPerformanceReview>{
        const performanceReview:AppPerformanceReview | null = await this.repo.getById(_id);
        const userLogged:AppUser = contextService.getCurrentUser();
        if(performanceReview===null) throw AppError.DoesntExist.setExtraData({performanceReviewID:_id});
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==performanceReview.companyID) throw AppError.WrongCompany.setExtraData({
                userLogged_company:userLogged.company,
                performanceReviewCompanyID:performanceReview.companyID
            })
        }

        const performanceReviewDetailsRepo:AppPerformanceReviewDetailRepository = new AppPerformanceReviewDetailRepository();
        const performanceReviewDetails:Array<AppPerformanceReviewDetail> = await performanceReviewDetailsRepo.getByReviewID(performanceReview._id as string);
        if(performanceReviewDetails.length>0) throw AppError.RelatedDataExist.setExtraData({
            performanceReview:performanceReview,
            performanceReviewDetails:performanceReviewDetails
        });

        return this.repo.delete(performanceReview);
    }
    public static async completePerformanceReview(_id:string, result:AppPerformanceReviewResult, resultComment:string,newDetails:AppPerformanceReviewDetail[]){
        const performanceReview = await this.repo.getById(_id);
        if(!performanceReview) throw new Error("Evaluación no encontrada")
        const performanceReviewDetailsRepo:AppPerformanceReviewDetailRepository = new AppPerformanceReviewDetailRepository();
        const oldDetails = await performanceReviewDetailsRepo.getByReviewID(performanceReview._id)
        await Promise.all(oldDetails.map(async old=>performanceReviewDetailsRepo.delete(old)))
        await Promise.all(newDetails.map(async detail=>performanceReviewDetailsRepo.insert(detail)))
        await this.updatePerformanceReview({...performanceReview, result, resultComment})
    }
    private static async validateperformanceReview(performanceReview:AppPerformanceReview) : Promise<void>{
        const userRepository:AppUserRepository = new AppUserRepository();
        const jobPositionRepository:AppJobPositionRepository = new AppJobPositionRepository();
        const companyRepository:AppCompanyRepository = new AppCompanyRepository();
        
        const employee:AppUser | null = await userRepository.getById(performanceReview.employeeID);
        const superior:AppUser | null = await userRepository.getById(performanceReview.evaluatorID);
        const jobPosition:AppJobPosition | null = await jobPositionRepository.getById(performanceReview.jobPositionID);
        const performanceReviewResult:AppPerformanceReviewResult | null = AppPerformanceReviewResult.getById(performanceReview.result.id);
        
        if(employee===null) throw AppError.DoesntExist.setExtraData({employeeID:performanceReview.employeeID});
        if(superior===null) throw AppError.DoesntExist.setExtraData({directSuperiorID:performanceReview.evaluatorID});
        if(jobPosition===null) throw AppError.DoesntExist.setExtraData({jobPositionID:performanceReview.jobPositionID});
        if(performanceReviewResult===null) throw AppError.DoesntExist.setExtraData({result:performanceReview.result});

        const jobPositionCompany:AppCompany | null = await companyRepository.getById(jobPosition.companyID);

        if(employee.company===null) throw AppError.ElementMalformed.setExtraData({employee:employee});
        if(superior.company===null) throw AppError.ElementMalformed.setExtraData({superior:superior});
        if(jobPositionCompany===null) throw AppError.DoesntExist.setExtraData({jobPositionCompanyID:jobPosition.companyID})


        if(performanceReview.companyID!==employee.company._id) throw AppError.WrongCompany.setExtraData({performanceReviewCompanyID:performanceReview.companyID, employee_company:employee.company});
        if(performanceReview.companyID!==superior.company._id) throw AppError.WrongCompany.setExtraData({performanceReviewCompanyID:performanceReview.companyID, superior_company:superior.company});
        if(performanceReview.companyID!==jobPositionCompany._id) throw AppError.WrongCompany.setExtraData({performanceReviewCompanyID:performanceReview.companyID, jobPositionCompany:jobPositionCompany});

        const userLogged:AppUser = contextService.getCurrentUser();

        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==performanceReview.companyID) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, performanceReviewCompanyID:performanceReview.companyID});
        }
        
        if(performanceReview._id!==""){
            const exist:AppPerformanceReview | null = await this.repo.getById(performanceReview._id as string);
            if(exist===null) throw AppError.DoesntExist.setExtraData({performanceReviewID:performanceReview._id});
        }
    }
}
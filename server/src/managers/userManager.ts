import { AppUser, Roles } from '../entities/appUser';
import { AppCompany } from '../entities/appCompany';
import { AppUserRepository } from '../repositories/appUserRepository';
import { SecurityManager } from './securityManager';
import { CompanyManager } from './companyManager';
import ContextService from '../context/contextService';
import { AppError } from '../middlewares/appErrorMiddleware';

export class UserManager{
    public static async getList() : Promise<Array<AppUser>> {
        const loggedUser = ContextService.getCurrentUser();
        const appUserRepo = new AppUserRepository();
        return await appUserRepo.getManyByFilter(
            !Roles.SuperAdmin.isEqual(loggedUser.role)?{
                company: loggedUser.company
            }:{}
        );
    }
    public static async getById(id:string) : Promise<AppUser>{
        const loggedUser = ContextService.getCurrentUser();
        const appUserRepo = new AppUserRepository();
        const user = await appUserRepo.getById(id);
        if(!user) throw AppError.DoesntExist.setExtraData({user_id:id})
        if(!Roles.SuperAdmin.isEqual(loggedUser.role)){
            if(!user.company) throw AppError.ElementMalformed.setExtraData({user_company:user.company})
            if(user.company._id!==loggedUser.company?._id) throw AppError.NotAllowed.setExtraData({
                user,
                loggedUser
            })
            return user
        }
        
        return user
    }
    public static async getActive() : Promise<Array<AppUser>> {
        const loggedUser = ContextService.getCurrentUser();
        const appUserRepo = new AppUserRepository();
        return await appUserRepo.getManyByFilter(
            !Roles.SuperAdmin.isEqual(loggedUser.role)?{
                company: loggedUser.company,
                active: true
            }:{
                active:true
            }
        );
    }
    public static async addUser(newUser: AppUser ) : Promise<AppUser>{
        // Solo el usuario SuperAdmin puede crear usuarios CompanyAdmin.
        const loggedUser = ContextService.getCurrentUser();
        if(!Roles.Employee.isEqual(newUser.role) && !Roles.SuperAdmin.isEqual(loggedUser.role)) throw AppError.NotAllowed;

        // Se debe verificar que no exista otro usuario con el mismo mail en la base de datos. 
        const appUserRepo:AppUserRepository = new AppUserRepository();        
        const userAlreadyExists: AppUser | null = await appUserRepo.getUserByEmail(newUser.email);
        if(userAlreadyExists!==null) throw AppError.AlreadyExist.setExtraData({newUserEmail:newUser.email});

        //

        // Un usuario de rol Super Admin puede seleccionar cualquier empresa 
        // para el usuario que està editando o dando de alta. Mientras que un 
        // usuario Administrador de empresa no puede realizar esa selección y 
        // todos los usuarios que cree, serán asignados a la empresa del usuario logueado.
        
        if(!Roles.SuperAdmin.isEqual(loggedUser.role)) newUser.company=loggedUser.company;
        


        return await appUserRepo.insert(newUser);
    }
    public static async updateUser(updatedUser: AppUser): Promise<AppUser>{
        const appUserRepo: AppUserRepository = new AppUserRepository();
        const actualUser : AppUser | null= await appUserRepo.getById(updatedUser._id as string);
        if(actualUser===null) throw AppError.DoesntExist.setExtraData({user_id:updatedUser._id, updateData:updatedUser});

        // Un usuario de rol Super Admin puede seleccionar cualquier empresa 
        // para el usuario que està editando o dando de alta. Mientras que un 
        // usuario Administrador de empresa no puede realizar esa selección y 
        // todos los usuarios que cree, serán asignados a la empresa del usuario logueado.
        const loggedUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(loggedUser.role) && updatedUser.company?._id!==loggedUser.company?._id) 
            throw AppError.WrongCompany.setExtraData({userToUpdate:actualUser.company,loggedUser:loggedUser.company});
            
            
        // Se debe verificar que no exista otro usuario con el mismo mail en la base de datos. 
        const userAlreadyExists: AppUser | null = await appUserRepo.getUserByEmail(updatedUser.email);
        if(userAlreadyExists!==null && userAlreadyExists._id!==userAlreadyExists._id) throw AppError.AlreadyExist.setExtraData({updateDataEmail:updatedUser.email});

        return await appUserRepo.update(updatedUser);
    }
    public static async deleteUser(id: string): Promise<AppUser>{
        const appUserRepo: AppUserRepository = new AppUserRepository();
        const userToDelete:AppUser | null= await appUserRepo.getById(id);
        if(userToDelete===null) throw AppError.DoesntExist.setExtraData({user_id:id});    
        
        // Un usuario de rol Super Admin puede seleccionar cualquier empresa 
        // para el usuario que està editando o dando de alta. Mientras que un 
        // usuario Administrador de empresa no puede realizar esa selección y 
        // todos los usuarios que cree, serán asignados a la empresa del usuario logueado.    
        const loggedUser = ContextService.getCurrentUser();
        if(Roles.Employee.isEqual(loggedUser.role)) throw AppError.NotAllowed;
        if(!Roles.SuperAdmin.isEqual(loggedUser.role) && userToDelete.company?._id!==loggedUser.company?._id) 
            throw AppError.WrongCompany.setExtraData({actualCompany:loggedUser.company, userCompany:userToDelete.company});

        return await appUserRepo.delete(userToDelete);
             
    }
    public static async getByCompany(companyID:string): Promise<AppUser[]>{
        const appUserRepo: AppUserRepository=new AppUserRepository()
        const list:AppUser[]=await appUserRepo.getAll()
        return list.filter((user:AppUser)=>user.company?._id===companyID)
    }
}
import { AppUser, Roles } from '../entities/appUser';
import { AppJobPositionRepository } from '../repositories/appJobPositionRepository';
import { AppAssignmentRepository } from '../repositories/appAssignmentRepository';
import { AppUserRepository } from '../repositories/appUserRepository';
import { AppCompanyRepository } from '../repositories/appCompanyRepository';
import ContextService from '../context/contextService';
import { AppError } from '../middlewares/appErrorMiddleware';
import { AppAssignment } from '../entities/appAssignment'
import { AppJobPosition } from '../entities/appJobPosition'
import { AppCompany } from '../entities/appCompany';

export class AssignmentManager{
    private static repo:AppAssignmentRepository=new AppAssignmentRepository();

    public static async addAssignment(appAssignment:AppAssignment):Promise<AppAssignment>{
        const appUserRepository:AppUserRepository=new AppUserRepository();
        const appJobPositionRepository:AppJobPositionRepository=new AppJobPositionRepository();
        const appCompanyRepository:AppCompanyRepository=new AppCompanyRepository();

        const employee:AppUser | null = (await appUserRepository.getById(appAssignment.employeeID));
        const evaluator:AppUser | null = (await appUserRepository.getById(appAssignment.employeeEvaluatorID));
        const jobPosition:AppJobPosition | null = (await appJobPositionRepository.getById(appAssignment.jobPositionID));

        if(employee===null) throw AppError.DoesntExist.setExtraData({employeeID:appAssignment.employeeID});
        if(evaluator===null) throw AppError.DoesntExist.setExtraData({employeeEvaluatorID:appAssignment.employeeEvaluatorID});
        if(jobPosition===null) throw AppError.DoesntExist.setExtraData({jobPositionID:appAssignment.jobPositionID});

        const jobPositionCompany:AppCompany | null = (await appCompanyRepository.getById(jobPosition.companyID));

        if(employee.company===null) throw AppError.ElementMalformed.setExtraData({employee:employee});
        if(evaluator.company===null) throw AppError.ElementMalformed.setExtraData({evaluator:evaluator});
        if(jobPositionCompany===null) throw AppError.DoesntExist.setExtraData({jobPosition_company:jobPositionCompany});
        
        const userLogged:AppUser = ContextService.getCurrentUser();

        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});

        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==employee.company._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, employee_company:employee.company});
            if(userLogged.company._id!==evaluator.company._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, evaluator_company:evaluator.company});
            if(userLogged.company._id!==jobPositionCompany._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, jobPosition_company:jobPositionCompany});
        }
        return this.repo.insert(appAssignment);
    }
    public static async removeAssignment(_id:string):Promise<AppAssignment>{
        const appAssignment:AppAssignment|null=await this.repo.getById(_id);
        if(appAssignment===null) throw AppError.DoesntExist.setExtraData({_id:_id});
        const userLogged:AppUser = ContextService.getCurrentUser();
        const appUserRepository:AppUserRepository = new AppUserRepository();
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            const appAssignmentEmployee:AppUser | null = (await appUserRepository.getById(appAssignment.employeeID));
            if(appAssignmentEmployee===null) throw AppError.DoesntExist.setExtraData({assignment_employeeID:appAssignment.employeeID});
            if(appAssignmentEmployee.company===null) throw AppError.ElementMalformed.setExtraData({assignment_employee:appAssignmentEmployee});
            if(userLogged.company._id!==appAssignmentEmployee.company._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, assignment_company:appAssignmentEmployee.company});
        }
        return this.repo.delete(appAssignment);
    }
    public static async updateAssignment(newAssignment:AppAssignment):Promise<AppAssignment>{
        const appAssignmentRepository:AppAssignmentRepository = new AppAssignmentRepository();
        const oldAssignment:AppAssignment | null = (await appAssignmentRepository.getByFilter({
            jobPositionID:newAssignment.jobPositionID,
            employeeID:newAssignment.employeeID
        }));
        if(oldAssignment!==null) newAssignment._id=oldAssignment._id;

        const appUserRepository:AppUserRepository=new AppUserRepository();
        const appJobPositionRepository:AppJobPositionRepository=new AppJobPositionRepository();
        const appCompanyRepository:AppCompanyRepository=new AppCompanyRepository();

        const employee:AppUser | null = (await appUserRepository.getById(newAssignment.employeeID));
        const evaluator:AppUser | null = (await appUserRepository.getById(newAssignment.employeeEvaluatorID));
        const jobPosition:AppJobPosition | null = (await appJobPositionRepository.getById(newAssignment.jobPositionID));

        if(employee===null) throw AppError.DoesntExist.setExtraData({employeeID:newAssignment.employeeID});
        if(evaluator===null) throw AppError.DoesntExist.setExtraData({employeeEvaluatorID:newAssignment.employeeEvaluatorID});
        if(jobPosition===null) throw AppError.DoesntExist.setExtraData({jobPositionID:newAssignment.jobPositionID});

        const jobPositionCompany:AppCompany | null = (await appCompanyRepository.getById(jobPosition.companyID));

        if(employee.company===null) throw AppError.ElementMalformed.setExtraData({employee:employee});
        if(evaluator.company===null) throw AppError.ElementMalformed.setExtraData({evaluator:evaluator});
        if(jobPositionCompany===null) throw AppError.DoesntExist.setExtraData({jobPosition_company:jobPositionCompany});
        
        const userLogged:AppUser = ContextService.getCurrentUser();

        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});

        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==employee.company._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, employee_company:employee.company});
            if(userLogged.company._id!==evaluator.company._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, evaluator_company:evaluator.company});
            if(userLogged.company._id!==jobPositionCompany._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, jobPosition_company:jobPositionCompany});
        }

        return appAssignmentRepository.update(newAssignment);
    }
    public static async getList():Promise<Array<AppAssignment>>{
        const appAssignmentRepository:AppAssignmentRepository = new AppAssignmentRepository();
        const appUserRepository:AppUserRepository = new AppUserRepository();
        const userLogged:AppUser = ContextService.getCurrentUser();

        if(Roles.SuperAdmin.isEqual(userLogged.role)) return appAssignmentRepository.getAll();
        if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});

        const employeeIDs:string[] = (await appUserRepository.getManyByFilter({company:userLogged.company})).map( (employee:AppUser) => employee._id as string);
        return appAssignmentRepository.getManyByFilter({
            employeeID:{$in: employeeIDs}
        });
    }
    public static async getByEmployeeID(employeeID:string):Promise<AppAssignment[]>{
        const appAssignmentRepository:AppAssignmentRepository = new AppAssignmentRepository();
        const appUserRepository:AppUserRepository = new AppUserRepository();
        const userLogged:AppUser = ContextService.getCurrentUser();
        const employee:AppUser|null = await appUserRepository.getById(employeeID)

        if(employee===null) throw AppError.DoesntExist.setExtraData({employeeID})

        if(Roles.SuperAdmin.isEqual(userLogged.role)) return appAssignmentRepository.getManyByFilter({employeeID:employee._id})??[];

        if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(employee.company?._id!==userLogged.company._id) throw AppError.NotAllowed.setExtraData({});
        
        return appAssignmentRepository.getManyByFilter({employeeID:employee._id})
    }
    public static async getAssignment(_id:string):Promise<AppAssignment>{
        return new Promise(async (resolve, reject)=>{
            const appAssignment:AppAssignment|null=(await this.repo.getById(_id));
            if(appAssignment===null) reject(AppError.DoesntExist.setExtraData({assignmentID:_id}));
            else try {
                await this.validateAssignment(appAssignment);
                resolve(appAssignment);
            }catch(error){reject(error);}
        })
    }

    private static async validateAssignment(appAssignment:AppAssignment):Promise<void>{
        const appUserRepository:AppUserRepository=new AppUserRepository();
        const appJobPositionRepository:AppJobPositionRepository=new AppJobPositionRepository();
        const appCompanyRepository:AppCompanyRepository=new AppCompanyRepository();

        const employee:AppUser | null = (await appUserRepository.getById(appAssignment.employeeID));
        const evaluator:AppUser | null = (await appUserRepository.getById(appAssignment.employeeEvaluatorID));
        const jobPosition:AppJobPosition | null = (await appJobPositionRepository.getById(appAssignment.jobPositionID));

        if(employee===null) throw AppError.DoesntExist.setExtraData({employeeID:appAssignment.employeeID});
        if(evaluator===null) throw AppError.DoesntExist.setExtraData({employeeEvaluatorID:appAssignment.employeeEvaluatorID});
        if(jobPosition===null) throw AppError.DoesntExist.setExtraData({jobPositionID:appAssignment.jobPositionID});

        const jobPositionCompany:AppCompany | null = (await appCompanyRepository.getById(jobPosition.companyID));

        if(employee.company===null) throw AppError.ElementMalformed.setExtraData({employee:employee});
        if(evaluator.company===null) throw AppError.ElementMalformed.setExtraData({evaluator:evaluator});
        if(jobPositionCompany===null) throw AppError.DoesntExist.setExtraData({jobPosition_company:jobPositionCompany});
        
        const userLogged:AppUser = ContextService.getCurrentUser();

        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});

        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==employee.company._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, employee_company:employee.company});
            if(userLogged.company._id!==evaluator.company._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, evaluator_company:evaluator.company});
            if(userLogged.company._id!==jobPositionCompany._id) throw AppError.WrongCompany.setExtraData({userLogged_company:userLogged.company, jobPosition_company:jobPositionCompany});
        }
        
        const alreadyExist:AppAssignment|null=(await this.repo.getByFilter({
            employeeID:appAssignment.employeeID,
            jobPositionID:appAssignment.jobPositionID
        }));

        if(appAssignment._id==="" && alreadyExist!==null) throw AppError.AlreadyExist.setExtraData({
            newAssignment:appAssignment,
            alreadyExist:alreadyExist
        });
        if(appAssignment._id!=="" && alreadyExist===null) throw AppError.DoesntExist.setExtraData({editedAssignment:appAssignment});
        

    }
}
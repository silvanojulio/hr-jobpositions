import { ObjectId } from 'mongodb';

import { AppUser, Roles }               from '../entities/appUser';
import { AppJobPositionResponsibility } from '../entities/appJobPositionResponsibility';
import { AppJobPosition }               from '../entities/appJobPosition';

import { AppUserRepository }                        from '../repositories/appUserRepository';
import { AppJobPositionRepository }                 from '../repositories/appJobPositionRepository';
import { AppJobPositionResponsibilityRepository }   from '../repositories/appJobPositionResponsibilityRepository';

import ContextService   from '../context/contextService';
import { AppError }     from '../middlewares/appErrorMiddleware';


export class JobPositionResponsibilityManager{
    private static repo:AppJobPositionResponsibilityRepository=new AppJobPositionResponsibilityRepository();
    public static async getList(companyID:string):Promise<Array<AppJobPositionResponsibility>>{   
        const userLogged:AppUser = ContextService.getCurrentUser();
        if( !Roles.SuperAdmin.isEqual(userLogged.role)){
            if( userLogged.company===null ) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if( userLogged.company._id!==companyID) throw AppError.WrongCompany.setExtraData({
                userCompany:userLogged.company,
                jobPositionCompanyID:companyID
            });
        }
        return this.repo.getByCompanyID(companyID)
        
    }
    public static async addResponsibility(responsibility:AppJobPositionResponsibility): Promise<AppJobPositionResponsibility>{
        await this.validateResponsibility(responsibility);
        return this.repo.insert(responsibility);
    }
    public static async getResponsibility(_id:string): Promise<AppJobPositionResponsibility | null>{
        return new Promise(async (resolve, reject) => {
            const responsibility:AppJobPositionResponsibility|null=(await this.repo.getById(_id));
            if(responsibility===null) reject(AppError.DoesntExist.setExtraData({responsibilityID:_id}));
            else try {
                await this.validateResponsibility(responsibility);
                resolve(responsibility);
            }catch(error){reject(error);};            
        })
    }
    public static async updateResponsibility(responsibility:AppJobPositionResponsibility): Promise<AppJobPositionResponsibility>{  
        await this.validateResponsibility(responsibility);
        return this.repo.update(responsibility); 
    }
    public static async deleteResponsibility(_id:string):Promise<AppJobPositionResponsibility>{
        const responsibility:AppJobPositionResponsibility|null= (await this.getResponsibility(_id));
        if(responsibility===null) throw AppError.DoesntExist.setExtraData({responsibilityID:_id});
        await this.validateResponsibility(responsibility);
        return this.repo.delete(responsibility);
    }
    private static async validateResponsibility(responsibility:AppJobPositionResponsibility):Promise<void>{
        const userLogged:AppUser = ContextService.getCurrentUser();
        if(responsibility.appJobPositionID){
            const jobPositionRepository:AppJobPositionRepository=new AppJobPositionRepository();
            const jobPosition:AppJobPosition|null=(await jobPositionRepository.getById(responsibility.appJobPositionID));
            if(jobPosition===null) throw AppError.DoesntExist.setExtraData({jobPositionID:responsibility.appJobPositionID});
        }
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null ) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(userLogged.company._id!==responsibility.companyID) throw AppError.WrongCompany.setExtraData({
                userCompany:userLogged.company,
                responsibility:responsibility
            });
        }
        const appUserRepository:AppUserRepository=new AppUserRepository();
        const controlledBy:AppUser|null=(await appUserRepository.getById(responsibility.controlledBy));
        if(controlledBy===null) throw AppError.DoesntExist.setExtraData({controlledBy:responsibility.controlledBy});

        if(responsibility._id!==""){
            const alreadyExist:AppJobPositionResponsibility|null=(await this.repo.getById(responsibility._id as string));
            if(alreadyExist===null) throw AppError.DoesntExist.setExtraData(responsibility);
        }
    }
}
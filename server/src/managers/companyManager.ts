import {AppCompany} from '../entities/appCompany';
import {AppUser, Roles} from '../entities/appUser';
import { AppCompanyRepository } from '../repositories/appCompanyRepository';
import ContextService from '../context/contextService';
import { AppError } from '../middlewares/appErrorMiddleware';
import { AppUserRepository } from '../repositories/appUserRepository';

export class CompanyManager{
    public static async addCompany(newCompany:AppCompany): Promise<AppCompany>{
        const loggedUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(loggedUser.role)) throw AppError.NotAllowed;

        const appCompanyRepo=new AppCompanyRepository();
        const companyAlreadyExist:AppCompany|null= await appCompanyRepo.getByFilter({name:newCompany.name});
        if(companyAlreadyExist!==null) throw AppError.AlreadyExist.setExtraData(companyAlreadyExist);

        return await appCompanyRepo.insert(newCompany);
    }
    public static async getList():Promise<Array<AppCompany>>{
        const loggedUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(loggedUser.role)) throw AppError.NotAllowed;

        const appCompanyRepo=new AppCompanyRepository();
        return (await appCompanyRepo.getCompanies());
    }

    public static async getActive():Promise<Array<AppCompany>>{
        const loggedUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(loggedUser.role)) throw AppError.NotAllowed;
        const appCompanyRepo=new AppCompanyRepository();
        return (await appCompanyRepo.getCompaniesActive(true));
    }

    public static async getCompany(id:string):Promise<AppCompany>{
        const loggedUser:AppUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(loggedUser.role)){
            if(loggedUser.company===null) throw AppError.ElementMalformed.setExtraData({loggedUser:loggedUser});
            if(loggedUser.company._id!=id) throw AppError.NotAllowed;
        }
        
        const appCompanyRepo=new AppCompanyRepository();
        const company:AppCompany|null = await appCompanyRepo.getById(id);
        if(!company) throw AppError.DoesntExist.setExtraData({companyId:id});
        return company;
    }

    public static async getByName(name: string):Promise<AppCompany | null>{
        const appCompanyRepo=new AppCompanyRepository();
        return (await appCompanyRepo.getByFilter({
            name:name
        }));

    }

    public static async deleteCompany(id:string):Promise<AppCompany>{
        const loggedUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(loggedUser.role)) throw AppError.NotAllowed;

        const appCompanyRepo: AppCompanyRepository = new AppCompanyRepository();
        const companyToDelete:AppCompany | null= await appCompanyRepo.getById(id);
        if(companyToDelete===null){
            throw AppError.DoesntExist.setExtraData({companyId:id});
        }else{
            return await appCompanyRepo.delete(companyToDelete);
        }  
    }

    public static async updateCompany(id:string, editCompany:AppCompany):Promise<AppCompany>{
        const loggedUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(loggedUser.role)) throw AppError.NotAllowed;

        await this.validateCompany(editCompany);

        const company:AppCompany=await this.getCompany(id);
        const appCompanyRepo=new AppCompanyRepository();
        
        return appCompanyRepo.update(editCompany);

    }

    private static async validateCompany(company:AppCompany):Promise<void>{
        const appCompanyRepo=new AppCompanyRepository();
        const alreadyExist:AppCompany|null=await appCompanyRepo.getByFilter({name:company.name});

        if(company._id===""){
            if(alreadyExist!==null) throw AppError.AlreadyExist.setExtraData(alreadyExist);
        }else{
            if(alreadyExist!==null && alreadyExist._id!==company._id) throw AppError.AlreadyExist.setExtraData(alreadyExist);
        }
    }
    
    public static async getEvaluators(companyID:string):Promise<AppUser[]>{

        const appUserRepo: AppUserRepository=new AppUserRepository()
        const list:AppUser[]=await appUserRepo.getAll()
        return list.filter(user=>user.company?._id===companyID && Roles.CompanyAdmin.isEqual(user.role))
    }
}
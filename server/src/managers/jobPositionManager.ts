import { AppUser, Roles } from '../entities/appUser';
import { AppJobPositionRepository } from '../repositories/appJobPositionRepository';
import { AppAssignmentRepository } from '../repositories/appAssignmentRepository';
import { AppCompanyRepository } from '../repositories/appCompanyRepository';
import { AppUserRepository } from '../repositories/appUserRepository';
import ContextService from '../context/contextService';
import { AppError } from '../middlewares/appErrorMiddleware';
import { AppJobPosition } from '../entities/appJobPosition';
import { AppAssignment } from '../entities/appAssignment';
import { AppCompany } from '../entities/appCompany';
import { rejects } from 'assert';
import { AppJobPositionResponsibilityRepository } from '../repositories/appJobPositionResponsibilityRepository';
import { AppJobPositionResponsibility } from '../entities/appJobPositionResponsibility';

export class JobPositionManager{
    private static repo:AppJobPositionRepository=new AppJobPositionRepository();
    
    public static async addJobPosition(newPosition:AppJobPosition):Promise<AppJobPosition>{
        const userLogged:AppUser = ContextService.getCurrentUser();
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            newPosition.companyID=userLogged.company._id as string;
        }
        await this.validateJobPosition(newPosition);

        return this.repo.insert(newPosition);
    }
    public static async getList():Promise<Array<AppJobPosition>>{        
        const userLogged:AppUser = ContextService.getCurrentUser();
        if(Roles.SuperAdmin.isEqual(userLogged.role)) return this.repo.getAll();

        if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});

        return this.repo.getManyByFilter({CompanyID:userLogged.company._id});
    }
    public static async getJobPosition(_id:string):Promise<AppJobPosition | null>{        
        const userLogged:AppUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(userLogged.role) && userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
        const filter:any=Roles.SuperAdmin.isEqual(userLogged.role)?{_id:_id}:{_id:_id, companyID:userLogged.company?._id};
        return this.repo.getByFilter(filter);
    }
    public static async updateJobPosition(editedPosition:AppJobPosition):Promise<AppJobPosition>{     
        const userLogged:AppUser = ContextService.getCurrentUser();
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            editedPosition.companyID=userLogged.company._id as string;
        }
        
        await this.validateJobPosition(editedPosition);
        
        return this.repo.update(editedPosition);
    }
    public static async deleteJobPosition(id:string):Promise<AppJobPosition>{
        const userLogged=ContextService.getCurrentUser();
        const appJobPosition=(await this.repo.getById(id));
        if(appJobPosition===null) throw AppError.DoesntExist.setExtraData({JobPosition_ID:id})
        if(Roles.Employee.isEqual(userLogged.role)) throw AppError.NotAllowed.setExtraData({});
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            if(appJobPosition.companyID!==userLogged.company._id) throw AppError.NotAllowed.setExtraData({});
        }
        const appAssignmentRepository = new AppAssignmentRepository();
        const assignments = (await appAssignmentRepository.getByJobPosition(appJobPosition));
        if(assignments.length>0){
            Promise
            .all(assignments.map(assignment=>appAssignmentRepository.delete(assignment)))
            .catch(()=>{
                throw AppError.RelatedDataExist.setExtraData({assignments});
            })
        }    
        
        const appJobPositionResponsibility = new AppJobPositionResponsibilityRepository();
        const responsibilities = (await appJobPositionResponsibility.getByJobPositionID(appJobPosition._id as string));
        if(responsibilities.length>0) {
            Promise
            .all(responsibilities.map(res=>appJobPositionResponsibility.delete(res)))
            .catch(()=>{
                throw AppError.RelatedDataExist.setExtraData({responsibilities});
            })
        }

        return this.repo.delete(appJobPosition);

    }
    private static async validateJobPosition(jobPosition:AppJobPosition):Promise<void>{
        const alreadyExist:AppJobPosition|null=(await this.repo.getByFilter(jobPosition._id===null?{
            companyID:jobPosition.companyID,
            name:jobPosition.name
        }:{
            _id:jobPosition._id
        }));
        if(jobPosition._id===""){
            if(alreadyExist!==null) throw AppError.AlreadyExist.setExtraData(alreadyExist);
        }else{
            if(alreadyExist===null) throw AppError.DoesntExist.setExtraData(jobPosition);
            if(alreadyExist.companyID!==jobPosition.companyID) throw AppError.WrongCompany.setExtraData({
                oldCompanyID:alreadyExist.companyID,
                newCompanyID:jobPosition.companyID
            });
            if(alreadyExist._id!==jobPosition._id) throw AppError.AlreadyExist.setExtraData(alreadyExist);
        }

        const appCompanyRepository:AppCompanyRepository=new AppCompanyRepository();
        const appCompany:AppCompany | null = (await appCompanyRepository.getById(jobPosition.companyID));
        if(appCompany===null) throw AppError.DoesntExist.setExtraData({companyID:jobPosition.companyID});

        const appUserRepository:AppUserRepository=new AppUserRepository();
        if(!!jobPosition.helperID && !(await appUserRepository.getById(jobPosition.helperID)))
            throw AppError.DoesntExist.setExtraData({helperID:jobPosition.helperID})
        
        return;
    }
    public static async getByCompany(companyID:string):Promise<AppJobPosition[]>{
        const userLogged:AppUser = ContextService.getCurrentUser();
        if(!Roles.SuperAdmin.isEqual(userLogged.role)){            
            if(userLogged.company===null) throw AppError.ElementMalformed.setExtraData({userLogged:userLogged});
            return this.repo.getManyByFilter({companyID:userLogged.company._id})
        }
        return await this.repo.getManyByFilter({companyID:companyID})
    }
}
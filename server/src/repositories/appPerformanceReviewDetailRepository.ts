import { RepositoryBase } from './repositoryBase';
import { AppPerformanceReviewDetail } from '../entities/appPerformanceReviewDetail';
import { AppError } from '../middlewares/appErrorMiddleware';

export class AppPerformanceReviewDetailRepository extends RepositoryBase<AppPerformanceReviewDetail>{
    constructor (){
        super("AppPerformanceReviewDetails");
    }
    
    public async getByReviewID(_id:string): Promise<Array<AppPerformanceReviewDetail>>{
        return this.getManyByFilter({performanceReviewID:_id});
    }
}
import { RepositoryBase } from './repositoryBase';
import { AppJobPosition } from '../entities/appJobPosition'
import ContextService from '../context/contextService';
import { AppUser, Roles} from '../entities/appUser';
import { AppError } from '../middlewares/appErrorMiddleware';

export class AppJobPositionRepository extends RepositoryBase<AppJobPosition>{
    constructor (){
        super("AppJobPositions");
    }

    public async getJobPositions():Promise<Array<AppJobPosition>>{        
        const userLogged:AppUser = ContextService.getCurrentUser();
        if( Roles.SuperAdmin.isEqual(userLogged.role)) return this.getAll();
        if( userLogged.company ) return (await this.getManyByFilter({PositionCompanyID:userLogged.company._id}));
        throw AppError.ElementMalformed.setExtraData({userCompany:userLogged.company});
    }
}
import { RepositoryBase } from './repositoryBase';
import { AppPerformanceReview } from '../entities/appPerformanceReview';
import { AppError } from '../middlewares/appErrorMiddleware';

export class AppPerformanceReviewRepository extends RepositoryBase<AppPerformanceReview>{
    constructor (){
        super("AppPerformanceReviews");
    }
    
}
import { RepositoryBase } from './repositoryBase';
import { AppAssignment } from '../entities/appAssignment'
import ContextService from '../context/contextService';
import { AppUser, Roles} from '../entities/appUser';
import { AppJobPosition} from '../entities/appJobPosition';
import { AppError } from '../middlewares/appErrorMiddleware';



export class AppAssignmentRepository extends RepositoryBase<AppAssignment>{
    constructor (){
        super("AppAssignments");
    }
    
    public async getByJobPosition(appJobPosition:AppJobPosition):Promise<Array<AppAssignment>>{
        const appAssignmentRepository:AppAssignmentRepository = new AppAssignmentRepository();
        return appAssignmentRepository.getManyByFilter({jobPositionID:appJobPosition._id});
    }
}
import { RepositoryBase } from './repositoryBase'
import { AppDocument } from '../entities/appDocuments'

export class AppDocumentRepository extends RepositoryBase<AppDocument>{
    public constructor(){
        super("AppDocuments");
    }
}
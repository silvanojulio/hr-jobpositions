import {MongoClient, ObjectID, FilterQuery, OptionalId, WithId} from 'mongodb';
import {IMongoEntityBase} from './../db/mongoEntityBase';
import config from '../config';

const database = config.database.name;
const url = config.database.mongoUrl || "";

export class RepositoryBase<T extends IMongoEntityBase>
{
    public collectionName: string = "";

    public constructor(collectionName: string) {
        this.collectionName = collectionName;
    }

    async update(item:WithId<T>):Promise<WithId<T>>{
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        await collection.updateOne({_id:item._id} as FilterQuery<T>,{$set:item as T})
        return item
    }

    async insert(item:OptionalId<T>):Promise<WithId<T>>{
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        const res=await collection.insertOne({...item, _id:new ObjectID().toHexString()})
        return res.ops[0]
    }

    async insertMany(items:OptionalId<T>[]):Promise<WithId<T>[]>{
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        const res = await collection.insertMany(items.map(item=>{return {...item, _id:new ObjectID().toHexString()}}))
        return res.ops
    }

    async getById(id:string): Promise<WithId<T> | null>{
        
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        return collection.findOne<WithId<T>>({ _id: id} as FilterQuery<T>)
    }

    async getByIds(ids:string[]): Promise<WithId<T>[]>{
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        return collection.find<WithId<T>>({_id: {$in:ids}} as FilterQuery<T>).toArray()
    }

    async getManyByFilter(filter:FilterQuery<T>):Promise<WithId<T>[]>{        
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        return collection.find<WithId<T>>(filter).toArray()
    }

    async getByFilter(filter:FilterQuery<T>): Promise<WithId<T> | null>{
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        return collection.findOne<WithId<T>>(filter)
    }

    async getAll(): Promise<WithId<T>[]>{
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        return collection.find<WithId<T>>({}).toArray()
    }
    async delete(item:WithId<T>): Promise<WithId<T>>{        
        const mongoClient = await this.getMongoClient();
        const db = mongoClient.db(database);
        const collection = db.collection<T>(this.collectionName);
        await collection.deleteOne({_id:item._id} as FilterQuery<T>)
        return item
    }
    public async getMongoClient():Promise<MongoClient>{
        return MongoClient.connect(url,{ useUnifiedTopology: true })
    }
    
}
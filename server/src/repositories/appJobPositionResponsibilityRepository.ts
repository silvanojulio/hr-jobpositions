import { RepositoryBase } from './repositoryBase';
import { AppJobPositionResponsibility } from '../entities/appJobPositionResponsibility'
import { ObjectId } from 'mongodb';

export class AppJobPositionResponsibilityRepository extends RepositoryBase<AppJobPositionResponsibility>{
    constructor (){
        super("AppJobPositionResponsibilities");
    }

    public async getByJobPositionID(appJobPositionID:string):Promise<AppJobPositionResponsibility[]>{
        return this.getManyByFilter({appJobPositionID})
    }
    public async getByCompanyID(companyID:string):Promise<AppJobPositionResponsibility[]>{
        return (await this.getManyByFilter({companyID}))
    }
}
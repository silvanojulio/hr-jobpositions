import { RepositoryBase } from './repositoryBase';
import { AppUser, Roles} from '../entities/appUser';
import { AppCompany } from '../entities/appCompany';
import ContextService from '../context/contextService';
import { AppError } from '../middlewares/appErrorMiddleware';

export class AppCompanyRepository extends RepositoryBase<AppCompany>{

    public constructor() {
        super("AppCompanies");
    }

    public async getCompanies(): Promise<AppCompany[]>{
        const userLogged:AppUser = ContextService.getCurrentUser();
        if( Roles.SuperAdmin.isEqual(userLogged.role)) return this.getAll();
        if( userLogged.company ) return [userLogged.company];
        throw AppError.ElementMalformed.setExtraData({userCompany:userLogged.company});
    }

    public async getCompaniesActive(active:Boolean):Promise<AppCompany[]>{
        const userLogged:AppUser = ContextService.getCurrentUser();
        if( Roles.SuperAdmin.isEqual(userLogged.role)) return this.getManyByFilter({active:active});
        throw AppError.NotAllowed;
    }
}
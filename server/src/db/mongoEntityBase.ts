import { ObjectID, WithId } from "mongodb";

export interface IMongoEntityBase{
    _id: string | ObjectID;
    isNew():boolean;
}

export class MongoEntityBase implements IMongoEntityBase{
    _id: string="";
    public isNew():boolean{
        return this._id===null || this._id ===undefined || this._id === "";
    }
}
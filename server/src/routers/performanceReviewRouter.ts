import express, { Request, Response, NextFunction, Router } from 'express';
import { PerformanceReviewManager } from '../managers/performanceReviewManager';
import { AppPerformanceReview, AppPerformanceReviewResult } from '../entities/appPerformanceReview';

import {body} from 'express-validator';
import SchemaValidatorMiddleware from '../middlewares/generalSchemaValidator';
import performanceReviewDetailRouter from './performanceReviewDetailRouter';
import { AppError } from '../middlewares/appErrorMiddleware';
import { networkInterfaces } from 'os';
import { AppPerformanceReviewDetail } from 'entities/appPerformanceReviewDetail';

const performanceReviewSchemaValidatorRules = [
    body('employeeID').isMongoId(),
    body('evaluatorID').isMongoId(),
    body('jobPositionID').isMongoId(),
    body('companyID').isMongoId(),
    body('date').isISO8601(),
    body('period').isLength({ min: 2 }),
    body('result').isIn(AppPerformanceReviewResult.getAll()),
];
const performanceReviewCompleteSchemaValidator = [
    body('result').isIn(AppPerformanceReviewResult.getAll()),
    body('resultComment').isString(),
    body('details').isArray(),
    body('details.*.weighing').isNumeric(),
    body('details.*.skill').isString(),
    body('details.*.reference').isString(),
    body('details.*.axis').isString(),    
    body('details.*.employeeAssessment').isNumeric(),
    body('details.*.employeeComments').isString(),    
    body('details.*.evaluatorAssessment').isNumeric(),
    body('details.*.evaluatorComments').isString(),
    body('details.*.responsibilityID').isString()
]

var performanceReviewRouter:Router = express.Router({mergeParams:true});

performanceReviewRouter.use('/:performanceReviewID/details', performanceReviewDetailRouter);


performanceReviewRouter.route('/:performanceReviewID/complete')
.post(performanceReviewCompleteSchemaValidator, SchemaValidatorMiddleware)
.post(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const details = req.body.details as AppPerformanceReviewDetail[]
        details.forEach(det=>{det.performanceReviewID=req.params.performanceReviewID})

        await PerformanceReviewManager.completePerformanceReview(
            req.params.performanceReviewID,
            req.body.result,
            req.body.resultComment,
            details
        )
        res.status(200).json({
            result:true
        })
    }catch(error){next(error)}
})

performanceReviewRouter.route('/:performanceReviewID')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const appPerformanceReview:AppPerformanceReview = await PerformanceReviewManager.getPerformanceReview(req.params.performanceReviewID);
        res.status(200).send(appPerformanceReview);
    }catch(error){next(error);}
})
.delete(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const appPerformanceReview:AppPerformanceReview = await PerformanceReviewManager.deletePerformanceReview(req.params.performanceReviewID);
        res.status(200).send(appPerformanceReview);
    }catch(error){next(error);}
})
.put(performanceReviewSchemaValidatorRules, SchemaValidatorMiddleware)
.put(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const performanceReview:AppPerformanceReview = await PerformanceReviewManager.getPerformanceReview(req.params.performanceReviewID);
        const performanceReviewEdited:AppPerformanceReview = await PerformanceReviewManager.updatePerformanceReview({
            ...performanceReview,
            ...req.body
        });
        res.status(200).send(performanceReviewEdited);
    }catch(error){next(error);}
});

performanceReviewRouter.route('/')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const performanceReviewList:Array<AppPerformanceReview> = req.params.companyID
        ? await PerformanceReviewManager.getByCompanyID(req.params.companyID)
        : await PerformanceReviewManager.getList();
        res.status(200).send(performanceReviewList);
    }catch(error){next(error);}
})
.post(performanceReviewSchemaValidatorRules, SchemaValidatorMiddleware)
.post(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const newPerformanceReview:AppPerformanceReview = await PerformanceReviewManager.addPerformanceReview({
            ...req.body,
            _id:"",        
        });
        res.status(200).send(newPerformanceReview);
    }catch(error){next(error);}
});


export default performanceReviewRouter;
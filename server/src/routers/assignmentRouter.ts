import express, { Request, Response, NextFunction } from 'express';
import { SecurityManager } from '../managers/securityManager'
import { body } from 'express-validator';
import SchemaValidatorMiddleware from '../middlewares/generalSchemaValidator';
import { AssignmentManager } from '../managers/assignmentManager'
import { AppAssignment } from '../entities/appAssignment';



const assignmentSchemaValidatorRules = [
    body('jobPositionID').isLength({ min: 24 }),
    body('employeeID').isLength({ min: 24 }),
    body('employeeEvaluatorID').isLength({ min: 24 }),
    body('assignmentDate').isDate(),
    body('suggestedReviewDate').isDate(),
];

var AssignmentRouter = express.Router({mergeParams:true});

AssignmentRouter.route('/:assignment_id')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const assignment:AppAssignment=await AssignmentManager.getAssignment(req.params.assignment_id);
        return res.status(200).send(assignment);
    }catch(error){next(error);}
})
.put(assignmentSchemaValidatorRules, SchemaValidatorMiddleware)
.put(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const toUpdateAsignment:AppAssignment=await AssignmentManager.getAssignment(req.params.assignment_id);
        const updatedAssignment:AppAssignment=await AssignmentManager.updateAssignment({...toUpdateAsignment, ...req.body} as AppAssignment);
        return res.status(200).send(updatedAssignment);
    }catch(error){next(error);}
})
.delete(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const deletedAssignment:AppAssignment=await AssignmentManager.removeAssignment(req.params.assignment_id);
        res.status(200).send(deletedAssignment);
    }catch(error){next(error);}
})

AssignmentRouter.route('/')
.get(async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const assignments:AppAssignment[] = await AssignmentManager.getByEmployeeID(req.params.employee_id)
        return res.status(200).send(assignments)
    }catch(error){next(error)}
})
.post(assignmentSchemaValidatorRules, SchemaValidatorMiddleware)
.post(async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const newAssignment:AppAssignment=await AssignmentManager.addAssignment({...req.body, _id:""} as AppAssignment);
        return res.status(200).send(newAssignment);
    }catch (error) {next(error);}
})

export default AssignmentRouter;
import express, { Request, Response, NextFunction } from 'express';
import {body} from 'express-validator';

import SchemaValidatorMiddleware from '../middlewares/generalSchemaValidator';

import { AppJobPosition }   from '../entities/appJobPosition';
import { AppCompany }       from '../entities/appCompany';

import JobPositionRouter    from './jobPositionRouter';
import UserRouter           from './userRouter';
import Performancerouter    from './performanceReviewRouter'
import ResponsibilityRouter from './jobPositionResponsibilityRouter'

import {CompanyManager}     from '../managers/companyManager'
import {JobPositionManager} from '../managers/jobPositionManager'

var CompanyRouter = express.Router();

const companySchemaValidatorRules = [
    body('name').isLength({ min: 5 }),
    body('active').isBoolean()
];


CompanyRouter.use('/:companyID/jobpositions', JobPositionRouter)
CompanyRouter.use('/:companyID/employees', UserRouter)
CompanyRouter.use('/:companyID/performances', Performancerouter)
CompanyRouter.use('/:companyID/responsibilities', ResponsibilityRouter)

CompanyRouter.route('/')
.get(async (req: Request, res: Response,next:NextFunction)=>{
    try{
        const companyList:Array<AppCompany> = await CompanyManager.getList()
        res.status(200).send(companyList);
    }catch(error){next(error);}
})
.post(companySchemaValidatorRules, SchemaValidatorMiddleware)
.post(async (req: Request, res:Response, next:NextFunction)=>{
    try {
        const company:AppCompany=new AppCompany(req.body.name, req.body.active);
        const newCompany = await CompanyManager.addCompany(company);
        res.status(200).send(newCompany);
    } catch (appError) {
        next(appError);
    }
});

CompanyRouter.route('/active')
.get(async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const companyList:AppCompany[] = await CompanyManager.getActive();
        res.status(200).send(companyList);
    }catch(error){next(error);}
})

CompanyRouter.route('/:companyID')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const company:AppCompany=await CompanyManager.getCompany(req.params.companyID)
        res.status(200).send(company);
    }catch(error){next(error);}
})
.put(companySchemaValidatorRules, SchemaValidatorMiddleware)
.put(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const updatedCompany:AppCompany= await CompanyManager.updateCompany(req.params.companyID,req.body as AppCompany);
        res.status(200).send(updatedCompany);
    }catch(error){next(error);}
})
.delete(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const deletedCompany:AppCompany=await CompanyManager.deleteCompany(req.params.companyID);
        res.status(200).send(deletedCompany);
    }catch(error){next(error);}
})

/*CompanyRouter.route('/:companyId/employees')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const employees:AppUser[]=await UserManager.getByCompany(req.params.companyId)
        res.status(200).send(employees);
    }catch(error){next(error);}
})*/

CompanyRouter.route('/:companyID/jobpositions')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const jobpositions:AppJobPosition[]=await JobPositionManager.getByCompany(req.params.companyID)
        res.status(200).send(jobpositions)
    }catch(error){next(error);}
})

CompanyRouter.route('/:companyID/evaluators')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        res.status(200).send(await CompanyManager.getEvaluators(req.params.companyID))
    }catch(error){next(error);}
})
export default CompanyRouter;
import express, { Request, Response, NextFunction } from 'express';
import {body}                                       from 'express-validator';

import { AppUser }                  from '../entities/appUser';
import {UserManager}                from '../managers/userManager'
import {SecurityManager}            from '../managers/securityManager'
import SchemaValidatorMiddleware    from '../middlewares/generalSchemaValidator';
import { AppError }                 from '../middlewares/appErrorMiddleware';


import assignmentRouter from './assignmentRouter'


var UserRouter = express.Router({mergeParams:true});

const userAddSchemaValidatorRules = [
    body('fullName').isLength({ min: 4 }),
    body('email').isEmail(),
    body('documentIDs').isArray(),
    body('comments').isArray()
];
const userUpdateSchemaValidatorRules = [
    body('_id').isMongoId(),
    ...userAddSchemaValidatorRules
];



UserRouter.use('/:employee_id/assignments',assignmentRouter);

UserRouter.route('/')
.get( async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const userList:AppUser[] = req.params.companyID 
            ? await UserManager.getByCompany(req.params.companyID)
            : await UserManager.getList()
        res.status(200).send(userList)
    }catch(error){next(error)}
})
.post(userAddSchemaValidatorRules, SchemaValidatorMiddleware)
.post( async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const nUser:AppUser = {
            ...req.body, 
            passwordHash:await SecurityManager.getPasswordHashFromLiteral("ChangeMe123"),
            comments:[]
        } as AppUser
        res.status(200).send(await UserManager.addUser(nUser))    
    }catch(error){next(error)}
})

UserRouter.route('/active')
.get( async (_req: Request, res: Response, next:NextFunction)=>{
    UserManager.getActive()
    .then((usersList:Array<AppUser>)=>res.status(200).send(usersList))
    .catch((appError:AppError)=>next(appError));
})

UserRouter.route('/:user_id')
.get( async (req:Request, res:Response, next:NextFunction)=>{
    try{
        res.status(200).send(await UserManager.getById(req.params.user_id))
    }catch(error){next(error)}
})
.put(userUpdateSchemaValidatorRules, SchemaValidatorMiddleware)
.put( async (req:Request, res:Response, next:NextFunction)=>{
    try{
        res.status(200).send(await UserManager.updateUser({...req.body, _id:req.params.user_id} as AppUser))
    }catch(error){next(error)}
})
.delete( async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const deletedUser:AppUser=await UserManager.deleteUser(req.params.user_id)
        res.status(200).send(deletedUser)
    }catch(error){next(error)}
})
export default UserRouter;
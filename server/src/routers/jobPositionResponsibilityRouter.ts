import express, { Request, Response, NextFunction } from 'express';
import { body }                                     from 'express-validator';

import SchemaValidatorMiddleware                from '../middlewares/generalSchemaValidator';
import { AppError }                             from '../middlewares/appErrorMiddleware';
import { JobPositionResponsibilityManager }     from '../managers/jobPositionResponsibilityManager';
import { CompanyManager }                       from '../managers/companyManager'
import { AppJobPositionResponsibility, 
    AppJobPositionResponsibilityRisk, 
    AppJobPositionResponsibilityPeriodicity }   from '../entities/appJobPositionResponsibility'



const jobPositionResponsibilityValidatorRules = [
    body('companyID').isMongoId(),
    body('controlledBy').isMongoId(),
    body('details').isLength({ min: 5 }),
    body('isHomeOffice').isBoolean(),
    body('periodicity').isNumeric(),
    body('risk').isNumeric(),
    body('referenceComments').isLength({ min: 5 }),
    body('weighing').isNumeric(),
    body('documentIDs').isArray(),
    body('days').isNumeric()
];

var JobPositionResponsibilityRouter = express.Router({mergeParams:true});

JobPositionResponsibilityRouter.route('/:responsibilityId')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const jobPositionResponsibility:AppJobPositionResponsibility|null=await JobPositionResponsibilityManager.getResponsibility(req.params.responsibilityId);
        if(jobPositionResponsibility===null) return next(AppError.DoesntExist.setExtraData({_id:req.params.responsibilityId}));
        if(jobPositionResponsibility.appJobPositionID!==req.params.jobPosId) 
            return next(AppError.ElementMalformed.setExtraData({
                jobPositionID:req.params.jobPosId, 
                responsibilityJobPositionID:jobPositionResponsibility.appJobPositionID
            }))
        return res.status(200).send(jobPositionResponsibility);
    }catch(error){next(error);}
})
.delete(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const jobPositionResponsibility:AppJobPositionResponsibility=await JobPositionResponsibilityManager.deleteResponsibility(req.params.responsibilityId);
        return res.status(200).send(jobPositionResponsibility);
    }catch(error){next(error);};
})
.put(jobPositionResponsibilityValidatorRules, SchemaValidatorMiddleware)
.put(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const args={
            ...req.body,
            periodicity:{
                ...AppJobPositionResponsibilityPeriodicity.getById(req.body.periodicity),
                days:req.body.days
            } as AppJobPositionResponsibilityPeriodicity,
            risk:AppJobPositionResponsibilityRisk.getById(req.body.risk)
        };
        const jobPositionResponsibility:AppJobPositionResponsibility|null=await JobPositionResponsibilityManager.getResponsibility(req.params.responsibilityId);
        if(jobPositionResponsibility===null) return next(AppError.DoesntExist.setExtraData({_id:req.params.responsibilityId}));
        const jobPositionResponsibilityEdited:AppJobPositionResponsibility=await JobPositionResponsibilityManager.updateResponsibility({...jobPositionResponsibility, ...args } as AppJobPositionResponsibility);
        return res.status(200).send(jobPositionResponsibilityEdited);
    }catch(error){next(error);}
});

JobPositionResponsibilityRouter.route('/')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        await CompanyManager.getCompany(req.params.companyID)
        return res.status(200).send(await JobPositionResponsibilityManager.getList(req.params.companyID))
    }catch(error){next(error);}
})
.post(jobPositionResponsibilityValidatorRules, SchemaValidatorMiddleware)
.post(async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const args={
            ...req.body, 
            periodicity:{
                ...AppJobPositionResponsibilityPeriodicity.getById(req.body.periodicity),
                days:req.body.days
            } as AppJobPositionResponsibilityPeriodicity, 
            risk:AppJobPositionResponsibilityRisk.getById(req.body.risk)
        };
        const jobPositionResponsibility:AppJobPositionResponsibility=await JobPositionResponsibilityManager.addResponsibility({indicator:"",appJobPositionID:req.params.jobPosId??null,...args, _id:""} as AppJobPositionResponsibility);
        return res.status(200).send(jobPositionResponsibility);
    }catch(error){next(error);}
})

export default JobPositionResponsibilityRouter;
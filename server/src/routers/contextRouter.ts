import express, { Request, Response, NextFunction } from 'express';

import { AppDocument } from 'entities/appDocuments'

import { DocumentsManager } from '../managers/documentsManager'

export const ContextRouter = express.Router();

ContextRouter.route('/homeContext').get(async (req: Request, res: Response,next:NextFunction)=>{
    try{

        res.status(200).send({
            relatedDocuments:await DocumentsManager.getRelatedToUser()
        })
    }catch(error){next(error)}
})

import express, { Request, Response, NextFunction } from 'express';
import {body}                       from 'express-validator';

import SchemaValidatorMiddleware    from '../middlewares/generalSchemaValidator';
import { DocumentsManager }         from '../managers/documentsManager'
import { AppDocument }              from '../entities/appDocuments'

const DocumentsRouter=express.Router()

const documentValidatorRules = [
    body('title').isLength({ min: 5 }),
    body('link').isLength({ min: 10 }),
    body('companyID').isLength({ min: 24 }),
];

DocumentsRouter.route('/:docID')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const doc=await DocumentsManager.getById(req.params.docID)
        res.status(200).send(doc)
    }catch(error){next(error);}
})
.put(documentValidatorRules, SchemaValidatorMiddleware)
.put(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const doc=await DocumentsManager.updateDocument({...req.body, _id:req.params.docID} as AppDocument)
        res.status(200).send(doc)
    }catch(error){next(error);}
})
.delete(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const doc=await DocumentsManager.deleteDocument(req.params.docID)
        res.status(200).send(doc)
    }catch(error){next(error);}
})

DocumentsRouter.route('/')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const docList = await DocumentsManager.getByCompany(req.query['companyID'] as string)
        res.status(200).send(docList)        
    }catch(error){next(error);}
})
.post(documentValidatorRules, SchemaValidatorMiddleware)
.post(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const doc=await DocumentsManager.addDocument({...req.body, _id:""})
        res.status(200).send(doc)
    }catch(error){next(error);}
})

export default DocumentsRouter
import express, { Request, Response, NextFunction } from 'express';
import { body } from 'express-validator';;
import SchemaValidatorMiddleware from '../middlewares/generalSchemaValidator';
import { JobPositionManager } from '../managers/jobPositionManager'
import { AppJobPosition } from '../entities/appJobPosition';

import { AppError } from '../middlewares/appErrorMiddleware';


const jobPositionSchemaValidatorRules = [
    body('name').isLength({ min: 5 }),
    body('process').isLength({ min: 5 }),
    body('companyID').isMongoId(),
];

var JobPositionRouter = express.Router({mergeParams:true});

JobPositionRouter.route('/:jobPosId')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const jobPosition:AppJobPosition|null=await JobPositionManager.getJobPosition(req.params.jobPosId);
        if(jobPosition===null) return next(AppError.DoesntExist.setExtraData({_id:req.params.jobPosId}));
        return res.status(200).send(jobPosition);
    }catch(error){next(error);}
})
.delete(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const jobPosition:AppJobPosition=await JobPositionManager.deleteJobPosition(req.params.jobPosId);
        return res.status(200).send(jobPosition);
    }catch(error){next(error);}
})
.put(jobPositionSchemaValidatorRules, SchemaValidatorMiddleware)
.put(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const jobPosition:AppJobPosition|null=await JobPositionManager.getJobPosition(req.params.jobPosId);
        if(jobPosition===null) return next(AppError.DoesntExist.setExtraData({_id:req.params.jobPosId}));
        const jobPositionEdited:AppJobPosition=await JobPositionManager.updateJobPosition({...jobPosition, ...req.body} as AppJobPosition);
        return res.status(200).send(jobPositionEdited);
    }catch(error){next(error);}
});

JobPositionRouter.route('/')
.get(async (req: Request, res: Response, next:NextFunction)=>{
    try{
        const jobPositionList:AppJobPosition[] = req.params.companyID
            ? await JobPositionManager.getByCompany(req.params.companyID)
            : await JobPositionManager.getList();
        return res.status(200).send(jobPositionList);
    }catch(error){next(error);}
})
.post(jobPositionSchemaValidatorRules, SchemaValidatorMiddleware)
.post(async (req: Request, res: Response, next:NextFunction)=>{    
    try{
        const appJobPosition:AppJobPosition=await JobPositionManager.addJobPosition({ ...req.body, _id:"" } as AppJobPosition);
        return res.status(200).send(appJobPosition);
    }catch(error){next(error);}
});

export default JobPositionRouter;
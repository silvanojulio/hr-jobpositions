import express, { Request, Response, NextFunction, Router } from 'express';
import {body} from 'express-validator';
import SchemaValidatorMiddleware from '../middlewares/generalSchemaValidator';
import performanceReviewRouter from './performanceReviewRouter';
import { AppPerformanceReviewDetail } from '../entities/appPerformanceReviewDetail';
import { PerformanceReviewDetailManager } from '../managers/performanceReviewDetailManager';
import { AppPerformanceReview } from '../entities/appPerformanceReview';
import { PerformanceReviewManager } from '../managers/performanceReviewManager';

const performanceReviewDetailValidatorRules = [
    body('performanceReviewID').isLength({ min: 24 }),
    body('weighing').isNumeric(),
    body('skill').isLength({ min: 10 }),
    body('reference').isLength({ min: 5 }),
    body('axis').isLength({ min: 2 }),
    body('employeeAssessment').isNumeric(),
    body('employeeComments').isLength({ min: 5 }),
];

var performanceReviewDetailRouter:Router = express.Router({mergeParams:true});

performanceReviewDetailRouter.route('/:detialID')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const performanceReviewDetail:AppPerformanceReviewDetail = await PerformanceReviewDetailManager.getPerformanceReviewDetail(req.params.detialID);
        res.status(200).send(performanceReviewDetail);
    }catch(error){next(error);}
})
.delete(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const deletedPerformanceReviewDetail:AppPerformanceReviewDetail = await PerformanceReviewDetailManager.deletePerformancereviewdetail(req.params.detialID);
        res.status(200).send(deletedPerformanceReviewDetail);
    }catch(error){next(error);}
})
.put([...performanceReviewDetailValidatorRules,
    body('evaluatorAssessment').isNumeric(),
    body('evaluatorComments').isLength({ min: 5 }),] , SchemaValidatorMiddleware)
.put(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const performanceReviewDetail:AppPerformanceReviewDetail = await PerformanceReviewDetailManager.getPerformanceReviewDetail(req.params.detialID);
        const editedPerformanceReviewDetail:AppPerformanceReviewDetail = await PerformanceReviewDetailManager.updatePerformanceReviewDetail({
            ...performanceReviewDetail,
            ...req.body,
            performanceReviewID:req.params.performanceReviewID,
            _id:performanceReviewDetail._id
        });
        res.status(200).send(editedPerformanceReviewDetail);
    }catch(error){next(error);}
});

performanceReviewDetailRouter.route('/')
.get(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const performanceReview:AppPerformanceReview = await PerformanceReviewManager.getPerformanceReview(req.params.performanceReviewID);
        const listPerformanceReviewDetail:Array<AppPerformanceReviewDetail> = await PerformanceReviewDetailManager.getList(performanceReview);
        res.status(200).send(listPerformanceReviewDetail)
    }catch(error){next(error);}
})
.post(performanceReviewDetailValidatorRules, SchemaValidatorMiddleware)
.post(async (req:Request, res:Response, next:NextFunction)=>{
    try{
        const newPerformanceReviewDetail:AppPerformanceReviewDetail = await PerformanceReviewDetailManager.addPerformanceReviewDetail({
            ...req.body,
            performanceReviewID:req.params.performanceReviewID,
            _id:"",
        });
        res.status(200).send(newPerformanceReviewDetail);
    }catch(error){next(error);}
})

export default performanceReviewDetailRouter;
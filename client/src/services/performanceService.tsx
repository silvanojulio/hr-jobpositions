import ApiClient, {ApiRequest} from 'utils/apiClient';
import { AppPerformanceReview, AppPerformanceReviewResult } from 'entities/appPerformanceReview'
import { AppPerformanceReviewDetail } from 'entities/appPerformanceReviewDetail';

export default class PerformanceServices{    
    private static apiBase:string ="performance/"

    public static async savePerformance(performance:AppPerformanceReview):Promise<AppPerformanceReview>{
        const req = new ApiRequest()
        req.url=`/api/${this.apiBase}/${performance._id}`
        req.method=performance._id===""?"post":"put"
        req.body=performance

        const response = await ApiClient.call(req)
        return response.data as AppPerformanceReview
    }

    public static async deletePerformance(performance:AppPerformanceReview):Promise<AppPerformanceReview>{
        const req=new ApiRequest()
        req.url=`/api/${this.apiBase}/${performance._id}`
        req.method="delete"
        
        const response = await ApiClient.call(req)
        return response.data as AppPerformanceReview
    }
    public static async completePerformanceDetails(
        performanceId:string,
        details:AppPerformanceReviewDetail[],
        result:AppPerformanceReviewResult,
        resultComment:string
        ){
        const req=new ApiRequest()
        req.url=`/api/${this.apiBase}/${performanceId}/complete`
        req.method="post"
        req.body={result, resultComment, details}
        await ApiClient.call(req)
    }
    public static async getCurrentDetails(performanceId:string){
        const req=new ApiRequest()
        req.url=`/api/${this.apiBase}/${performanceId}/details`
        req.method="get"
        const response = await ApiClient.call(req)
        return response.data as AppPerformanceReviewDetail[]
    }
}
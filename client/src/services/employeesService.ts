import ApiClient, {ApiRequest} from '../utils/apiClient';
import { AppUser } from 'entities/appUser'
import { AppAssignment } from 'entities/appAssignmnet';

export default class EmployeesService{
    private static baseURl:string="/api/users/"

    static async getList():Promise<AppUser[]>{
        const req=new ApiRequest()
        req.url = `${this.baseURl}`
        req.method='get'
        req.body={}

        const res= await ApiClient.call(req)
        return res.data as AppUser[]
    }
    static async getEmployee(employeeID:string):Promise<AppUser>{
        const req=new ApiRequest()
        req.url = `${this.baseURl}${employeeID}`
        req.method='get'
        req.body={}

        const res= await ApiClient.call(req)
        return res.data as AppUser
    }
    static async saveEmployee(employee:AppUser):Promise<AppUser>{
        const req = new ApiRequest();
        req.url = `${this.baseURl}${employee._id}`
        req.method = employee._id!==""?'put':"post";
        req.body =  employee;
        const response = (await ApiClient.call(req));
        
        return response.data as AppUser;
    }
    static async deleteEmployee(employee:AppUser):Promise<AppUser>{
        
        const req = new ApiRequest();
        req.url = `${this.baseURl}${employee._id}`
        req.method = "delete";
        req.body =  {};
        const response = (await ApiClient.call(req));

        return response.data as AppUser;
    }
    static async getAssignments(employeeID:string):Promise<AppAssignment[]>{
        const req=new ApiRequest()
        req.url=`${this.baseURl}${employeeID}/assignments`
        req.method='get'
        req.body={}

        const res= await ApiClient.call(req)

        return res.data as AppAssignment[]
    }
}
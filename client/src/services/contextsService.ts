import ApiClient, {ApiRequest} from 'utils/apiClient'
import { HomeContextVars } from 'context/homeContext'


export class ContextsService{
    private static apiBase:string ="contexts/"

    public static async getHomeContext():Promise<HomeContextVars>{
        const req = new ApiRequest()
        req.url=`/api/${this.apiBase}homeContext`
        req.method="get"
        
        const response = (await ApiClient.call(req));
        return response.data as HomeContextVars
    }
}
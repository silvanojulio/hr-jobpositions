import ApiClient, {ApiRequest} from '../utils/apiClient';
import { AppJobPositionResponsibility } from 'entities/appJobPositionResponsibility';


export default class ResponsibilitiesService{
    private static apiBase:string ="responsibilities/"

    static async getList(companyID:string):Promise<AppJobPositionResponsibility[]> {
        const req = new ApiRequest()
        req.url = `/api/companies/${companyID}/${this.apiBase}`
        req.method = 'get'
        req.body =  {}
        
        const response = (await ApiClient.call(req))

        return response.data as AppJobPositionResponsibility[]
    }

    static async saveResponsibility(responsibility:AppJobPositionResponsibility):Promise<AppJobPositionResponsibility>{

        const req = new ApiRequest()
        req.url = `/api/companies/${responsibility.companyID}/${this.apiBase}${responsibility._id}`
        req.method = responsibility._id!==""?'put':"post"
        req.body =  {...responsibility, risk:responsibility.risk?.id, periodicity:responsibility.periodicity?.id, days:responsibility.periodicity?.days??0}

        const response = (await ApiClient.call(req))

        return response.data as AppJobPositionResponsibility
    }

    static async deleteResponsibility(responsibility:AppJobPositionResponsibility):Promise<AppJobPositionResponsibility>{
        
        const req = new ApiRequest()
        req.url = `/api/companies/${responsibility.companyID}/${this.apiBase}${responsibility._id}`
        req.method = "delete"
        req.body =  responsibility

        const response = (await ApiClient.call(req))

        return response.data as AppJobPositionResponsibility
    }

}
export {ResponsibilitiesService}
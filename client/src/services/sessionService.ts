import { AppSession } from './../entities/appSession';
import AppStore from '../utils/appStorage';
import ApiClient, { ApiRequest } from 'utils/apiClient';
import { AppUser } from 'entities/appUser';

export default class SessionService {

    static async getCurrentSession(): Promise<AppSession | null> {
        try {
            const session = await AppStore.getObject<AppSession>('CURRENT_SESSION')
            return session
        } catch (error) {
            return null
        }
    }
    
    static async changepass(email:string, old_pass:string, new_pass:string):Promise<AppUser>{
        const req = new ApiRequest()
        req.url = '/api/session/changepass'
        req.method = 'put'
        req.body={email, old_pass, new_pass}

        const appUser = await ApiClient.call(req)

        return (appUser.data as AppUser)
    }
}
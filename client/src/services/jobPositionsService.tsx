import ApiClient, {ApiRequest} from '../utils/apiClient';
import { AppJobPosition } from '../entities/appJobPosition';
import { AppJobPositionResponsibility } from 'entities/appJobPositionResponsibility';
import { AppUser } from 'entities/appUser';
import CompaniesService from 'services/companiesService'

export default class JobPositionService{
    private static apiBase:string ="jobpositions/"

    static async getList():Promise<Array<AppJobPosition>> {
        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}`
        req.method = 'get'
        req.body =  {}
        
        const response = (await ApiClient.call(req))

        return response.data as Array<AppJobPosition>
    }

    static async saveJobPosition(jobPosition:AppJobPosition):Promise<AppJobPosition>{

        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}${jobPosition._id}`
        req.method = jobPosition._id!==""?'put':"post"
        req.body =  jobPosition

        const response = (await ApiClient.call(req))

        return response.data as AppJobPosition
    }

    static async deleteJobPosition(jobPosition:AppJobPosition):Promise<AppJobPosition>{
        
        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}${jobPosition._id}`
        req.method = "delete"
        req.body =  jobPosition

        const response = (await ApiClient.call(req))

        return response.data as AppJobPosition
    }
    static async getById(jobPositionID:string):Promise<AppJobPosition>{
        
        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}${jobPositionID}`
        req.method = 'get'
        req.body =  {}
        
        const response = (await ApiClient.call(req))

        return response.data as AppJobPosition
    }

    static async getCoworkers(jobPositionID:string):Promise<AppUser[]>{
        const jobPosition:AppJobPosition=await this.getById(jobPositionID)
        const coworkers:AppUser[]=await CompaniesService.getCompanyEmployees(jobPosition.companyID)
        return coworkers
    }
    static async getResponsibilities(jobPosition:AppJobPosition):Promise<AppJobPositionResponsibility[]>{
        
        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}${jobPosition._id}/responsibilities`
        req.method = "get"
        req.body =  {}

        const response = (await ApiClient.call(req))
        
        return response.data as AppJobPositionResponsibility[]
    }
}
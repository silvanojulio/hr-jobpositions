import ApiClient, {ApiRequest} from 'utils/apiClient';
import { AppCompany } from 'entities/appCompany';
import { AppJobPosition } from 'entities/appJobPosition';
import { AppUser } from 'entities/appUser';
import { AppPerformanceReview } from 'entities/appPerformanceReview';

export default class CompaniesService{
    private static apiBase:string ="companies/"
    static async getList():Promise<AppCompany[]> {
        const req = new ApiRequest();
        req.url = `/api/${this.apiBase}`;
        req.method = 'get';
        req.body =  {};
        
        const response = (await ApiClient.call(req));

        return response.data as AppCompany[];
    }
    static async getActive():Promise<Array<AppCompany>> {
        const req = new ApiRequest();
        req.url = `/api/${this.apiBase}active`;
        req.method = 'get';
        req.body =  {};
        
        const response = (await ApiClient.call(req));

        return response.data as Array<AppCompany>;
    }
    static async saveCompany(company:AppCompany):Promise<AppCompany>{

        const req = new ApiRequest();
        req.url = `/api/${this.apiBase}${company._id}`;
        req.method = company._id!==""?'put':"post";
        req.body =  company;

        const response = (await ApiClient.call(req));

        return response.data as AppCompany;
    }

    static async deleteCompany(company:AppCompany):Promise<AppCompany>{
        
        const req = new ApiRequest();
        req.url = `/api/${this.apiBase}${company._id}`;
        req.method = "delete";
        req.body =  company;

        const response = (await ApiClient.call(req));

        return response.data as AppCompany;
    }

    static async getCompanyEmployees(companyID:string):Promise<AppUser[]>{
        const req=new ApiRequest()
        req.url=`/api/${this.apiBase}${companyID}/employees`
        req.method='get'
        req.body={}

        const res= await ApiClient.call(req)
        return res.data as AppUser[]
    }
    
    static async getCompanyJobPositions(companyID:string):Promise<AppJobPosition[]>{
        
        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}${companyID}/jobpositions`
        req.method = "get"
        req.body =  {}

        const response = (await ApiClient.call(req))

        return response.data as AppJobPosition[]

    }

    static async getEvaluators(companyID:string):Promise<AppUser[]>{
        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}${companyID}/evaluators`
        req.method = "get"

        const response = (await ApiClient.call(req))

        return response.data as AppUser[]
    }
 
    static async getCompanyPerformances(companyID:string):Promise<AppPerformanceReview[]>{
        
        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}${companyID}/performances`

        const response = await ApiClient.call(req)

        return response.data as AppPerformanceReview[]
    }
}
import { AppSession } from './../entities/appSession';
import ApiClient, {ApiRequest} from '../utils/apiClient';

export default class LoginService {

    static async login(username:string, password:string): Promise<AppSession> {
        const req = new ApiRequest();
        req.url = '/api/session/login';
        req.method = 'post';
        req.body =  {
            email: username,
            password: password,
        };

        const session = await ApiClient.call(req);

        return (session.data as AppSession);
    }
}
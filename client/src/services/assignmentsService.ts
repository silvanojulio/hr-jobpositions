import { AppAssignment } from "entities/appAssignmnet";
import ApiClient, { ApiRequest } from "utils/apiClient";


export default class AssignmnetsService{
    private static apiBase:string ="/api/assignments/"
    static async saveAssignment(assignment:AppAssignment):Promise<AppAssignment>{
        const req = new ApiRequest()
        req.url = `${this.apiBase}${assignment._id}`
        req.method = assignment._id===""?"post":"put"
        req.body = assignment

        const response = await ApiClient.call(req)
        return response.data as AppAssignment
    }
    static async deleteAssignment(assignment:AppAssignment):Promise<AppAssignment>{
        const req = new ApiRequest()
        req.url = `${this.apiBase}${assignment._id}`
        req.method = "delete"
        
        const response = await ApiClient.call(req)
        return response.data as AppAssignment
    }

}
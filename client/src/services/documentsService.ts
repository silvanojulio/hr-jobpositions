import ApiClient, {ApiRequest} from 'utils/apiClient'
import { AppDocument } from 'entities/appDocuments'

export default class DocumentsService{
    private static apiBase:string ="documents/"

    static async getList(companyID:string):Promise<AppDocument[]> {
        const req = new ApiRequest();
        req.url = `/api/${this.apiBase}`;
        req.method = 'get';
        req.body =  {};
        req.params = {companyID}
        
        const response = (await ApiClient.call(req));

        return response.data as AppDocument[];
    }

    static async save(doc:AppDocument):Promise<AppDocument>{
        const req = new ApiRequest()
        req.url = `/api/${this.apiBase}${doc._id}`
        req.method = doc._id===""?"post":"put"
        req.body = doc
        
        const response = await ApiClient.call(req)

        return response.data as AppDocument
    }
    
    static async delete(doc:AppDocument):Promise<AppDocument>{
        const req = new ApiRequest()
        req.url=`/api/${this.apiBase}${doc._id}`
        req.method="delete"
        
        const res = await ApiClient.call(req)


        return res.data as AppDocument
    }
}
import ApiClient, {ApiRequest} from 'utils/apiClient';
import { AppPerformanceReviewDetail } from 'entities/appPerformanceReviewDetail'

export default class ReviewServices{
    
    private static apiBase:string ="performance/"

    public static async saveReview(review:AppPerformanceReviewDetail):Promise<AppPerformanceReviewDetail>{
        const req = new ApiRequest()
        req.url=`/api/${this.apiBase}/${review.performanceReviewID}/details/${review._id}`
        req.method=review._id===""?"post":"put"
        req.body=review

        const response = await ApiClient.call(req)
        return response.data as AppPerformanceReviewDetail

    }
}
import React, { 
    FC,
    useState, 
    useEffect, 
    useContext, 
    useCallback 
}               from 'react'
import { 
    useHistory, 
    useLocation 
}               from 'react-router-dom'
import moment   from 'moment'

import { ConfirmPrompt }        from 'components/common/ConfirmPrompt'
import { ConfirmPromptProps }   from 'components/common/ConfirmPrompt/confirmPrompt'

import { AppCompany }           from 'entities/appCompany'
import { AppSession }           from 'entities/appSession'
import { AppUser, Roles }       from 'entities/appUser'
import { CompaniesService }     from 'services'
import { SessionService }       from 'services'
import { DocumentsService }     from 'services'
import { useSessionContext }    from 'hooks'
import { AppDocument }          from 'entities/appDocuments'
import { ResponsibilityForm }   from 'components/responsibilities'
import { IResponsibilityForm }  from 'components/responsibilities'

type AppContextType={
    currentCompany:AppCompany|null,
    currentCompanyDocs:AppDocument[],
    companyList:AppCompany[],
    employeeList:AppUser[],
    currentSession:AppSession|null,
    setCurrentCompany:(company:AppCompany)=>Promise<void>,
    setCurrentCompanyDocs:(list:AppDocument[])=>Promise<void>,
    setCompanyList:(list:AppCompany[])=>Promise<void>,
    setEmployeeList:(list:AppUser[])=>Promise<void>,
    changePass:(email:string, old_pass:string, new_pass:string)=>Promise<AppUser>
    ask:(question:string, title:string)=>Promise<void>
    responsibilityForm:React.RefObject<IResponsibilityForm>
}

export const AppContext=React.createContext<AppContextType>({
    currentCompany:null,
    currentCompanyDocs:[],
    companyList:[],
    employeeList:[],
    currentSession:null,
    setCurrentCompany:async ()=>{},
    setCurrentCompanyDocs:async()=>{},
    setCompanyList:async()=>{},
    setEmployeeList:async()=>{},
    changePass:SessionService.changepass,
    ask:async ()=>{},
    responsibilityForm:React.createRef<IResponsibilityForm>()
})

export const AppContextProvider:FC = ({children})=>{
    const context           = useContext(AppContext)
    const currentSession    = useSessionContext()
    const history           = useHistory()
    const location          = useLocation()

    const [companyList, setCompanyList]                 = useState<AppCompany[]>([])
    const [currentCompany, setCurrentCompany]           = useState<AppCompany|null>(null)
    const [currentCompanyDocs, setCurrentCompanyDocs]   = useState<AppDocument[]>([])
    const [employeeList, setEmployeeList]               = useState<AppUser[]>([])
    const [confirm, setConfirm]                         = useState<ConfirmPromptProps>({
        show:false,
        title:"",
        question:""
    })
    const setCurrentCompany_ = useCallback(async(company:AppCompany)=>setCurrentCompany(company), [setCurrentCompany])
    const setCompanyList_ = useCallback(async(list:AppCompany[])=>setCompanyList(list),[])
    const loadCompanyData = useCallback(async(companyID)=>{
        setCurrentCompanyDocs(await DocumentsService.getList(companyID))
        setEmployeeList(await CompaniesService.getCompanyEmployees(companyID))
    },[])
    const ask=async (question:string, title:string):Promise<void> =>{
        const ret=new Promise<void>((resolve, reject)=>{
            setConfirm({question, title, show:true, resolve, reject})
        })
        ret.finally(()=>{
            setConfirm({
                show:false,
                title:"",
                question:""
            })
        })
        return ret
    }
    useEffect(()=>{
        if(!['/','/login'].some(l=>l===location.pathname)){
            if(!currentSession) return
            if(moment(currentSession.expiration)<moment(moment.now())) history.push('/login')
        }
    },[history, location,currentSession])
    useEffect(()=>{
        (async ()=>{
            try{
                if(Roles.SuperAdmin.isEqual(currentSession?.appUser.role??null))
                    setCompanyList(await CompaniesService.getList())
                else
                    setCompanyList(currentSession && currentSession.appUser.company?[currentSession.appUser.company]:[])                
            }catch(error){console.log(error)}
        })()
    },[currentSession])
    useEffect(()=>{
        setCurrentCompanyDocs([])
        setEmployeeList([])
        if(currentCompany) loadCompanyData(currentCompany._id)
    },[currentCompany, loadCompanyData])
    useEffect(()=>{
        if(companyList.length===1) setCurrentCompany(companyList[0])
    },[companyList])
    return (
        <AppContext.Provider value={{
            ...context,
            ask,
            currentCompany,
            currentCompanyDocs,
            companyList,
            employeeList,
            currentSession,
            setCurrentCompany:setCurrentCompany_,
            setCurrentCompanyDocs:async(list:AppDocument[])=>setCurrentCompanyDocs(list),
            setEmployeeList:async(list:AppUser[])=>setEmployeeList(list),
            setCompanyList:setCompanyList_
        }}>
            <ConfirmPrompt {...confirm} />
            <ResponsibilityForm ref={context.responsibilityForm}/>
            {children}
        </AppContext.Provider>
    )
}
import React, { FC, Context, useState, useContext } from 'react'

import { ReviewServices } from 'services'

import { AppPerformanceReviewDetail } from 'entities/appPerformanceReviewDetail'

type ReviewContextType = { 
    currentReview?:AppPerformanceReviewDetail,

    setCurrentReview?:React.Dispatch<React.SetStateAction<AppPerformanceReviewDetail | undefined>>
    saveReview:(review:AppPerformanceReviewDetail)=>Promise<AppPerformanceReviewDetail>
}

export const ReviewContext:Context<ReviewContextType> = React.createContext<ReviewContextType>({
    saveReview:ReviewServices.saveReview
})

export const ReviewContextProvider:FC<{performanceID:string}> = ({children, performanceID})=>{
    const [currentReview, setCurrentReview] = useState<AppPerformanceReviewDetail>()
    const context = useContext(ReviewContext)

    return <ReviewContext.Provider value={{
        ...context,
        currentReview,
        setCurrentReview
    }}>
        {children}
    </ReviewContext.Provider>
}
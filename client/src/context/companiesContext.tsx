import React, { FC, useState, useCallback} from 'react';

import { AppCompany }       from 'entities/appCompany';
import { CompaniesService } from 'services'
import { useAppContext }    from 'hooks/useAppContext'
import { AppError }         from 'utils/apiClient';

type CompanyContextType={
    currentCompany:AppCompany|null,
    companyList:AppCompany[], 
    serviceError:AppError|null,
    setCurrentCompany:(company:AppCompany|null)=>Promise<void>,
    addCompany:()=>Promise<void>,
    saveCompany:(company:AppCompany)=>Promise<void>,
    deleteCompany:(company:AppCompany)=>Promise<void>    
}
export const CompanyContext = React.createContext<CompanyContextType>({
    currentCompany:null,
    companyList:[],
    serviceError:null,
    setCurrentCompany:async()=>{},
    addCompany:async()=>{},
    saveCompany:async()=>{},
    deleteCompany:async()=>{}
});

export const CompanyContextProvider:FC = ({children})=>{    
    const {companyList, setCompanyList} = useAppContext()
    const [currentCompany, setCurrentCompany] = useState<AppCompany|null>(null)
    const [serviceError, setServiceError] = useState<AppError|null>(null)
    const addCompany = useCallback(async()=>{
        setCurrentCompany({_id:"",active:true,name:""})
    },[setCurrentCompany])
    const saveCompany = useCallback(async(company:AppCompany)=>{
        try{
            var savedCompany = await CompaniesService.saveCompany(company)
            if(companyList.some(c=>c._id===savedCompany._id)){
                setCompanyList(companyList.map(c=>c._id===savedCompany._id?savedCompany:c))
            }else{
                setCompanyList([...companyList, savedCompany])
            } 
            setCurrentCompany(null)
        }catch(error){setServiceError(error)}
    },[setCompanyList, companyList])
    const deleteCompany = useCallback(async(company:AppCompany)=>{
        try{
            const deletedCompany = await CompaniesService.deleteCompany(company);
            setCompanyList(companyList.filter(c=>c._id!==deletedCompany._id));
        }catch(error){setServiceError(error)}
    },[setCompanyList, companyList])
    return (
        <CompanyContext.Provider value={{
            companyList:companyList,
            currentCompany:currentCompany, 
            serviceError,
            setCurrentCompany:async(company:AppCompany|null)=>{setCurrentCompany(company)},
            addCompany,
            saveCompany,
            deleteCompany
        }}>
            {children}
        </CompanyContext.Provider>
    )
}

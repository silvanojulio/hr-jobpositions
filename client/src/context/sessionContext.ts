import React from 'react';

import { AppSession } from 'entities/appSession';

export const SessionContext = React.createContext<AppSession|null>(null);

export const SessionContextProvider = SessionContext.Provider;

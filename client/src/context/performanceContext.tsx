import React, { FC, Context, useState, useEffect, useCallback } from 'react'

import { CompaniesService }     from 'services'
import { PerformanceServices }  from 'services'
import { useAppContext }        from 'hooks' 

import { AppCompany } from 'entities/appCompany'
import { AppUser } from 'entities/appUser'
import { AppJobPosition } from 'entities/appJobPosition'
import { AppPerformanceReview, AppPerformanceReviewResult } from 'entities/appPerformanceReview'
import { SchemaError } from 'utils/apiClient'


type PerformanceContextType={
    currentCompany:AppCompany|null,
    currentPerformance:AppPerformanceReview|null,    
    performanceList:AppPerformanceReview[],
    companyEmployeeList:AppUser[],
    companyJobPositionList:AppJobPosition[],
    completePerformance:AppPerformanceReview|null,
    processing:boolean,
    saveErrors:SchemaError[],
    setCurrentPerformance?:React.Dispatch<React.SetStateAction<AppPerformanceReview|null>>,
    setPerformanceList?:React.Dispatch<React.SetStateAction<AppPerformanceReview[]>>,
    setCompanyEmployeeList?:React.Dispatch<React.SetStateAction<AppUser[]>>,
    setCompanyJobPositionList?:React.Dispatch<React.SetStateAction<AppJobPosition[]>>,
    setCompletePerformance?:React.Dispatch<React.SetStateAction<AppPerformanceReview|null>>,
    savePerformance?:()=>Promise<void>,
    addPerformance?:()=>Promise<void>,
    deletePerformance?:(performance:AppPerformanceReview)=>Promise<void>
    editPerformance?:(performance:AppPerformanceReview)=>Promise<void>
}
export const PerformanceContext:Context<PerformanceContextType> = React.createContext<PerformanceContextType>({
    currentCompany:null,
    currentPerformance:null,
    performanceList:[],
    companyEmployeeList:[],
    companyJobPositionList:[],
    completePerformance:null,
    processing:false,
    saveErrors:[]
})

export const PerformanceContextProvider:FC=({children})=>{
    const [saveErrors, setSaveErrors] =useState<SchemaError[]>([])
    const [currentPerformance, setCurrentPerformance] = useState<AppPerformanceReview|null>(null)
    const [performanceList, setPerformanceList] = useState<AppPerformanceReview[]>([])
    const [companyEmployeeList, setCompanyEmployeeList] = useState<AppUser[]>([])
    const [companyJobPositionList, setCompanyJobPositionList] = useState<AppJobPosition[]>([])
    const [processing, setProcessing] = useState<boolean>(false)
    const [completePerformance, setCompletePerformance] = useState<AppPerformanceReview|null>(null)
    const { currentCompany } = useAppContext()
 
    const savePerformance=useCallback(async ()=>{
        if(!currentPerformance || processing)return
        try{
            setProcessing(true)
            const performance_ = await PerformanceServices.savePerformance(currentPerformance)
            setPerformanceList([performance_, ...performanceList.filter(p=>p._id!==performance_._id)])
            setCurrentPerformance(null)
        }catch(error){
            console.log(error)
            switch(error.code){
                case 800:
                    setSaveErrors(error.extradata.errors)
                    break;
            }
        }finally{setProcessing(false)}
    },[currentPerformance, performanceList, processing])
    const addPerformance=useCallback(async()=>{
        if(!currentCompany || processing) return
        setSaveErrors([])
        setCurrentPerformance(new AppPerformanceReview("","","","",currentCompany._id,new Date(),"",AppPerformanceReviewResult.getById(0)))
    },[currentCompany,setCurrentPerformance,processing])
    const deletePerformance=useCallback(async(performance:AppPerformanceReview)=>{
        if(processing) return
        try{
            setProcessing(true)
            const deleted=await PerformanceServices.deletePerformance(performance)
            setPerformanceList(performanceList.filter(p=>p._id!==deleted._id))
        }catch(error){console.log(error)}finally{setProcessing(false)}
    },[performanceList,processing ])
    const editPerformance=useCallback(async(performance:AppPerformanceReview)=>{
        if(processing) return
        setSaveErrors([])
        setCurrentPerformance({...performance})
    },[processing, setCurrentPerformance])
    useEffect(()=>{
        try{
            if(!currentCompany){
                setPerformanceList([])
                return
            }
            (async ()=>setPerformanceList(await CompaniesService.getCompanyPerformances(currentCompany._id)))();
            (async ()=>setCompanyEmployeeList(await CompaniesService.getCompanyEmployees(currentCompany._id)))();
            (async ()=>setCompanyJobPositionList(await CompaniesService.getCompanyJobPositions(currentCompany._id)))();
        }catch(error){console.log(error)}
    },[currentCompany,setPerformanceList])
    
    return(
        <PerformanceContext.Provider value={{
            currentCompany, currentPerformance, performanceList, completePerformance,
            companyEmployeeList, companyJobPositionList, processing, saveErrors,
            setCurrentPerformance, setPerformanceList, setCompanyEmployeeList, 
            setCompanyJobPositionList, addPerformance, savePerformance,
            deletePerformance, editPerformance, setCompletePerformance
        }}>
            {children}
        </PerformanceContext.Provider>
    )
}
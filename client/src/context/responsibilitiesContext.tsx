import React, {Context, FC, useCallback, useEffect, useState} from 'react'

import { ResponsibilitiesService } from 'services'

import { AppUser } from 'entities/appUser'
import { AppJobPositionResponsibility } from 'entities/appJobPositionResponsibility'
import { useAppContext }    from 'hooks'

type responsibilitiesContextType={
    currentJobPositionID:string|null,
    currentResponsibility:AppJobPositionResponsibility|null
    responsibilities:AppJobPositionResponsibility[],
    coworkers:AppUser[],

    setResponsibility:(responsibility:AppJobPositionResponsibility|null)=>Promise<void>
    upsertResponsibility:(responsibility:AppJobPositionResponsibility)=>Promise<void>
    deleteResponsibility:(responsibility:AppJobPositionResponsibility)=>Promise<void>
}

export const ResponsibilitiesContext:Context<responsibilitiesContextType> = React.createContext<responsibilitiesContextType>({
    currentJobPositionID:null,
    currentResponsibility:null,
    responsibilities:[],
    coworkers:[],
    setResponsibility:async()=>{},
    upsertResponsibility:async()=>{},
    deleteResponsibility:async()=>{},
})

export const ResponsibilitiesContextProvider:FC<{jobPositionID?:string}> = ({jobPositionID, children})=>{
    const { currentCompany, employeeList } = useAppContext()
    const [responsibilities, setResponsibilities] = useState<AppJobPositionResponsibility[]>([])
    const [currentResponsibility, setCurrentResponsibility] = useState<AppJobPositionResponsibility|null>(null)
    const loadResponsibilities=useCallback(async()=>{
        if(!currentCompany) return setResponsibilities([])
        try{
            const [_responsibilities ] = [await ResponsibilitiesService.getList(currentCompany._id)]
            setResponsibilities(_responsibilities)
        }catch(error){console.log(error)}
    },[currentCompany])
    useEffect(()=>{loadResponsibilities()}, [loadResponsibilities])
    const setResponsibility = useCallback(async (responsibility:AppJobPositionResponsibility|null)=>{
        setCurrentResponsibility(responsibility?{...responsibility} as AppJobPositionResponsibility:null)
    },[])
    const upsertResponsibility=useCallback(async (responsibility:AppJobPositionResponsibility):Promise<void> =>{
        responsibilities.some((_responsibility:AppJobPositionResponsibility)=>_responsibility._id===responsibility._id)
            ? setResponsibilities(responsibilities.map((_responsibility:AppJobPositionResponsibility)=>_responsibility._id===responsibility._id?responsibility:_responsibility))
            : setResponsibilities([...responsibilities, responsibility])
    },[responsibilities])
    const deleteResponsibility=useCallback(async (responsibility:AppJobPositionResponsibility):Promise<void> =>{
        try{
            const deletedResponsibility:AppJobPositionResponsibility= await ResponsibilitiesService.deleteResponsibility(responsibility)
            setResponsibilities(responsibilities.filter(_responsibility=>_responsibility._id!==deletedResponsibility._id))
        }catch(error){console.log(error)}
    },[responsibilities])
    return (
        <ResponsibilitiesContext.Provider value={{
            currentJobPositionID:jobPositionID??null,
            currentResponsibility,
            responsibilities:responsibilities.filter(res=>(res.appJobPositionID??null)===(jobPositionID??null)),
            coworkers:employeeList,
            setResponsibility,
            upsertResponsibility,
            deleteResponsibility,
        }} >
            {children}
        </ResponsibilitiesContext.Provider>
    )
}
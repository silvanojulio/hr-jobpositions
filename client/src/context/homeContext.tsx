import React, { FC }            from 'react'
import { useCallback, useEffect, useState } from 'react'

import { AppDocument }      from 'entities/appDocuments'
import { useAppContext }    from 'hooks'
import { DocumentsService } from '../services'


export type HomeContextVars={
    relatedDocuments:AppDocument[]
}
type HomeContextMethods={
    
}
type HomeContextType=HomeContextVars & HomeContextMethods

export const HomeContext = React.createContext<HomeContextType>({
    relatedDocuments:[]
})


export const HomeContextProvider:FC = ({children})=>{
    const {currentCompany} = useAppContext()
    const [relatedDocuments, setRelatedDocuments] = useState<AppDocument[]>([])
    const loadContext=useCallback(async()=>{
        if(!currentCompany){
            setRelatedDocuments([])
            return
        }
        const documentos=await DocumentsService.getList(currentCompany._id)
        setRelatedDocuments(documentos.filter(d=>d.global))
    },[currentCompany])
    useEffect(()=>{loadContext()}, [loadContext])
    return <HomeContext.Provider value={{
        relatedDocuments
    }}>{children}</HomeContext.Provider>
}
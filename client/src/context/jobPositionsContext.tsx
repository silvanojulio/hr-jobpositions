import React, { Context, FC, useState, useEffect } from 'react'

import { JobPositionsService }  from 'services'
import { CompaniesService }     from 'services'
import { useAppContext }        from 'hooks'

import { AppJobPosition } from 'entities/appJobPosition'
import { AppCompany } from 'entities/appCompany'
import { AppUser } from 'entities/appUser'
import { AppJobPositionResponsibility } from 'entities/appJobPositionResponsibility'

type JobPositionsContextType={ 
    currentCompany:AppCompany|null,
    companyEmployeesList:AppUser[],
    currentJobPosition:AppJobPosition|null,
    currentResponsibility:AppJobPositionResponsibility|null,
    currentResponsibilityList:AppJobPositionResponsibility[],
    jobPositionList:AppJobPosition[],
    setCurrentJobPosition?:React.Dispatch<React.SetStateAction<AppJobPosition|null>>,
    setCurrentResponsibility?:React.Dispatch<React.SetStateAction<AppJobPositionResponsibility|null>>,
    setCurrentResponsibilityList?:React.Dispatch<React.SetStateAction<AppJobPositionResponsibility[]>>,
    setJobPositionList?:React.Dispatch<React.SetStateAction<AppJobPosition[]>>,
    updateOnList?:(jopPosition:AppJobPosition)=>Promise<void>,
    saveJobPosition?:(jobPosition:AppJobPosition)=>Promise<void>,
    deleteJobPosition?:(jobPosition:AppJobPosition)=>Promise<void>
}
export const JobPositionsContext:Context<JobPositionsContextType> =React.createContext<JobPositionsContextType>({
    currentCompany:null,
    companyEmployeesList:[],
    currentJobPosition:null,
    currentResponsibility:null,
    currentResponsibilityList:[],
    jobPositionList:[]

})

export const JobPositionContextProvider:FC=({children})=>{
    const {currentCompany} = useAppContext()
    const [companyEmployeesList, setCompanyEmployeesList] = useState<AppUser[]>([])
    const [jobPositions, setJobPositions] = useState<AppJobPosition[]>([])
    const [currentJobPosition, setCurrentJobPosition] = useState<AppJobPosition|null>(null)
    const [currentResponsibility, setCurrentResponsibility] = useState<AppJobPositionResponsibility|null>(null)
    const [currentResponsibilityList, setCurrentResponsibilityList] = useState<AppJobPositionResponsibility[]>([])
    const updateOnList=async (jobPosition:AppJobPosition)=>{        
        jobPositions.some((_jobPosition:AppJobPosition)=>_jobPosition._id===jobPosition._id)
            ? setJobPositions(jobPositions.map((_jobPosition:AppJobPosition)=>_jobPosition._id===jobPosition._id?jobPosition:_jobPosition))
            : jobPositions.push(jobPosition)
    }
    const saveJobPosition= async (jobPosition:AppJobPosition)=>{
        jobPosition = await JobPositionsService.saveJobPosition(jobPosition)
        jobPositions.some((_jobPosition:AppJobPosition)=>_jobPosition._id===jobPosition._id)
            ? updateOnList(jobPosition)
            : jobPositions.push(jobPosition)
    }
    const deleteJobPosition = async (jobPosition:AppJobPosition)=>{
        try{
            await JobPositionsService.deleteJobPosition(jobPosition)
            setJobPositions(jobPositions.filter(jp=>jp._id!==jobPosition._id))
        }catch(error){

        }
    }
    useEffect(()=>{
        try{
            if(!currentCompany){
                setJobPositions([])
                setCompanyEmployeesList([])
                return
            }
            (async ()=>setJobPositions(await CompaniesService.getCompanyJobPositions(currentCompany?._id??"")))();
            (async ()=>setCompanyEmployeesList(await CompaniesService.getCompanyEmployees(currentCompany?._id??"")))();
        }catch(error){console.log(error)}
    },[currentCompany])
    useEffect(()=>{
        (async ()=>{
            setCurrentResponsibilityList && setCurrentResponsibilityList(
                (!currentJobPosition || currentJobPosition._id==="")
                ?[]
                :await JobPositionsService.getResponsibilities(currentJobPosition)
            )
        })()
    },[currentJobPosition])
    
    return (
        <JobPositionsContext.Provider value={{
            currentCompany:currentCompany,
            companyEmployeesList:companyEmployeesList,
            currentJobPosition:currentJobPosition,
            currentResponsibility:currentResponsibility,
            currentResponsibilityList:currentResponsibilityList,
            jobPositionList:jobPositions,
            setCurrentJobPosition:setCurrentJobPosition,
            setCurrentResponsibility:setCurrentResponsibility,
            setCurrentResponsibilityList:setCurrentResponsibilityList,
            setJobPositionList:setJobPositions,
            updateOnList:updateOnList,
            saveJobPosition:saveJobPosition,
            deleteJobPosition
        }}>
            {children}
        </JobPositionsContext.Provider>
    )
}


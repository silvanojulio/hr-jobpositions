import React, { useContext, FC, useState, useEffect, useCallback } from 'react';

import { useAppContext }    from 'hooks'
import { EmployeeService }  from 'services'

import { AppUser }          from 'entities/appUser'
import { AppCompany }       from 'entities/appCompany'

type EmployeeContextType={
    currentCompany:AppCompany|null,
    currentEmployee:AppUser|null,
    employeeList:AppUser[],
    setCurrentEmployee:(employee:AppUser|null)=>Promise<void>,
    saveEmployee:(employee:AppUser)=>Promise<void>,
    deleteEmployee?:(employee:AppUser)=>Promise<void>,

}

export const EmployeeContext = React.createContext<EmployeeContextType>({
    employeeList:[],
    currentCompany:null,
    currentEmployee:null,
    setCurrentEmployee:async ()=>{},
    saveEmployee:async()=>{},
})

export const EmployeeContextProvider:FC=({children})=>{
    const [currentEmployee, setCurrentEmployee]         = useState<AppUser | null>(null)
    //const [employeeList, setEmployeeList]               = useState<AppUser[]>([])
    
    const { employeeList, setEmployeeList } = useAppContext()
    const context                           = useContext(EmployeeContext)
    const {currentCompany}                  = useAppContext()
    
    const loadCompanyData=useCallback(async(_companyID)=>{
    },[])
    useEffect(()=>{
        if(!currentCompany)return
        loadCompanyData(currentCompany._id)
    },[currentCompany, loadCompanyData])

    const saveEmployee=async (employee:AppUser)=>{
        try{
            employee={...employee, comments:employee.comments??[],documentIDs:employee.documentIDs??[]}
            const savedEmployee:AppUser=await EmployeeService.saveEmployee(employee)
            if(employeeList.some(e=>e._id===savedEmployee._id)){
                setEmployeeList(employeeList.map(e=>e._id===savedEmployee._id?savedEmployee:e))
            }else{
                setEmployeeList([...employeeList,savedEmployee])
            }
        }finally{}
    }
    const deleteEmployee=async (employee:AppUser)=>{
        try{
            await EmployeeService.deleteEmployee(employee)
            setEmployeeList(employeeList.filter((_employee:AppUser)=>_employee._id!==employee._id))
        }finally{}
    }
    return (
        <EmployeeContext.Provider value={{
            ...context,
            currentEmployee,
            currentCompany,
            employeeList,
            setCurrentEmployee:async(employee:AppUser|null)=>setCurrentEmployee(employee?{...employee} as AppUser:null),
            saveEmployee,
            deleteEmployee
        }}>
            {children}
        </EmployeeContext.Provider>
    )
}
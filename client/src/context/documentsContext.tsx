import React, { FC, useContext, useState, useCallback } from 'react'

import { AppDocument }      from 'entities/appDocuments'
import { useAppContext }    from 'hooks'
import { DocumentsService } from 'services'

type DocumentsContextType={
    currentDocument?:AppDocument
    documentList:AppDocument[]
    saveDocument:()=>Promise<void>
    delDocument:(doc:AppDocument)=>Promise<void>
    editDocument:(doc:AppDocument)=>void
    exitDocument:()=>void
    newDocument:()=>void
}

export const DocumentsContext = React.createContext<DocumentsContextType>({
    documentList:[],
    saveDocument:async ()=>{},
    delDocument:async (_doc:AppDocument)=>{},
    editDocument:(_doc:AppDocument)=>{},
    exitDocument:()=>{},
    newDocument:()=>{}
})

export const DocumentsContextProvider:FC = ({children})=>{
    const context=useContext(DocumentsContext)
    const { ask, currentCompany, currentSession } = useAppContext()
    const { currentCompanyDocs, setCurrentCompanyDocs } = useAppContext()
    const [currentDocument, setCurrentDocument] = useState<AppDocument>()
    const saveDocument=useCallback(async()=>{
        if(!currentDocument) return
        try{
            const _doc=await DocumentsService.save(currentDocument)
            if(currentDocument._id==="") currentCompanyDocs.push(_doc)
            setCurrentCompanyDocs(currentCompanyDocs.map(d=>d._id===_doc._id?_doc:d))
            setCurrentDocument(undefined)
        }catch(error){throw error}
    },[currentDocument, currentCompanyDocs, setCurrentCompanyDocs])
    const exitDocument=useCallback(()=>{
        setCurrentDocument(undefined)
    },[])
    const newDocument=useCallback(()=>{
        if(!currentSession) return
        if(!currentCompany) return
        setCurrentDocument({
            _id:"",
            companyID:currentCompany._id,
            last_update:new Date(),
            link:"",
            responsableFullName:currentSession.appUser.fullName,
            title:"",
            global:false
        })
    },[currentSession, currentCompany])
    const delDocument=useCallback(async(doc:AppDocument)=>{
        try{
            await ask(`Do you want to delete reference to '${doc.title}' document?`, "Please Confirm")
            await DocumentsService.delete(doc)
            setCurrentCompanyDocs(currentCompanyDocs.filter(d=>d._id!==doc._id))
        }finally{}
    },[currentCompanyDocs, setCurrentCompanyDocs, ask])
    const editDocument=useCallback((doc:AppDocument)=>{
        setCurrentDocument(doc)
    },[])
    return <DocumentsContext.Provider value={{
        ...context,
        currentDocument,
        saveDocument,
        exitDocument,
        newDocument,
        delDocument,
        editDocument,
        documentList:currentCompanyDocs
        }}>
        {children}
    </DocumentsContext.Provider>
}
import React, { FC, useState }          from 'react'
import { Context, useContext }          from 'react'
import { useEffect, useCallback }       from 'react'
import { Container, Row, Badge }        from 'react-bootstrap'
import { TiArrowBack,TiDocumentAdd }    from 'react-icons/ti'
import { useHistory }                   from 'react-router-dom'

import { EmployeeService }      from 'services'
import { CompaniesService }     from 'services'
import { AssignmnetsService }   from 'services'

import { AppUser }              from 'entities/appUser' 
import { AppJobPosition }       from 'entities/appJobPosition'
import { AppAssignment }        from 'entities/appAssignmnet'

import { useAppContext }        from 'hooks'
import { AssignmentsForm }      from 'components/assignments'
import { IAssignmentsForm }     from 'components/assignments'


type AssignmentsContextType={
    currentEmployee?:AppUser|null,
    assignmentList:AppAssignment[],
    currentAssignment:AppAssignment|null,
    jobPositions:AppJobPosition[],
    setCurrentAssignment?:React.Dispatch<React.SetStateAction<AppAssignment|null>>,
    setAssignmentList?:React.Dispatch<React.SetStateAction<AppAssignment[]>>,
    setJobPositions?:React.Dispatch<React.SetStateAction<AppJobPosition[]>>,
    deleteAssignment?:(assignment:AppAssignment)=>void,
    assignmentDialog:React.RefObject<IAssignmentsForm>
}

export const AssignmentsContext:Context<AssignmentsContextType > = React.createContext<AssignmentsContextType >(
    {
        assignmentList:[], 
        currentAssignment:null,
        jobPositions:[], 
        assignmentDialog:React.createRef<IAssignmentsForm>()
    }
);

export const AssignmentsContextProvider:FC<{employeeID:string}> = ({employeeID, children})=>{
    const history=useHistory()
    const context               = useContext(AssignmentsContext)
    const { currentCompany }    = useAppContext()
    const { employeeList }      = useAppContext()
    const [assignmentList, setAssignmentList] = useState<AppAssignment[]>([])
    const [currentAssignment, setCurrentAssignment] = useState<AppAssignment|null>(null)
    const [jobPositions, setJobPositions] = useState<AppJobPosition[]>([])
    const [currentEmployee, setCurrentEmployee] = useState<AppUser|null>()
    const addAssignment=useCallback(async ()=>{
        if(!context.assignmentDialog.current) return
        if(!currentEmployee) return  
        try{
            const newAssignment = await context.assignmentDialog.current.new(currentEmployee)
            setAssignmentList([...assignmentList, newAssignment])
        }catch(error){
            console.log(error)
        }
    },[currentEmployee,context.assignmentDialog, assignmentList, setAssignmentList])
    const deleteAssignment=useCallback(async (assignment:AppAssignment)=>{
        try{
            const deletedAssignment = await AssignmnetsService.deleteAssignment(assignment)
            setAssignmentList([...assignmentList.filter(assignment=>assignment._id!==deletedAssignment._id)])
        }catch(error){}
    },[assignmentList, setAssignmentList])

    const loadData=useCallback(async(employeeID:string)=>{
        const currentEmployee=employeeList.find(e=>e._id===employeeID)??null
        try{
            if(!currentEmployee) return 
            if(!currentCompany)  return history.goBack() //redireccionar a jobpositions
            setCurrentEmployee(currentEmployee)

            const [_jobPositions, _assignments]=await Promise.all([
                CompaniesService.getCompanyJobPositions(currentCompany._id),
                EmployeeService.getAssignments(currentEmployee._id)
            ])
            setJobPositions(_jobPositions)
            setAssignmentList(_assignments)
        }catch(error){}
    },[employeeList, currentCompany, history])
    useEffect(()=>{loadData(employeeID)}, [employeeID,loadData])
    return (
        <AssignmentsContext.Provider value={{
            ...context,
            currentEmployee, currentAssignment, assignmentList, jobPositions,
            setCurrentAssignment, setAssignmentList, setJobPositions, deleteAssignment, 
        }} >
            <AssignmentsForm ref={context.assignmentDialog} />
            <Container fluid className="assignmentList">
                <Row className="h1">Asignaciones</Row>
                <Row>
                    <Badge pill variant={"secondary"} onClick={history.goBack} ><TiArrowBack/>Volver</Badge>
                    <Badge pill variant={"success"} onClick={addAssignment} ><TiDocumentAdd/>Agregar</Badge>
                </Row>
                {children}
            </Container>
        </AssignmentsContext.Provider>
    )
}
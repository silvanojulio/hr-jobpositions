import AppStore from "./../utils/appStorage";
import { AppSession } from "../entities/appSession";
import axios, {AxiosRequestConfig, Method, ResponseType} from 'axios';
import config from '../config';
export type SchemaError={value:string, msg:string,param:string,location:string}
export class ApiRequest{
        public url: string="";
        public method?: "get"|"post"|"put"|"delete"="get"; 
        public params?: object; 
        public body?: object;
        public headers?: object;
        public responseType?: string = 'json';
}

export class AppError extends Error{
    constructor ( public code:number, public message:string, public extradata:any|null=null){
        super(message);
    }
}

export default class ApiClient {
    static async call(request: ApiRequest) {
        
        let token = null;
        try {
            const currentSession=(await AppStore.getObject<AppSession>('CURRENT_SESSION'));
            token = currentSession!.securityToken;
        } catch (error) {}

        try {
            const requestConfig:AxiosRequestConfig = {
                url: config.getApiUrl() + request.url,
                method: request.method as Method,
                headers: {
                    ...request.headers,
                    'security-token': token,
                },
                params: request.params,
                data: request.body,
                responseType: request.responseType as ResponseType
            };
            var response = await axios(requestConfig).catch(
                (error)=>{
                    if( error.response ){
                        console.log(error.response.data)
                        //console.log(error.response.data); // => the response payload 
                        throw error.response.data;
                    }else throw error;
                }
            );
    
            return response
        } catch (error) {
            throw error;
        }
    }
}

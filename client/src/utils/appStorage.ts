export default class AppStorage {
    
    static async setValue(key:string, value: any) {
        localStorage.setItem(key, typeof value === 'object'? JSON.stringify(value) : value);
    }

    static async getObject<T>(key:string):Promise<T>{
        return JSON.parse(localStorage[key]) as T;
    }

    static async getString(key:string):Promise<string>{
        return localStorage[key];
    }

    static removeValue(key:string){
        return localStorage.removeItem(key);
    }
}

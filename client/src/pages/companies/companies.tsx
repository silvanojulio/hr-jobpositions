import React, { FC, useContext } from 'react';
import { CompanyForm, CompanyList } from 'components/Companies';
import { SessionContext } from 'context/sessionContext';
import { CompanyContextProvider } from 'context/companiesContext';
import { AppSession } from 'entities/appSession';
import { Roles } from 'entities/appUser';

import './companies.scss';

const Companies:FC = ()=>{
    const currentSession:AppSession|null = useContext(SessionContext); 
    if(!Roles.SuperAdmin.isEqual(currentSession?.appUser.role??null)){
        return(<div>Not Allowed</div>);
    }
    return (
        <CompanyContextProvider> 
            <CompanyForm />
            <CompanyList />
        </CompanyContextProvider>
    );
}

export default Companies
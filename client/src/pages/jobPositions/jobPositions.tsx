import React, {FC} from 'react'
import { JobPositionContextProvider } from 'context/jobPositionsContext'
import { JobPositionForm, JobPositionsList } from 'components/jobPositions'

import './jobPositions.scss'

const jobPositions:FC = ()=>{
    
    return (
        <JobPositionContextProvider>
            <JobPositionsList />
            <JobPositionForm />
        </JobPositionContextProvider>
    )
}

export default jobPositions
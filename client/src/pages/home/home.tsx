import React, { FC } from 'react';
import './home.scss';
import { HomeContextProvider } from 'context/homeContext'
import { Dashboard } from 'components/dashboard'
const Home:FC = () => {
  return (
    <HomeContextProvider>
      {<Dashboard/>}
    </HomeContextProvider>
  );
}

export default Home;
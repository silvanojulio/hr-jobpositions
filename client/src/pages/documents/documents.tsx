import React, { FC } from 'react';
import { DocumentsContextProvider } from 'context/documentsContext'
import { DocumentsList, DocumentForm } from 'components/documents'

const Documents : FC = () => {
  return (
    <DocumentsContextProvider>
      <DocumentsList />
      <DocumentForm />
    </DocumentsContextProvider>
  );
}

export default Documents;
import React, {FC, useState, useEffect} from 'react';
import logo from './../../images/logo-icon.png'
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { useForm } from "react-hook-form";
import './login.scss';
import LoginService from '../../services/loginService';
import AppStorage from '../../utils/appStorage';
import { useHistory } from "react-router-dom";

const Login : FC = () => {

  const history = useHistory();
  const { register, handleSubmit, errors } = useForm();
  const [loginError, setLoginError] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    AppStorage.removeValue("CURRENT_SESSION");
  }, []);

  const onSubmit = async (data: any) => {
    try {
      setLoginError("");
      setIsLoading(true);
      const session = await LoginService.login(data.email, data.password);
      await AppStorage.setValue("CURRENT_SESSION", session);
      history.push("/home");
    } catch (error) {
      setIsLoading(false);
      setLoginError(error.message);
    }
  };

  return (
    <Container>
      <Row>
        <Col><img src={logo} alt={"logo"}/> </Col>
      </Row>
      <Row>
        <Col sm={5} style={{marginLeft: 'auto', marginRight: 'auto'}}>
          <Card>
            <Card.Body>
              <Card.Title>Ingreso al sistema</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">Ingrese los datos para acceder al sistema</Card.Subtitle>
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" name={"email"} placeholder="Ingrese su email" ref={register({ required: true })}  />
                  {errors.email && <span style={{ color: 'red'}}>Campo requerido</span>}
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Contraseña</Form.Label>
                  <Form.Control type="password" placeholder="Contraseña" name={"password"} ref={register({ required: true })} />
                  {errors.password && <span style={{ color: 'red'}}>Campo requerido</span>}
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isLoading}>
                  Ingresar
                </Button>
              </Form>
              {<span style={{ color: 'red'}}>{loginError}</span>}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Login;
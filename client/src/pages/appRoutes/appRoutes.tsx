import React, { FC, Suspense } from 'react';
import { withRouter, Switch, Route, Redirect } from 'react-router-dom';
import {RouteComponentProps} from "react-router";
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import PrivateLayout from 'pages/layouts/private';
import PublicLayout from 'pages/layouts/public';
import Login from 'pages/login';
import Home from 'pages/home';
import Companies from 'pages/companies';
import Employees from 'pages/employees';
import JobPositions from 'pages/jobPositions';
import Assignments from 'pages/assignments';
import Responsibilities from 'pages/responsibilities';
import Performances from 'pages/performances';
import PageLoader from 'components/common/pageLoader';
import Review from 'pages/reviews';
import Documents from 'pages/documents'

const listofPublicPages : string[] = [ 
  '/login',
  '/signup',
  '/recover',
  '/lock'
];

// Type whatever you expect in 'this.props.match.params.*'
type PathParamsType = {
  param1: string,
}

// Your component own properties
type PropsType = RouteComponentProps<PathParamsType> & {
  someString?: string,
}

const AppRoutes : FC<PropsType> = ({ location }) => {
  const currentKey = location.pathname.split('/')[1] || '/';
  const timeout = { enter: 500, exit: 500 };
  const animationName = 'rag-fadeIn'

  if(listofPublicPages.indexOf(location.pathname) > -1) {
    return (
        <PublicLayout>
            <Switch location={location}>
                <Route path="/login" component={Login}/>
                <Route path="/signup" component={Login}/>
                <Route path="/recover" component={Login}/>
                <Route path="/lock" component={Login}/>
            </Switch>
        </PublicLayout>
    );
  }else{
    return (
      <PrivateLayout>
        <TransitionGroup>
          <CSSTransition key={currentKey} timeout={timeout} classNames={animationName} exit={false} >
                  <Suspense fallback={<PageLoader/>}>
                  <Switch location={location}>
                      <Route path="/home" component={Home}/>
                      <Route path="/companies" component={Companies}/>
                      <Route path="/employees" component={Employees} />
                      <Route path="/assignments/:employeeID" component={(props:any)=><Assignments employeeID={props.match.params.employeeID} />} />
                      <Route path="/positions" component={JobPositions} />
                      <Route path="/responsibilities/:jobPositionID" component={(props:any)=><Responsibilities jobPositionID={props.match.params.jobPositionID} />} />
                      <Route path="/skills" component={Responsibilities} />
                      <Route path="/performances" component={Performances} />
                      <Route path="/reviews/:performanceID" component={(props:any)=><Review performanceID={props.match.params.performanceID} />} />
                      <Route path="/documents" component={Documents} />
                      <Route path="/" component={Home}/>

                      <Redirect to="/"/>
                  </Switch>
                  </Suspense>
          </CSSTransition>
        </TransitionGroup>
      </PrivateLayout>
  );
  }
}

export default withRouter(AppRoutes);

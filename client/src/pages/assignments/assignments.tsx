import React, { FC } from 'react'
import { AssignmentsList, AssignmentsForm } from 'components/assignments'
import { AssignmentsContextProvider } from 'context/assignmentsContext'

import './assignments.scss'
const Assignments:FC<{employeeID:string}> = ({employeeID})=>{
    return (
        <AssignmentsContextProvider employeeID={employeeID}>
            <AssignmentsList/>
            <AssignmentsForm/>
        </AssignmentsContextProvider>
    )
}

export default Assignments
import React, { FC } from 'react'
import { ResponsibilitiesContextProvider } from 'context/responsibilitiesContext'
import { ResponsibilitiesList } from 'components/responsibilities'
import './responsibilities.scss'


const responsibilities:FC<{jobPositionID?:string}> = ({jobPositionID})=>{
    return (
        <ResponsibilitiesContextProvider jobPositionID={jobPositionID}>
            <ResponsibilitiesList />
        </ResponsibilitiesContextProvider>
    )
}

export default responsibilities
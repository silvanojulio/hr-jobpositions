import React, { FC } from 'react'
import { PerformanceContextProvider } from 'context/performanceContext'
import { PerformanceForm, PerformanceList } from 'components/performances'
export const Performances:FC = ()=>(
    <PerformanceContextProvider>
        <PerformanceForm />
        <PerformanceList />
    </PerformanceContextProvider>
)
import React, {FC, useEffect, useState} from 'react'
import { useHistory } from "react-router-dom"
import './private-layout.scss'
import SessionService from 'services/sessionService'
import AppNavbar from 'components/common/AppNavbar'
import AppMenu from 'components/common/AppMenu'
import { AppSession } from 'entities/appSession'
import { SessionContextProvider } from 'context/sessionContext'
import { AppContextProvider } from 'context/appContext'
import { Container, Row, Col } from 'react-bootstrap'

import './private-layout.scss'

const PrivateLayout : FC = ({children}) => {  
  const [currentSession, setCurrentSession] = useState<AppSession|null>(null)
  const history = useHistory()
  useEffect(() => {
    const getCurrentSession = async () => {
      const _currentSession:AppSession | null = await SessionService.getCurrentSession()
      if(!_currentSession || !_currentSession.appUser)
        history.push("/login")
      else
        setCurrentSession(_currentSession)
    };
    getCurrentSession()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="App">
      <SessionContextProvider value={currentSession}>
        <AppContextProvider>
          <Container fluid className="private-layout">
            <Row><Col><AppNavbar /></Col></Row>
            <Row>
              <Col md={2} className="d-none d-md-block"><AppMenu/></Col>
              <Col xs={12} md={10}>{children}</Col>
            </Row>
          </Container>  
        </AppContextProvider>          
      </SessionContextProvider>
    </div>
  );
}

export default PrivateLayout;
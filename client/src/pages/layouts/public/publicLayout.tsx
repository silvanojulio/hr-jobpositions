import React, {FC} from 'react';
import './public-layout.scss';

const PublicLayout : FC = ({children}) => {

  
  return (
    <div className="public-background">
      {children}
    </div>
  );
}

export default PublicLayout;

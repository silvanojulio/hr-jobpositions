import React , {FC} from 'react'
import { ReviewContextProvider } from 'context/reviewContext'


export const Review:FC<{performanceID:string}> = ({performanceID}) =>{    
    return (
        <ReviewContextProvider performanceID={performanceID} >
            <div>{performanceID}</div>
        </ReviewContextProvider>
    )
}
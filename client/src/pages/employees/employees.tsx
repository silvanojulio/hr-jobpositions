import React, { FC } from 'react'
import { EmployeeContextProvider } from 'context/employeesContext'
import { EmployeeForm, EmployeeList } from 'components/employees'
import './employees.scss'

export const Employees:FC = ()=>{
    return (     
        <EmployeeContextProvider>
            <EmployeeForm />
            <EmployeeList />
        </EmployeeContextProvider>
    )
}
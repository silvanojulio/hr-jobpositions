import React, { FC, useEffect, useCallback, useState }   from 'react'
import { Container, Form, Badge }         from 'react-bootstrap'

import { useAppContext }   from 'hooks'
import { AppJobPositionResponsibility } from 'entities/appJobPositionResponsibility'
import { ResponsibilitiesService } from 'services'

export const ResponsibilityList:FC = ()=>{
    const { currentCompany } = useAppContext()
    const [ responsibilities, setResponsibilities ] = useState<AppJobPositionResponsibility[]>([])
    const loadResponsibilities=useCallback(async()=>{
        if(!currentCompany) return setResponsibilities([])
        const _responsibilities=await ResponsibilitiesService.getList(currentCompany._id)
        setResponsibilities(_responsibilities.filter(res=>res.appJobPositionID===null))

    },[currentCompany])
    useEffect(()=>{loadResponsibilities()},[loadResponsibilities])
    return (
        <Container fluid className="responsibility-list-container">
            <Form.Row>
                <Form.Label><Badge variant="dark">{responsibilities.length}</Badge>Competencias Globales</Form.Label>
            </Form.Row>
            {responsibilities.map(res=>(
                <Form.Row key={res._id} >
                    <Form.Label>{res.details}</Form.Label>
                </Form.Row>
            ))}
            {responsibilities.length===0 && <Form.Row>
                <Form.Label>Sin competencias globales</Form.Label>    
            </Form.Row>}
        </Container>
    )
}
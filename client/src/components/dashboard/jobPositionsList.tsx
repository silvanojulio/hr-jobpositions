import React, { FC, useEffect, useCallback, useState }   from 'react'
import { Container, Form, Badge }         from 'react-bootstrap'

import { useAppContext }   from 'hooks'
import { AppJobPosition } from 'entities/appJobPosition'
import { CompaniesService } from 'services'

export const JobPositionsList:FC = ()=>{
    const { currentCompany } = useAppContext()
    const [ jobPositions, setJobPositions ] = useState<AppJobPosition[]>([])
    const loadPositions=useCallback(async()=>{
        if(!currentCompany) return setJobPositions([])
        setJobPositions(await CompaniesService.getCompanyJobPositions(currentCompany._id))

    },[currentCompany])
    useEffect(()=>{loadPositions()},[loadPositions])
    return (
        <Container fluid className="positions-list-container">
            <Form.Row>
                <Form.Label><Badge variant="dark">{jobPositions.length}</Badge>Posiciones</Form.Label>
            </Form.Row>
            {jobPositions.map(job=>(
                <Form.Row key={job._id} >
                    <Form.Label>{job.name}</Form.Label>
                </Form.Row>
            ))}
            {jobPositions.length===0 && <Form.Row>
                <Form.Label>Sin posiciones creadas</Form.Label>    
            </Form.Row>}
        </Container>
    )
}
import React, { FC }   from 'react'
import { Container, Form, NavLink, Badge }         from 'react-bootstrap'

import { useHomeContext }   from 'hooks'


type DocumentsListProps={
}
export const DocumentsList:FC<DocumentsListProps> = (props)=>{
    const { relatedDocuments } = useHomeContext()
    return (
        <Container fluid className="documents-list-container">
            <Form.Row>
                <Form.Label><Badge variant="dark">{relatedDocuments.length}</Badge>Documentos Globales</Form.Label>
            </Form.Row>
            {relatedDocuments.map(doc=>(
                <Form.Row key={doc._id} >
                    <NavLink href={doc.link} target="_blank">{doc.title}</NavLink>
                </Form.Row>
            ))}
            {relatedDocuments.length===0 && <Form.Row>
                <Form.Label>Sin documentos globales</Form.Label>    
            </Form.Row>}
        </Container>
    )
}
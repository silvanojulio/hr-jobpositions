import React, { FC }   from 'react'
import { Form, Col }         from 'react-bootstrap'

import { EmployeeList }         from './employeeList'
import { DocumentsList }        from './documentsList'
import { ResponsibilityList }   from './responsibilityList'
import { JobPositionsList }     from './jobPositionsList'


type DashboardProps={}
export const Dashboard:FC<DashboardProps> = (props)=>{
    return (<>
        <Form.Row>
            <Col xs={4}><EmployeeList /></Col>
            <Col xs={4}><JobPositionsList /></Col>
            <Col xs={4}>
                <DocumentsList />
                <ResponsibilityList />
            </Col>
        </Form.Row>
    </>)
}
import React, { FC, useRef }            from 'react'
import { useCallback, useState }        from 'react'
import { Container, Form, Badge }       from 'react-bootstrap'

import { useAppContext }                from 'hooks'
import { AppUser }                      from 'entities/appUser'
import { EmployeeForm, IEmployeeForm }  from 'dialogs'

type EmployeeListProps={
}
export const EmployeeList:FC<EmployeeListProps> = (props)=>{
    const { employeeList, setEmployeeList }      = useAppContext()
    const editEmployeeRef       = useRef<IEmployeeForm>(null)

    const editar = useCallback(async(employee:AppUser)=>{
        const saved=await editEmployeeRef.current!.editar(employee).catch(()=>employee)
        setEmployeeList([
            ...employeeList.filter(e=>e._id!==saved._id),
            saved
        ])
        return saved
    },[employeeList, setEmployeeList])
    return (<>
        <EmployeeForm ref={editEmployeeRef} />
        <Container fluid className="employee-list-container">
            <Form.Row>
                <Form.Label><Badge variant="dark">{employeeList.length}</Badge>Colaboradores</Form.Label>
            </Form.Row>
            {employeeList.map((employee, index)=>(
                <Employee key={index} employee={employee} onClick={editar} />
            ))}
            {employeeList.length === 0 && <Form.Row>
                <Form.Label>Sin colaboradores cargados</Form.Label>    
            </Form.Row>}
        </Container>
    </>)
}
const Employee:FC<{employee:AppUser, onClick:(employee:AppUser)=>Promise<AppUser>}> = ({employee, onClick})=>{
    const [ fullName, setFullName ] = useState<string>(employee.fullName)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const click = useCallback(async()=>{
        setFullName((await onClick(employee)).fullName)
    },[onClick, employee])
    return (
        <Form.Row>
            <Form.Label>{fullName}</Form.Label>
        </Form.Row>
    )
}
import React, { useEffect, useState, useCallback}   from 'react'
import { forwardRef, useImperativeHandle, useRef }  from 'react'
import { Form, Modal,Button, Col, Spinner }         from 'react-bootstrap'

import Select       from 'react-select'
import DatePicker   from 'react-datepicker'
import moment       from 'moment'

import AssignmentsServices          from 'services/assignmentsService'
import { useAssignmentsContext }    from 'hooks'
import { useAppContext }            from 'hooks'
import { AppError }                 from 'utils/apiClient'
import { PromiseCallbacks }         from 'types'

import { AppUser }          from 'entities/appUser'
import { AppJobPosition }   from 'entities/appJobPosition'
import { AppAssignment }    from 'entities/appAssignmnet'

export interface IAssignmentsForm{
    new:(employee:AppUser)=>Promise<AppAssignment>
    edit:(assignment:AppAssignment)=>Promise<AppAssignment>
}

export const AssignmentsForm = forwardRef<IAssignmentsForm> ((_, ref)=>{ 
    //#region Variable de contexto
    const assignmentsContext    = useAssignmentsContext()
    const appContext            = useAppContext()
    const { currentEmployee }   = assignmentsContext
    const { jobPositions }      = assignmentsContext
    const { employeeList }      = appContext
    //#endregion
    //#region Variable locales
    const [ error,          setError ]          = useState<AppError|null>(null)
    const [ bussy,          setBussy ]          = useState<boolean>(false)
    const [ curAssignment,  setCurAssignment ]  = useState<AppAssignment>()
    const [ assignmentID,   setAssignmentID ]   = useState<string>("")
    const [ validData,      setValidData ]      = useState<{
        jobPosition:boolean,
        suggestedReviewDate:boolean,
        evaluator:boolean,
    }>({
        jobPosition:true,
        suggestedReviewDate:true,
        evaluator:true
    })
    const [assignmentDate,      setAssignmentDate]      = useState<Date>(new Date())
    const [suggestedReviewDate, setSuggestedReviewDate] = useState<Date>(new Date())
    const [callbacks,           setCallbacks]           = useState<PromiseCallbacks<AppAssignment>>()
    //#endregion
    const jobPositionRef    = useRef<Select<{value: AppJobPosition, label: string}>>(null)
    const evaluatorRef      = useRef<Select<{value:AppUser, label:string}>>(null)
    
    useImperativeHandle(ref,()=>({
        new:async ()=>{
            if(!currentEmployee)throw new Error("Employee not selected")
            const p = new Promise<AppAssignment>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>setCallbacks(undefined))
            setCurAssignment(new AppAssignment(currentEmployee._id))
            return p
        },
        edit:async (assignment:AppAssignment)=>{
            const p = new Promise<AppAssignment>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>setCallbacks(undefined))
            setCurAssignment(assignment)
            return p
        }
    }))
    
    //#region FormActions
    const handleClose=useCallback(()=>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    const handleSave=useCallback(async ()=>{
        if(!callbacks) return 
        if(!currentEmployee) return callbacks.reject(new Error("Employee not selected"))
        if(!jobPositionRef.current) return callbacks.reject(new Error("cannot read jobPosition Reference"))
        if(!evaluatorRef.current) return callbacks.reject(new Error("cannot read evaluator Reference"))
        setBussy(true)
        try{
            const assignment = await AssignmentsServices.saveAssignment({
                _id:assignmentID,
                active:true,
                assignmentDate:assignmentDate.toISOString().split('T')[0],
                suggestedReviewDate:suggestedReviewDate.toISOString().split('T')[0],
                employeeID: currentEmployee._id,
                jobPositionID:(jobPositionRef.current.state.value as {value:AppJobPosition, label:string}).value._id,
                employeeEvaluatorID:(evaluatorRef.current.state.value as {value:AppUser, label:string}).value._id
            })
            callbacks.resolve(assignment)
        }
        catch(error){setError(error)}
        finally{setBussy(false)}
    },[callbacks, assignmentDate, assignmentID, currentEmployee, suggestedReviewDate])
    //#endregion
    //#region Manejo de errores
    useEffect(()=>{
        setValidData({
            jobPosition:true,
            suggestedReviewDate:true,
            evaluator:true
        })
        if(!error) return
        switch(error.code){
        case 800:
            setValidData({
                jobPosition:!error.extradata.errors.some((_error:{location:string, msg:string, param:string, value:string})=>_error.param==="jobPositionID"),
                suggestedReviewDate:!error.extradata.errors.some((_error:{location:string, msg:string, param:string, value:string})=>_error.param==="suggestedReviewDate"),
                evaluator:!error.extradata.errors.some((_error:{location:string, msg:string, param:string, value:string})=>_error.param==="employeeEvaluatorID")
            })
            break;
        default: 
        }
    },[error, setValidData])
    //#endregion
    useEffect(()=>{
        if(!curAssignment) return
        console.log(jobPositionRef.current, evaluatorRef.current)
        if(!jobPositionRef.current) throw new Error("Cannot read jobPosition reference")
        if(!evaluatorRef.current) throw new Error("Cannot read evaluator reference")
        setAssignmentID(curAssignment._id)
        setAssignmentDate(moment(curAssignment.assignmentDate).toDate())
        setSuggestedReviewDate(moment(curAssignment.suggestedReviewDate).toDate())
        evaluatorRef.current.select.clearValue()
        jobPositionRef.current.select.clearValue()
        const currentEvaluator=employeeList.map(e=>({value:e, label:e.fullName})).find(e=>e.value._id===curAssignment.employeeEvaluatorID)
        if(currentEvaluator) evaluatorRef.current.select.selectOption(currentEvaluator)
        const currentJobPosition=jobPositions.map(j=>({value:j, label:j.name})).find(j=>j.value._id===curAssignment.jobPositionID)
        if(currentJobPosition) jobPositionRef.current.select.selectOption(currentJobPosition)
    },[curAssignment, employeeList, jobPositions]) 
    return (
        <Modal show={!!callbacks} className="assignmentForm">
            <Modal.Header >
                <Modal.Title>Asignación</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Row>
                        <Col md={6}>
                            <Form.Label column >Colaborador</Form.Label>
                            <Form.Control size={"sm"} disabled value={currentEmployee?.fullName} />
                        </Col>
                        <Col md={6}>
                            <Form.Label column className={!validData.jobPosition?"text-danger":""} >Posición</Form.Label>
                            <Select<{value: AppJobPosition, label: string}> ref={jobPositionRef}
                                isDisabled={bussy} classNamePrefix={"select-sm"}
                                options={jobPositions.map(jobPosition=>({value:jobPosition, label:jobPosition.name})) }
                            />
                        </Col>
                        <Col md={6} >
                            <Form.Label column >Fecha Asignación</Form.Label>
                            <DatePicker disabled={bussy} dateFormat="yyyy-MM-dd"
                                onChange={(date:Date)=>setAssignmentDate(date)}
                                selected={assignmentDate}
                            />
                        </Col>
                        <Col md={6}>
                            <Form.Label column className={!validData.jobPosition?"text-danger":""} >Fecha evaluación sugerida</Form.Label>
                            <DatePicker disabled={bussy} dateFormat="yyyy-MM-dd" 
                                onChange={(date:Date)=>setSuggestedReviewDate(date)}
                                selected={suggestedReviewDate}
                            />
                        </Col>  
                        <Col>
                            <Form.Label column className={!validData.evaluator?"text-danger":""}>Evaluador</Form.Label>
                            <Select<{value:AppUser, label:string}> ref={evaluatorRef}
                                isDisabled={bussy} classNamePrefix={"select-sm"} 
                                options={employeeList.map(employee=>({value:employee, label:employee.fullName}))}
                            />
                        </Col>
                    </Form.Row>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                {error && <Form.Label>{error.message}</Form.Label>}
                <Button disabled={bussy}
                    variant="secondary" 
                    onClick={handleClose}
                    >Close</Button>
                <Button disabled={bussy}
                    variant="primary" 
                    onClick={handleSave}
                    >{bussy?(<Spinner animation="border" size="sm"/>):"Save"}</Button>
            </Modal.Footer>
        </Modal>
    )
})
import React, { FC } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

import { AppAssignment } from 'entities/appAssignmnet' 
import { Assignment } from 'components/assignments'

import { useAssignmentsContext } from 'hooks'

import './index.scss'

export const AssignmentsList:FC= ()=>{
    const { assignmentList } = useAssignmentsContext()
    return (        
        <Container fluid className={"grid-striped"}>
            <Row className={"grid-head"}>
                <Col xs={3}>Posición</Col>
                <Col xs={3}>Fecha de asignación</Col>
                <Col xs={3}>Fecha evaluación sugerida</Col>
                <Col xs={3}></Col>
            </Row>
            
            {assignmentList.map((assignment:AppAssignment)=>(<Assignment key={assignment._id} assignment={assignment} />))}    
        </Container>
    )
}
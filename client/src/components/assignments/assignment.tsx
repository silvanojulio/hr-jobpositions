import React, { FC, useCallback, useEffect, useState } from 'react'
import { Row, Col, Badge } from 'react-bootstrap'
import { useAssignmentsContext, useAppContext } from 'hooks'
import { AppJobPosition } from 'entities/appJobPosition'
import { TiEdit, TiDelete } from 'react-icons/ti'


import { AppAssignment } from 'entities/appAssignmnet'


export const Assignment:FC<{assignment:AppAssignment}> = ({assignment})=>{
    const context = {...useAssignmentsContext(), ...useAppContext()}
    const { assignmentList }        = context
    const { setAssignmentList }     = context
    const { jobPositions }          = context
    const { currentEmployee }       = context
    const { deleteAssignment }      = context
    const { assignmentDialog }      = context
    const { ask } = useAppContext()
    const [jobPosition, setJobPosition] = useState<AppJobPosition>()

    //#region Carga Inicial
    useEffect(()=>{setJobPosition(jobPositions.find(_job=>_job._id===assignment.jobPositionID))},[jobPositions,assignment])
    //#endregion
    //#region Buttons Handlers
    const editClickHandler=useCallback(async()=>{
        if(!assignmentDialog.current || !setAssignmentList || !assignmentList) return
        try{
            const editedAssignment = await assignmentDialog.current.edit(assignment)
            setAssignmentList([...assignmentList.map(a=>a._id===editedAssignment._id?editedAssignment:a) ])
        }catch(error){ console.log(error) }
    }, [assignment,assignmentDialog,setAssignmentList,assignmentList])
    const deleteClickHandler=useCallback(async ()=>{
        try{
            await ask(`Do you want to delete the Assignment of ${jobPosition?.name} to ${currentEmployee?.fullName}?`, "Please Confirm Action")
            deleteAssignment!(assignment)
        }catch(error){}
        
    },[deleteAssignment, assignment,jobPosition, ask, currentEmployee])
    //#endregion

    return (    
        <Row>
            <Col sm={3}>{jobPosition?.name}</Col>
            <Col sm={3}>{assignment.assignmentDate??""}</Col>
            <Col sm={3}>{assignment.suggestedReviewDate??""}</Col>
            <Col sm={3}>
                <Badge as="button" pill variant={"secondary"} onClick={editClickHandler}><TiEdit />Editar</Badge>
                <Badge pill variant={"danger"} onClick={deleteClickHandler}><TiDelete />Borrar</Badge>
            </Col>
        </Row> 
    )
}
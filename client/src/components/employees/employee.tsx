import React, { FC }        from 'react'
import { useHistory }       from 'react-router-dom'
import { Row, Col, Badge }  from 'react-bootstrap'
import { TiEdit }           from 'react-icons/ti'
import { TiDelete }         from 'react-icons/ti'
import { TiThList }         from 'react-icons/ti'

import { AppUser }                              from 'entities/appUser'
import { useEmployeeContext, useAppContext }    from 'hooks'
import { EmployeeComments }                     from './comments'


type EmployeeType={
    employee:AppUser
}

export const Employee:FC<EmployeeType> = ({employee})=>{
    const { deleteEmployee }        = useEmployeeContext()
    const { setCurrentEmployee }    = useEmployeeContext()
    const { ask }                   = useAppContext()
    const history                   = useHistory()
    
    const deleteMe=async ()=>{
        try{
            await ask(`Do you want to delete the Employee ${employee.fullName}?`,"Please Confirm Action")
            deleteEmployee && deleteEmployee(employee)
        }catch(error){console.log(error)}
    }
    const editMe=async ()=>{
        try{
            setCurrentEmployee && setCurrentEmployee(employee)
        }catch(error){console.log(error )}
    }
    return (
        <Row noGutters>
            <Col xs={3}>{employee.fullName}</Col>
            <Col xs={2}>{employee.role?.description}</Col>
            <Col xs={7}>
                {/*<Badge pill variant={employee.active?"success":"danger"} onClick={changeStatus}>
                    <TiPower/>{employee.active?"Activo":"Inactivo"}
                </Badge>*/}
                <EmployeeComments employee={employee}/>
                <Badge pill variant="primary" onClick={()=>{history.push("/assignments/"+employee._id);}}>
                    <TiThList/>Asignaciones
                </Badge>
                <Badge pill variant="secondary" onClick={editMe}>
                    <TiEdit />Editar
                </Badge>
                <Badge pill variant="danger" onClick={deleteMe}>
                    <TiDelete />Borrar
                </Badge>
            </Col>
        </Row>
    )
}


import React, { FC, useState, useCallback, useRef } from 'react'
import { Modal, Form, Col, Button, Badge, Spinner } from 'react-bootstrap'
import moment from 'moment'


import { AppUser, Comment }     from 'entities/appUser'
import { TiThList }             from 'react-icons/ti'
import { FaRegSave }            from 'react-icons/fa'
import { MdAttachment }         from 'react-icons/md'
import { useAppContext }        from 'hooks'
import { useEmployeeContext }   from 'hooks'

type EmployeeCommentsProps={
    employee:AppUser
}
export const EmployeeComments:FC<EmployeeCommentsProps> = (props)=>{
    const {employee} = props
    const { currentSession } = useAppContext()
    const { saveEmployee,employeeList }   = useEmployeeContext()
    const commentRef=useRef<HTMLInputElement>(null)
    const linkRef=useRef<HTMLInputElement>(null)
    const [show, setShow] = useState<boolean>(false)
    const [bussy, setbussy] = useState<boolean>(false)
    const saveComment=useCallback(async()=>{
        if(!commentRef.current) return
        if(!linkRef.current) return
        if(!currentSession) return
        if(!employee.comments) employee.comments=[]
        try{
            setbussy(true)
            const comment:Comment = {
                author:currentSession.appUser._id, 
                comment:commentRef.current.value,
                link:linkRef.current.value,
                timestamp:Date.now()
            }
            await saveEmployee({
                ...employee,
                comments:[...employee.comments, comment]
            })
            employee.comments.push(comment)
            commentRef.current.value=""
            linkRef.current.value=""
        }catch(error){console.log(error)}finally{setbussy(false)}
    },[currentSession, saveEmployee, employee])
    const openCommentLink=useCallback(async (comment:Comment)=>{
        window.open(comment.link,"_blank")
    },[])
    return (<>
        <Badge pill variant="secondary" onClick={()=>setShow(true)}>
            <TiThList/>Comentarios
        </Badge>
        <Modal show={show} size="lg">
            <Modal.Body>
                <Form.Row className="bg-info">
                    <Col xs={3}>Fecha/Hora</Col>
                    <Col xs={3}>Autor</Col>
                    <Col xs={6}>Comentario</Col>
                </Form.Row>
                {(employee.comments??[]).map((comment, index)=>(
                    <Form.Row key={index}>
                        <Col xs={3}>{moment(comment.timestamp).format("YYYY-MM-DD HH:mm:ss")}</Col>
                        <Col xs={3}>{employeeList.find(e=>e._id===comment.author)?.fullName??""}</Col>
                        <Col xs={6}>
                            {comment.link
                                ?<MdAttachment style={{color:"blue", cursor:"pointer"}} onClick={()=>openCommentLink(comment)}/>
                                :""
                            }
                            {comment.comment}
                        </Col>
                    </Form.Row>
                ))}
                <Form.Row className="mt-3">
                    <Col xs={12}><Form.Control disabled={bussy} ref={linkRef} size="sm" placeholder="Link" className="mb-2" /></Col>
                    <Col xs={11}><Form.Control disabled={bussy} ref={commentRef} size="sm" placeholder="Nuevo Comentario"/></Col>
                    <Col className="text-right">
                        <Button size="sm" className="w-100">
                            {bussy
                                ?<Spinner animation="border" size="sm"/>
                                :<FaRegSave style={{fontSize:"1.25em"}} onClick={saveComment}/>
                            }
                        </Button>
                    </Col>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={()=>setShow(false)}>Cerrar</Button>
            </Modal.Footer>
        </Modal>
        </>
    )
}
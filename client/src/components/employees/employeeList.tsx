import React, { FC, useCallback } from 'react'
import { useEmployeeContext } from 'hooks/useEmployeeContext'
import { AppUser, Roles } from 'entities/appUser'
import { Employee } from './employee'

import { TiDocumentAdd } from 'react-icons/ti'
import { Form, Container, Row, Col, Badge } from 'react-bootstrap'

export const EmployeeList:FC=()=>{
    const {setCurrentEmployee, employeeList, currentCompany}=useEmployeeContext()
    const addClickHandle=useCallback(()=>{
        setCurrentEmployee({
            _id:"",
            fullName:"",
            active:false,
            company:currentCompany,
            role:Roles.Employee,
            email:"",
            documentIDs:[],
            comments:[]
        } as AppUser)
    },[currentCompany, setCurrentEmployee])
    return (
        <Container fluid>
            <Row className="h1">Colaboradores</Row>
            <Row hidden={!!currentCompany}><Form.Label>Debe seleccionar una companía</Form.Label></Row>
            <Row>
                <Col xs={6}>
                    <Badge hidden={currentCompany===null} variant="success" pill onClick={addClickHandle} >
                        <TiDocumentAdd/>Agregar
                    </Badge>
                </Col>
            </Row>
            <Container hidden={currentCompany===null} fluid className="grid-striped">
                <Row className="grid-head" >
                    <Col xs={3}>Nombre</Col>
                    <Col xs={3}>Rol</Col>
                    <Col xs={6}></Col>
                </Row>    
                {employeeList.map((employee:AppUser)=>( 
                    <Employee employee={employee} key={employee._id} />
                ))}
            </Container>
        </Container>  
    )
}
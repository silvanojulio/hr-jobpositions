import React, { FC, useState, useEffect }               from 'react'
import {Button, Col, Modal, Form, Spinner, Dropdown}    from 'react-bootstrap'
import Select                                           from 'react-select'

import { useEmployeeContext }   from 'hooks'
import { useAppContext }        from 'hooks'
import { AppUser, Roles }       from 'entities/appUser'
import { AppError }             from 'utils/apiClient'
import { DocumentSelectOption } from 'entities/appDocuments'

type ValidDataType={
    fullName?:Boolean, 
    email?:Boolean,
}
type SchemaError={value:string, msg: string, param: string, location: string}

export const EmployeeForm:FC = ()=>{
    const { setCurrentEmployee, saveEmployee, currentEmployee } = useEmployeeContext()
    const { currentCompanyDocs }                                = useAppContext()
    
    const [error, setError]=useState<AppError|null>(null)    
    const [bussy, setBussy] = useState<boolean>(false)
    const [validData, setValidData]=useState<ValidDataType>({fullName:true,email:true})

    useEffect(()=>{
        if(error===null){
            setValidData({
                fullName:true,
                email:true
            })
            return
        }
        switch(error?.code){
            case 800:
                setValidData({
                    fullName:!error.extradata.errors.some((err:SchemaError)=>err.param==="fullName"),
                    email:!error.extradata.errors.some((err:SchemaError)=>err.param==="email")
                })
                break
            case 801:
                setValidData({
                    email:!error.extradata.newUserEmail,
                    fullName:!error.extradata.fullName
                })
                break
            default:console.log(error)
        }
    },[error])
    useEffect(()=>{
        setError(null)
    },[currentEmployee])
    const handleClose=()=>{
        setError(null)
        setValidData({
            fullName:true,
            email:true
        })
    }
    const handleSave=async ()=>{
        try{
            setBussy(true)
            setError(null)
            await saveEmployee(currentEmployee!)
            setCurrentEmployee(null)
        }catch(error){
            setError(error as AppError)
        }finally{
            setBussy(false)
        }
    }

    return (
        
        <Modal show={!!currentEmployee} onHide={handleClose} >
            <Modal.Header>
                <Modal.Title>{currentEmployee?._id?'Editar':'Agregar'} Colaborador</Modal.Title>
            </Modal.Header>
            
            <Modal.Body>
                <Form>
                    <Form.Row className="align-items-center mb-2">
                        <Col sm={3}>
                            <Form.Label column className={!validData.fullName?"text-danger":""}>Nombre</Form.Label>
                        </Col>
                        <Col sm={9}>
                            <Form.Control disabled={bussy}
                                className={!validData.fullName?"border-danger":""} 
                                onChange={
                                    (e)=>setCurrentEmployee({...currentEmployee,...{fullName:e.target.value}} as AppUser)
                                } 
                                defaultValue={currentEmployee?.fullName??""} 
                            />
                        </Col>
                    </Form.Row> 
                    <Form.Row className="align-items-center mb-2">
                        <Col sm={3}>
                            <Form.Label column className={!validData.email?"text-danger":""}>Email</Form.Label>
                        </Col>
                        <Col sm={9}>
                            <Form.Control disabled={bussy}
                                className={!validData.email?"border-danger":""} 
                                onChange={
                                    (e)=>setCurrentEmployee({...currentEmployee,...{email:e.target.value}} as AppUser)
                                } 
                                defaultValue={currentEmployee?.email??""} 
                            />
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col sm={3}>
                            <Form.Label column>Rol</Form.Label>
                        </Col>
                        <Col sm={9}>                            
                            <Dropdown>
                                <Dropdown.Toggle disabled={bussy} variant="info" size="sm" id="dropdown-basic">
                                    {currentEmployee?.role?.description??"Select Rol"}
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    {Roles.getAll().filter((role:Roles)=>!Roles.SuperAdmin.isEqual(role)).map((role:Roles)=>{
                                        return (                        
                                            <Dropdown.Item key={role.id} onClick={()=>{
                                                setCurrentEmployee({...currentEmployee, ...{role:role}} as AppUser)
                                            }} >{role.description}</Dropdown.Item>
                                        )
                                    })}
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                    </Form.Row>
                    <Form.Row className="align-items-center">
                        <Col sm={3}>
                            <Form.Label column>Activo</Form.Label>
                        </Col>
                        <Col sm={9}>
                            <Form.Switch id="ativeSwitch" label=""
                                disabled={bussy}  
                                checked={currentEmployee?.active??false} 
                                onChange={()=>setCurrentEmployee({
                                    ...currentEmployee,
                                    active:!currentEmployee?.active??true
                                } as AppUser)} />
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col xs={3}>
                            <Form.Label column>Documentos</Form.Label>
                        </Col>
                        <Col xs={9}>
                            <Select<DocumentSelectOption> placeholder="Documentos relacionados" isMulti
                                isDisabled={bussy}
                                defaultValue={
                                    currentCompanyDocs
                                    .filter(doc=>currentEmployee?.documentIDs.some(id=>id===doc._id))
                                    .map(doc=>({doc,value:doc._id, label:doc.title}))
                                }
                                options={
                                    currentCompanyDocs.map(doc=>({doc,value:doc._id, label:doc.title}))
                                }
                                onChange={(options)=>{
                                    if(!currentEmployee)return 
                                    if(!options) {
                                        currentEmployee.documentIDs=[]
                                        return
                                    }
                                    currentEmployee.documentIDs=(options as DocumentSelectOption[]).map(o=>o.value)
                                }}
                            />
                        </Col>
                    </Form.Row>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                {error && <Form.Label>{error.message}</Form.Label>}
                <Button disabled={bussy}
                    variant="secondary" 
                    onClick={()=>setCurrentEmployee(null)}
                    >Cerrar</Button>
                <Button disabled={bussy}
                    variant="primary" 
                    onClick={()=>handleSave()}
                    >{bussy?(<Spinner animation="border" size="sm"/>):"Guardar"}</Button>
            </Modal.Footer>
        </Modal>
    )
}
import React, { FC, useState, useEffect } from 'react'
import { useJobPositionsContext } from 'hooks/useJobPositionsContext'
import { AppJobPosition } from 'entities/appJobPosition'
import { AppError } from 'utils/apiClient'
import JobPositionsService from 'services/jobPositionsService'

import { Form }     from 'react-bootstrap'
import { Col }      from 'react-bootstrap'
import { Button }   from 'react-bootstrap'
import { Modal }    from 'react-bootstrap'
import { Spinner }  from 'react-bootstrap'
import Select       from 'react-select'

type ValidDataType={
    name:boolean, 
    managerID:boolean, 
    process:boolean,
    helper:boolean
}

const JobPositionForm:FC= ()=>{
    const {
        currentJobPosition,
        setCurrentJobPosition, 
        updateOnList, 
        companyEmployeesList, 
        jobPositionList,
    }=useJobPositionsContext()
    const [error, setError]=useState<AppError|null>(null)
    const [validData, setValidData]=useState<ValidDataType>({name:true, managerID:true, process:true, helper:true})
    const [show, setShow]=useState<Boolean>(false)
    const [JPOptions, setJPOptions] = useState<{value:string|null, label:string}[]>([])
    const [HOptions, setHOptions] = useState<{value:string|null, label:string}[]>([])
    const [bussy, setBussy] = useState<boolean>(false)
    useEffect(()=>{
        setJPOptions([{value:"", label:"N/A"}, ...jobPositionList.map(jp=>({value:jp._id, label:jp.name}))])
    },[jobPositionList])
    useEffect(()=>{
        setHOptions([{value:"", label:"N/A"}, ...companyEmployeesList.map(em=>({value:em._id, label:em.fullName}))])
    },[companyEmployeesList])
    useEffect(()=>{
        setShow(!!currentJobPosition)
    },[currentJobPosition])
    const handleClose=()=>{
        setCurrentJobPosition && setCurrentJobPosition(null)
    }
    const validate=async()=>{
        validData.helper=currentJobPosition?.helperID!==undefined
        validData.managerID=currentJobPosition?.managerID!==undefined
        validData.name=!!currentJobPosition?.name && currentJobPosition.name.length>5
        validData.process=!!currentJobPosition?.process && currentJobPosition.process.length>5

        setValidData({...validData})
        const result=validData.name && validData.managerID && validData.process && validData.helper
        if(!result) setError(new AppError(0, "Verify required fields")); else setError(null)
        return result
    }

    const handleSave=async ()=>{        
        if(!currentJobPosition) return
        if(!await validate()) return
        setBussy(true)
        try{
            const jobPosition = await JobPositionsService.saveJobPosition(currentJobPosition)
            updateOnList!(jobPosition)
            setCurrentJobPosition!(null)
        }catch(_error){
            setError(_error)
        }finally{
            setBussy(false)
        }
    }
    
    useEffect(()=>{
        if(error===null) return
        switch(error.code){
        case 800:
            setValidData({
                name:!error.extradata.errors.some((e:{value:string,msg:string,param:string,location:string})=>e.param==="name"),
                managerID:!error.extradata.errors.some((e:{value:string,msg:string,param:string,location:string})=>e.param==="managerID"),
                process:!error.extradata.errors.some((e:{value:string,msg:string,param:string,location:string})=>e.param==="process"),
                helper:!error.extradata.errors.some((e:{value:string, msg:string, param:string, location:string})=>e.param==="helperID")
            });
            break
        case 801:
            setValidData({name:false, managerID:false, process:false, helper:false})
            break
        default:
        }
    },[error])

    return (
        
        <Modal show={show} onHide={handleClose}>
            <Modal.Header >
                <Modal.Title>Posición</Modal.Title>
            </Modal.Header>
            
            <Modal.Body>
                <Form>
                    <Form.Row className="align-items-center">
                        <Col sm={6}>
                            <Form.Label column className={!validData.name?"text-danger":""}>Nombre</Form.Label>
                            <Form.Control  disabled={bussy}
                                className={!validData.name?"border-danger":""} 
                                onChange={
                                    (e)=>setCurrentJobPosition!({...currentJobPosition, name:e.target.value} as AppJobPosition)
                                } 
                                defaultValue={currentJobPosition?.name??""} 
                            />
                        </Col>
                        <Col sm={6}>
                            <Form.Label column className={!validData.managerID?"text-danger":""}>Posición superior</Form.Label>
                            <Select isMulti={false} placeholder="Select a Job Position" isDisabled={bussy}
                                options={JPOptions} 
                                defaultValue={JPOptions.find(o=>o.value===currentJobPosition?.managerID)}
                                onChange={option=>{
                                    const {value}=(option as {label:string, value:string|null})
                                    setCurrentJobPosition!({...currentJobPosition, managerID:value??""} as AppJobPosition)
                                }}
                            />
                        </Col>
                    </Form.Row> 
                    
                    <Form.Row className="align-items-center">
                        <Col sm={6}>
                            <Form.Label column className={!validData.process?"text-danger":""}>Proceso</Form.Label>
                            <Form.Control disabled={bussy}
                                className={!validData.process?"border-danger":""} 
                                onChange={
                                    (e)=>setCurrentJobPosition!({...currentJobPosition, process:e.target.value} as AppJobPosition)
                                } 
                                defaultValue={currentJobPosition?.process??""} 
                            />
                        </Col>
                        <Col sm={6}>
                            <Form.Label column className={!validData.helper?"text-danger":""}>Referente</Form.Label>
                            <Select isMulti={false} placeholder="Select employee" isDisabled={bussy}
                                options={HOptions}
                                defaultValue={HOptions.find(ho=>ho.value===currentJobPosition?.helperID)}
                                onChange={option=>{
                                    const {value}=(option as {value:string|null, label:string})
                                    setCurrentJobPosition!({...currentJobPosition, helperID:value??""} as AppJobPosition)
                                }}
                                
                            />
                        </Col>
                    </Form.Row> 
                    <Form.Row style={{marginTop:"20px"}}>
                    </Form.Row>
                </Form>
            </Modal.Body>
            
            <Modal.Footer>
                {error && <Form.Label>{error.message}</Form.Label>}
                <Button 
                    disabled={bussy}
                    style={{marginRight:"10px"}} 
                    variant="secondary" 
                    onClick={handleClose}
                    >Cerrar</Button>
                <Button 
                    disabled={bussy}
                    style={{marginRight:"10px"}} 
                    variant="primary"
                    onClick={handleSave}
                    >{bussy?(<Spinner animation="border" size="sm"/>):"Guardar"}</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default JobPositionForm
import React, { FC } from 'react'
import { AppJobPosition } from 'entities/appJobPosition'
import { useJobPositionsContext } from 'hooks/useJobPositionsContext'
import { useAppContext } from 'hooks'
import { Row, Col, Badge} from 'react-bootstrap'
import { TiEdit, TiDelete, TiThList } from 'react-icons/ti'

import { useHistory } from 'react-router-dom';
type JobPositionType={
    jobPosition:AppJobPosition
}

const JobPosition:FC<JobPositionType> = ({jobPosition})=>{
    const { setCurrentJobPosition, deleteJobPosition } = useJobPositionsContext() 
    const { ask } = useAppContext()
    const history=useHistory()
    const askDelete=async (jobPosition:AppJobPosition)=>{
        try{
            await ask(`Desea eliminar la posición ${jobPosition.name}?`,'Confirme la acción')
            
            deleteJobPosition!(jobPosition)
        }catch(_error){}
    }
    return (        
        <Row noGutters>
            <Col xs={6}>{jobPosition.name}</Col>
            <Col xs={6}>
                <Badge pill variant="primary" onClick={()=>{history.push("/responsibilities/"+jobPosition._id);}}>
                    <TiThList/>Responsabilidades
                </Badge>
                <Badge pill variant="secondary" onClick={()=>setCurrentJobPosition && setCurrentJobPosition(jobPosition)}>
                    <TiEdit/>Editar
                </Badge>
                <Badge pill variant="danger" onClick={()=>{askDelete(jobPosition)}}>
                    <TiDelete/>Borrar
                </Badge>
            </Col>
        </Row>
    )
}

export default JobPosition
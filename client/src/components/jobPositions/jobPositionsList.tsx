import React, { FC, useCallback } from 'react';
import { AppJobPosition } from 'entities/appJobPosition';
import { useJobPositionsContext } from 'hooks/useJobPositionsContext'
import { JobPosition } from 'components/jobPositions'
import { Form, Container, Row, Col, Badge } from 'react-bootstrap';
import { TiDocumentAdd } from 'react-icons/ti'

const JobPositionList:FC = ()=>{    
    const {currentCompany, jobPositionList, setCurrentJobPosition} = useJobPositionsContext();
    const createJobPosition=useCallback(()=>{
        if(!setCurrentJobPosition || !currentCompany) return
        setCurrentJobPosition({
            _id:"",
            companyID:currentCompany._id
        } as AppJobPosition)
    },[currentCompany, setCurrentJobPosition])

    return ( 
        <Container fluid>
            <Row className="h1">Posiciones</Row>
            <Row hidden={!!currentCompany}><Form.Label>Debe seleccionar una companía</Form.Label></Row>
            <Row>
                <Col>
                    <Badge pill variant="success" hidden={!currentCompany} onClick={createJobPosition}>
                        <TiDocumentAdd />Agregar
                    </Badge>
                </Col>
            </Row>
            <Container hidden={!currentCompany} fluid className="grid-striped">
                <Row className="grid-head">
                    <Col xs={6}>Posición</Col>
                    <Col></Col>
                </Row>
                {jobPositionList.map((jobPosition:AppJobPosition)=>( 
                    <JobPosition key={jobPosition._id} jobPosition={jobPosition} />
                ))}
            </Container>   
        </Container>        
    )
}
export default JobPositionList
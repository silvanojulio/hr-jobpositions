import React, { FC } from 'react';
import { Company } from 'components/Companies';
import { AppCompany } from 'entities/appCompany';
import { useCompaniesContext } from 'hooks/useCompaniesContext'
import { Container, Row, Col, Badge} from 'react-bootstrap'
import { TiDocumentAdd} from 'react-icons/ti'

const CompanyList:FC = ()=>{    
    const {addCompany, companyList} = useCompaniesContext();
    return (
        <Container fluid>
            <Row className="h1">Companías</Row>
            <Row>
                <Badge pill variant="success" onClick={addCompany}>
                    <TiDocumentAdd/>Agregar
                </Badge>
            </Row>
            <Container fluid className="grid-striped" >
                <Row className="grid-head">
                    <Col xs={6}>Companía</Col>
                    <Col xs={6}></Col>
                </Row>
                {companyList.map((company:AppCompany)=>( 
                    <Company company={company} key={company._id} />
                ))}
            </Container>
        </Container>        
    )
}
export default CompanyList
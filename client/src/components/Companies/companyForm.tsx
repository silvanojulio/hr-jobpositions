import React, { FC, useState, useEffect } from 'react';
import { useCompaniesContext } from 'hooks/useCompaniesContext';

import Form from 'react-bootstrap/esm/Form';
import Modal from 'react-bootstrap/esm/Modal';
import Col from 'react-bootstrap/esm/Col';
import Button from 'react-bootstrap/esm/Button';


const CompanyForm:FC= ()=>{
    const {currentCompany, serviceError, setCurrentCompany, saveCompany } = useCompaniesContext()
    const [validData, setValidData]=useState<{name:Boolean}>({name:true})

    const handleClose=()=>{
        setCurrentCompany(null);
    }
    useEffect(()=>{
        if(serviceError===null) return;
        switch(serviceError.code){
        case 800:
            setValidData({
                name:!serviceError.extradata.errors.some((e:{value:string,msg:string,param:string,location:string})=>e.param==="name")
            });
            break;
        case 801:
            setValidData({name:false});
            break;
        default:
        }
    },[serviceError])
    return (
        <Modal show={!!currentCompany} onHide={handleClose} >
            <Modal.Header >
                <Modal.Title>Editar Companía</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Row className="align-items-center">
                        <Col sm={2}>
                            <Form.Label column className={!validData.name?"text-danger":""}>Nombre</Form.Label>
                        </Col>
                        <Col sm={10}>
                            <Form.Control 
                                className={!validData.name?"border-danger":""} 
                                onChange={(e)=>currentCompany!.name=e.target.value}
                                defaultValue={currentCompany?.name??""} 
                            />
                        </Col>
                    </Form.Row> 
                    <Form.Row className="align-items-center">
                        <Col sm={2}>
                            <Form.Label column>Activo</Form.Label>
                        </Col>
                        <Col sm={10}>
                            <Form.Switch 
                                id={"ativeSwitch"} 
                                label={""} 
                                checked={currentCompany?.active??false} 
                                onChange={()=>currentCompany!.active=!currentCompany?.active??false}
                            />
                        </Col>
                    </Form.Row>               
                </Form>
            </Modal.Body>
            <Modal.Footer>
                {serviceError && <Form.Label>{serviceError.message}</Form.Label>}
                <Button variant="secondary" onClick={handleClose}>Cerrar</Button>
                <Button variant="primary"  onClick={()=>{saveCompany(currentCompany!)}}>Guardar Cambios</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default CompanyForm;
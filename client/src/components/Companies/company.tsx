import React, { FC } from 'react';
import { AppCompany } from 'entities/appCompany';
import { useCompaniesContext } from 'hooks/useCompaniesContext';
import { Row, Col, Badge } from 'react-bootstrap';
import { TiEdit, TiDocumentAdd, TiDelete, TiPower } from 'react-icons/ti'

const Company:FC<{company:AppCompany}> = ({company})=>{
    const {setCurrentCompany, deleteCompany, saveCompany}=useCompaniesContext();
    return (
        <Row >
            <Col xs={6}>{company.name}</Col>
            <Col xs={6}>
                <Badge pill variant={company.active?"success":"danger" } onClick={()=>saveCompany({...company, active:!company.active})}>
                    <TiPower/>{company.active?"Activa":"Inactiva"}
                </Badge>
                <Badge pill variant={"secondary"} onClick={()=>setCurrentCompany(company)} >
                    <TiEdit/>Editar
                </Badge>
                <Badge pill variant={"primary"} onClick={()=>setCurrentCompany({...company, _id:"", name:company.name+"_copia"})}>
                    <TiDocumentAdd/>Duplicar
                </Badge>
                <Badge pill variant={"danger"} onClick={()=>deleteCompany(company)}>
                    <TiDelete/>Borrar
                </Badge>
            </Col>
        </Row>
    );
    
}


export default Company;
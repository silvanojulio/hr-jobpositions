import React, { FC, Fragment, useState, useCallback } from 'react';
import { Roles } from 'entities/appUser'
import { useAppContext } from 'hooks/useAppContext'
import { ButtonGroup, ToggleButton  } from 'react-bootstrap'
import { useHistory } from 'react-router-dom';
import Select from 'react-select'

import './AppMenu.scss' 

type menuItem={href:string, text:string, roles:Roles[]}
const AppMenu : FC = ()=> {
    const { currentSession, companyList, setCurrentCompany, currentCompany } = useAppContext()
    const [activeItem, setActiveItem] = useState<string>()
    const history = useHistory();
    
    const menuitems:menuItem[]=[
        {href:"home",text:"Inicio", roles:[Roles.CompanyAdmin, Roles.Employee]},
        {href:"companies",text:"Companías", roles:[]},
        {href:"positions", text:"Posiciones", roles:[Roles.CompanyAdmin]},
        {href:"employees", text:"Colaboradores", roles:[Roles.CompanyAdmin]},
        {href:"performances", text:"Evaluaciones", roles:[Roles.CompanyAdmin, Roles.Employee]},
        {href:"documents",text:"Documentos",roles:[Roles.CompanyAdmin, Roles.Employee]},
        {href:"skills",text:"Competencias",roles:[Roles.CompanyAdmin]},
        {href:"login",text:"Salir", roles:[Roles.CompanyAdmin, Roles.Employee]},
    ]
    const navigate=useCallback(async(item:menuItem)=>{
        setActiveItem(item.href)
        history.push(`/${item.href}`)
    },[history, setActiveItem])
    return (
        <Fragment>
            {Roles.SuperAdmin.isEqual(currentSession?.appUser.role??null) && (<Select 
                className="w-100 mb-2"
                options={companyList.map(company=>({value:company, label:company.name}))}
                value={currentCompany?{value:currentCompany,label:currentCompany.name??""}:undefined}
                onChange={(o:any)=>{
                    setCurrentCompany(o?.value??null)
                }}
                placeholder="Companía..."
                
            />)}
            <ButtonGroup className="menu w-100" vertical toggle>
                {menuitems.filter(
                    ({roles})=>roles.some(rol=>rol.isEqual(currentSession?.appUser.role??null)) ||
                               Roles.SuperAdmin.isEqual(currentSession?.appUser.role??null)                    
                ).map((item:menuItem, index:number)=>(
                    <ToggleButton 
                        key={index} 
                        checked={activeItem===item.href} 
                        value={index} 
                        type="radio" 
                        variant="outline-info" 
                        onClick={()=>navigate(item)} 
                    >{item.text}</ToggleButton>
                ))}
            </ButtonGroup >
        </Fragment>
    );
}
export default AppMenu;
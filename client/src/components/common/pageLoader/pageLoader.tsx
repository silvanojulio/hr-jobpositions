import React from 'react';

import './pageLoader.scss';

// See more loading icons here:
// https://fontawesome.com/how-to-use/on-the-web/styling/animating-icons
const PageLoader = () => (
    <div className="Loading">
        <div className="">
            <div></div>
            <div>Cargando...</div>
            <div></div>
        </div>
    </div>
)

export default PageLoader;
import React, {FC} from 'react'
import { Dropdown, SplitButton } from 'react-bootstrap'

export type DropDownType ={
    currentLabel:string,
    list:DropDownItemType[],
    hidden?:boolean,
    selectCallback:(item:DropDownItemType)=>void
}
export type DropDownItemType={
    _id:string,
    label:string,
}
export const DropDown:FC<DropDownType> = ({currentLabel, list, hidden, selectCallback})=>{
    return (
        <SplitButton id={"item._id"} style={{minWidth:"200px"}} className="float-left" variant="info" hidden={hidden} size="sm" title={currentLabel}>
            {list.map((item:DropDownItemType, index)=>(
                <Dropdown.Item key={index} onClick={()=>selectCallback(item)} >{item.label}</Dropdown.Item>
            ))}
        </SplitButton>
    )
}

export default DropDown
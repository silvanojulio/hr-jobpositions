import React, { FC, useContext, useState, useCallback, useRef } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { AppSession } from 'entities/appSession';
import { SessionContext } from 'context/sessionContext';
import { AppContext } from 'context/appContext'
import { NavLink} from 'react-bootstrap'
import { Modal, Form, Col, Button } from 'react-bootstrap'


import './AppNavbar.scss'

const AppNavbar : FC = () => {
  const currentSession:AppSession|null = useContext(SessionContext);
  const [errorMsg, setErrorMsg] = useState<string>("")
  const {changePass} = useContext(AppContext)
  const [show, setShow] = useState<boolean>(false)
  const old_pass=useRef<HTMLInputElement>(null)
  const new_pass=useRef<HTMLInputElement>(null)
  const repeat=useRef<HTMLInputElement>(null)

  const handleCancel=useCallback(()=>{
    old_pass.current!.value=""
    new_pass.current!.value=""
    repeat.current!.value=""

    setShow(false)
  },[])
  const handleSave=useCallback(async ()=>{
    if(new_pass.current!.value!==repeat.current!.value) {
      setErrorMsg("Por favor, repita la contraseña")
      return
    }
    try{
      const res=await changePass(currentSession!.appUser.email, old_pass.current!.value, new_pass.current!.value)
      currentSession!.appUser=res
      handleCancel()
    }catch(error){
      switch(error.code){
        case 800:setErrorMsg("Complete todos los campos"); break;
        case 409:setErrorMsg("La contraseña anterior es incorrecta");break;
        default: setErrorMsg(error.message);
      }
      console.log(error)
    }

  },[changePass, currentSession, handleCancel])
  return (
    <>
      <Navbar bg="info" className="rounded-bottom mb-4" sticky="top">
        <Navbar.Brand href="#home">{currentSession?.appUser.company?.name??"Administración"}</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text style={{textAlign:'right'}}>
            Logueado como: {currentSession?.appUser.fullName}<br/>
            <NavLink onClick={()=>setShow(true)}>(Cambiar contraseña)</NavLink>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
      <Modal show={show} >
        <Modal.Header style={{fontSize:'x-large'}}>Cambiar Contraseña</Modal.Header>
        <Modal.Body>
          <Form className="ChangePassForm">
            <Form.Row>
              <Col>Contraseña Anterior:</Col>
              <Col><Form.Control type="password" size="sm" ref={old_pass}/></Col>
            </Form.Row>
            <Form.Row>
              <Col>Nueva Contraseña:</Col>
              <Col><Form.Control type="password" size="sm" ref={new_pass}/></Col>
            </Form.Row>
            <Form.Row>
              <Col>Repetir Contraseña:</Col>
              <Col><Form.Control type="password" size="sm" ref={repeat}/></Col>
            </Form.Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Form.Label color="red">{errorMsg}</Form.Label>
          <Button variant="secondary" onClick={handleCancel}>Cancelar</Button>
          <Button variant="success" onClick={handleSave}>Guardar</Button>
        </Modal.Footer>
      </Modal>
    </>
)}

export default AppNavbar;
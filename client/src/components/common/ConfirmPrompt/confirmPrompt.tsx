import React, {FC} from 'react'
import { Modal, Button,  } from 'react-bootstrap'

export type ConfirmPromptProps={
    reject?:()=>void, 
    resolve?:(value:void)=>void,
    title:string, 
    question:string,
    show:boolean
}
export const ConfirmPrompt:FC<ConfirmPromptProps> = ({reject, resolve, show, title, question})=>{
    return (
        <Modal size="sm" show={show}>
            <Modal.Header><Modal.Title>{title}</Modal.Title></Modal.Header>
            <Modal.Body>{question}</Modal.Body>
            <Modal.Footer>
                <Button variant="success" onClick={()=>resolve!()}>Si</Button>
                <Button variant="danger" onClick={()=>reject!()}>No</Button>
            </Modal.Footer>
        </Modal>
    )
}
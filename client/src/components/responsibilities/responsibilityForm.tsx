import React, { useState, useEffect, useCallback }       from 'react'
import { useRef, forwardRef, useImperativeHandle } from 'react'
import {Button, Modal, Form, Row, Col, Spinner} from 'react-bootstrap/'
import Select                                   from 'react-select'


import { AppJobPositionResponsibility,
    AppJobPositionResponsibilityPeriodicity,
    AppJobPositionResponsibilityRisk }          from 'entities/appJobPositionResponsibility'
import { AppUser }                              from 'entities/appUser'
import { DocumentSelectOption }                 from 'entities/appDocuments'
import { useAppContext }                        from 'hooks'
import { ResponsibilitiesService }              from 'services'
import { AppError }                             from 'utils/apiClient'
import { PromiseCallbacks }                     from 'types'

export interface IResponsibilityForm{
    nuevo:  (company_id:string, jobPosition_id:string|null)=>Promise<AppJobPositionResponsibility>
    editar: (responsibility:AppJobPositionResponsibility)=>Promise<AppJobPositionResponsibility>
}
export const ResponsibilityForm=forwardRef<IResponsibilityForm> ((_, ref)=>{
    const appContext                = useAppContext()
    const { currentCompanyDocs }    = appContext
    const { employeeList }          = appContext

    const [ callbacks,              setCallbacks ]              = useState<PromiseCallbacks<AppJobPositionResponsibility>>()
    const [ currentResponsibility,  setCurrentResponsibility ]  = useState<AppJobPositionResponsibility>()
    const [ bussy,                  setBussy ]                  = useState<boolean>(false)
    const [ error,                  setError ]                  = useState<AppError|null>(null)
    const [ showDias,               setShowDias ]               = useState<boolean>(false)
    const [ global,                 setGlobal ]                 = useState<boolean>(false)  
    const [ validSchema, setValidSchema]                     = useState<{
        controlledBy:boolean,
        details:boolean,
        referenceComments:boolean,
        subProcess:boolean,
        periodicity:boolean,
        risk:boolean
    }>({
        controlledBy:true,
        details:true,
        referenceComments:true,
        subProcess:true,
        periodicity:true,
        risk:true
    })

    const detailsRef        = useRef<HTMLInputElement>(null)
    const subProcessRef     = useRef<HTMLInputElement>(null)
    const commentsRef       = useRef<HTMLTextAreaElement>(null)
    const weighingRef       = useRef<HTMLInputElement>(null)
    const isHomeOffice      = useRef<HTMLInputElement>(null)
    const diasRef           = useRef<HTMLInputElement>(null)
    const periodicityRef    = useRef<Select<{value:number, label:string, days:number}>>(null)
    const riskRef           = useRef<Select<{value:number, label:string}>>(null)
    const controllerRef     = useRef<Select>(null)
    const indicatorRef      = useRef<HTMLTextAreaElement>(null)
    const documentsRef      = useRef<Select<DocumentSelectOption>>(null)

    useImperativeHandle(ref, ()=>({
        nuevo:(company_id:string, jobPosition_id:string|null)=>{
            setCurrentResponsibility(new AppJobPositionResponsibility("",jobPosition_id,company_id))
            const p = new Promise<AppJobPositionResponsibility>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>setCallbacks(undefined))
            setError(null)
            return p
        },
        editar:(responsibility:AppJobPositionResponsibility)=>{
            setCurrentResponsibility({...responsibility})
            const p = new Promise<AppJobPositionResponsibility>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>setCallbacks(undefined))
            setError(null)
            return p
        }
    }))

    const validarSchema=useCallback(()=>{   
        const validation ={
            controlledBy:!!controllerRef.current?.state.value,
            details:(detailsRef.current?.value.length??0)>4,
            referenceComments:(commentsRef.current?.value.length??0)>4,
            subProcess:(subProcessRef.current?.value.length??0)>4 || global,
            periodicity:!!periodicityRef.current?.state.value || global,
            risk:!!riskRef.current?.state.value,
        }
        setValidSchema(validation)
        return Object.values(validation).reduce<boolean>((ret:boolean,valid:boolean)=>(ret && valid), true)
    },[global])
    const handleClose=()=>{
        if(!callbacks) return
        setError(null)
        callbacks.reject()
    }
    const handleSave=useCallback(async()=>{
        if(!callbacks) return
        if(!currentResponsibility) return setCallbacks(undefined)
        if(
            !isHomeOffice.current ||
            !detailsRef.current ||
            !subProcessRef.current ||
            !commentsRef.current ||
            !controllerRef.current ||
            !indicatorRef.current ||
            !periodicityRef.current ||
            !weighingRef.current ||
            !riskRef.current ||
            !documentsRef.current ||
            !diasRef.current
        ) return console.log([
            isHomeOffice.current,
            detailsRef.current,
            subProcessRef.current,
            commentsRef.current,
            controllerRef.current,
            indicatorRef.current,
            periodicityRef.current,
            weighingRef.current,
            riskRef.current,
            documentsRef.current,
            diasRef.current,
        ])
        if(!validarSchema()) return
        const periodicityId = global?0:(periodicityRef.current.state.value as {value:number, label:string}).value
        currentResponsibility.details           = detailsRef.current.value
        currentResponsibility.subProcess        = global?"":subProcessRef.current.value
        currentResponsibility.referenceComments = commentsRef.current.value
        currentResponsibility.weighing          = weighingRef.current.valueAsNumber
        currentResponsibility.isHomeOffice      = isHomeOffice.current.checked
        currentResponsibility.periodicity       = {
            ...AppJobPositionResponsibilityPeriodicity.getById(periodicityId),
            days:global?0:diasRef.current.valueAsNumber
        } as AppJobPositionResponsibilityPeriodicity
                                                  
        currentResponsibility.risk              = AppJobPositionResponsibilityRisk.getById((riskRef.current.state.value as {value:number, label:string}).value)
        currentResponsibility.controlledBy      = (controllerRef.current.state.value as {value:string, label:string}).value
        currentResponsibility.indicator         = indicatorRef.current.value
        currentResponsibility.documentIDs       = (documentsRef.current.state.value as DocumentSelectOption[]).map<string>(option=>option.value)
        try{
            setBussy(true)
            const responsibility=await ResponsibilitiesService.saveResponsibility(currentResponsibility)
            callbacks.resolve(responsibility)
        }
        catch(error){setError(error)}
        finally{setBussy(false)}
    },[currentResponsibility, validarSchema, callbacks, global])
    useEffect(()=>{
        setValidSchema({
            controlledBy:true,
            details:true,
            referenceComments:true,
            subProcess:true,
            periodicity:true,
            risk:true
        })
        if(!currentResponsibility) return 
        setGlobal(!currentResponsibility.appJobPositionID)
        if(!!controllerRef.current){
            const controller = employeeList.find(employee=>employee._id===currentResponsibility.controlledBy)
            if(!!controller) controllerRef.current.select.selectOption({value:controller._id, label:controller.fullName})
            else controllerRef.current.select.clearValue()
        }
        if(!!periodicityRef.current && !!diasRef.current){
            periodicityRef.current.select.clearValue()
            const periodicity = currentResponsibility.periodicity
            if(!!periodicity) periodicityRef.current.select.selectOption({value:periodicity.id, label:periodicity.description,days:periodicity.days??0})
            diasRef.current.value=(periodicity?.days??0).toString()
            setShowDias(!!periodicity && periodicity.id===0)
        }
        if(!!riskRef.current){
            riskRef.current.select.clearValue()
            const risk = currentResponsibility.risk
            if(!!risk) riskRef.current.select.selectOption({value:risk.id, label:risk.description})
        }
        if(!!documentsRef.current){
            documentsRef.current.select.clearValue()
            const defaultValue=currentCompanyDocs
                .filter(({_id})=>(currentResponsibility.documentIDs??[]).some(id=>id===_id))
                .map<DocumentSelectOption>(doc=>({doc, value:doc._id, label:doc.title}))
            //console.log(currentResponsibility,currentCompanyDocs,defaultValue)
            if(defaultValue.length>0) defaultValue.forEach(value=>documentsRef.current!.select.selectOption(value))
        }
        if(!!detailsRef.current)    detailsRef.current.value    = currentResponsibility.details
        if(!!subProcessRef.current) subProcessRef.current.value = currentResponsibility.appJobPositionID?currentResponsibility.subProcess:"    "
        if(!!commentsRef.current)   commentsRef.current.value   = currentResponsibility.referenceComments
        if(!!weighingRef.current)   weighingRef.current.value   = currentResponsibility.weighing.toString()
        if(!!isHomeOffice.current)  isHomeOffice.current.checked= currentResponsibility.isHomeOffice
        if(!!indicatorRef.current)  indicatorRef.current.value  = currentResponsibility.indicator
    },[currentResponsibility, employeeList, currentCompanyDocs])
    useEffect(()=>{
        const validSchema={
            controlledBy:true,
            details:true,
            indicator:true,
            isHomeOffice:true,
            referenceComments:true,
            subProcess:true,
            weighing:true,
            periodicity:true,
            risk:true
        }
        if(!error) return
        switch(error.code){
            case 800:
                type schemaError={
                    location:string, msg:string,  value:string,
                    param:"controlledBy"|"details"|"indicator"|"isHomeOffice"|"referenceComments"|"subProcess"|"weighing"
                }
                error.extradata.errors.forEach((_error:schemaError)=>{validSchema[_error.param]=false})
                setValidSchema({...validSchema})
                break;
            default:
        }
    },[error])
    return (
        <Modal show={!!callbacks} size="lg">
            <Modal.Header >
                <Modal.Title>{currentResponsibility?.appJobPositionID?"Responsabilidad":"Competencia"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row className={validSchema.details?undefined:"error"}>
                    <Col sm={4}><Form.Label>Detalle</Form.Label></Col>
                    <Col sm={8}><Form.Control disabled={bussy} ref={detailsRef}/></Col>
                </Row>
                <Row hidden={global} className={validSchema.subProcess?undefined:"error"}>
                    <Col sm={4}><Form.Label>Sub-Proceso</Form.Label></Col>
                    <Col sm={8}><Form.Control disabled={bussy} ref={subProcessRef}/></Col>
                </Row>
                <Row className={validSchema.referenceComments?undefined:"error"}>
                    <Col sm={4}><Form.Label>Definición</Form.Label></Col>
                    <Col sm={8}><Form.Control as="textarea" disabled={bussy} ref={commentsRef}/></Col>
                </Row>
                <Row>
                    <Col sm={4}><Form.Label>Peso</Form.Label></Col>
                    <Col sm={8}><Form.Control min={0} max={10} disabled={bussy} type={"number"} ref={weighingRef} /></Col>
                </Row>
                <Row hidden={global}>
                    <Col sm={4}><Form.Label>Remoto</Form.Label></Col>
                    <Col sm={8}><Form.Switch disabled={bussy} id={"wfhSwitch"} label={""} ref={isHomeOffice}/></Col>
                </Row>
                <Row hidden={global} className={validSchema.periodicity?undefined:"error"}>
                    <Col sm={4}><Form.Label>Frecuencia</Form.Label></Col>
                    <Col sm={showDias?4:8}>
                        <Select<{value:number, label:string, days:number}> isDisabled={bussy} ref={periodicityRef}  
                            options={AppJobPositionResponsibilityPeriodicity.getAll().map(item=>({value:item.id,label:item.description,days:item.days??0}))}
                            onChange={(option)=>{
                                if(!option) return setShowDias(false)
                                setShowDias((option as {value:number, label:string, days:number}).value===0)
                                if(!!diasRef.current) diasRef.current.value=(option as {value:number, label:string, days:number}).days.toString()
                            }}
                        />
                    </Col>
                    <Col sm={2} hidden={!showDias}>
                        <Form.Label>Días:</Form.Label>
                    </Col>
                    <Col sm={2} hidden={!showDias}>
                        <Form.Control disabled={bussy} type="number" ref={diasRef} placeholder="Dias"/>
                    </Col>
                </Row>
                <Row className={validSchema.risk?undefined:"error"}>
                    <Col sm={4}><Form.Label>Riesgo</Form.Label></Col>
                    <Col sm={8}><Select<{value:number, label:string}> isDisabled={bussy} ref={riskRef}
                        options={AppJobPositionResponsibilityRisk.getAll().map(item=>({value:item.id, label:item.description}))}
                    /></Col>
                </Row>
                <Row className={validSchema.controlledBy?undefined:"error"}>
                    <Col sm={4}><Form.Label>Controlador</Form.Label></Col>
                    <Col sm={8}><Select isDisabled={bussy} ref={controllerRef}
                        options={employeeList.map((employee:AppUser)=>({value:employee._id, label:employee.fullName}))}
                    /></Col>
                </Row>
                <Row>
                    <Col sm={4}><Form.Label>Indicador / OKR</Form.Label></Col>
                    <Col sm={8}><Form.Control disabled={bussy} as="textarea" ref={indicatorRef} /></Col>
                </Row>
                <Row>
                    <Col sm={4}><Form.Label>Documentos</Form.Label></Col>
                    <Col sm={8}>
                        <Select<DocumentSelectOption> isDisabled={bussy} placeholder="Documentos relacionados" isMulti ref={documentsRef}
                            options={currentCompanyDocs.map(doc=>({doc, value:doc._id, label:doc.title}))}
                        />
                    </Col>
                </Row>
            </Modal.Body>    

            <Modal.Footer>
                {error && <Form.Label>{error.message}</Form.Label>}
                <Button 
                    disabled={bussy}
                    style={{marginRight:"10px"}} 
                    variant="secondary" 
                    onClick={handleClose}
                    >Close</Button>
                <Button 
                    disabled={bussy}
                    style={{marginRight:"10px"}} 
                    variant="primary" 
                    onClick={handleSave}
                    >{bussy?(<Spinner animation="border" size="sm"/>):"Save"}</Button>
            </Modal.Footer>
        </Modal>
    )
})
import React, { FC, useCallback }       from 'react'
import { Container, Row, Col, Badge, Form }   from 'react-bootstrap'
import { useHistory }                   from 'react-router-dom'
import { TiArrowBack,TiDocumentAdd }    from 'react-icons/ti'



import { useResponsibilitiesContext }   from 'hooks'
import { useAppContext }                from 'hooks'
import { AppJobPositionResponsibility } from 'entities/appJobPositionResponsibility'
import { Responsibility }               from 'components/responsibilities'

export const ResponsibilitiesList:FC = ()=>{
    const responsibilitiesContext   = useResponsibilitiesContext()
    const appContext                = useAppContext()
    const { responsibilities }      = responsibilitiesContext 
    const { currentJobPositionID }  = responsibilitiesContext
    const { upsertResponsibility }  = responsibilitiesContext
    const { currentCompany }        = appContext
    const { responsibilityForm }    = appContext
     
    const createResponsibility=useCallback(async()=>{ 
        if(!responsibilityForm.current) return
        if(!currentCompany) return
        try{
            const newResponsibility= await responsibilityForm.current.nuevo(currentCompany._id, currentJobPositionID)
            upsertResponsibility(newResponsibility)
        }
        catch(error){}
    },[currentJobPositionID, currentCompany, responsibilityForm, upsertResponsibility])
    const history = useHistory()
    return (
        <Container fluid>
            <Row className="h1">{currentJobPositionID?"Responsabilidades de la posición":"Competencias globales"}</Row>
            <Row hidden={!!currentCompany}><Form.Label>Debe seleccionar una companía</Form.Label></Row>

            <Row hidden={!currentCompany}>
                <Badge hidden={!currentJobPositionID} pill variant="secondary" onClick={()=>{if(currentJobPositionID) history.push('/positions')}}>
                    <TiArrowBack/>Volver
                </Badge>
                <Badge pill variant="success" onClick={createResponsibility}>
                    <TiDocumentAdd/>Agregar
                </Badge>
            </Row>
            <Container hidden={!currentCompany} fluid className="grid-striped" >
                <Row className="grid-head">
                    <Col xs={3}>Responsabilidad</Col>
                    <Col xs={3}>Sub Proceso</Col>
                    <Col xs={3}>Frecuencia</Col>
                    <Col xs={3}></Col>
                </Row>
                {responsibilities && responsibilities.map((responsibility:AppJobPositionResponsibility)=>(
                    <Responsibility key={responsibility._id} responsibility={responsibility} />
                ))}
            </Container>
        </Container>
    )
}

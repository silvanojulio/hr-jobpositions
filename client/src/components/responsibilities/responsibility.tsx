import { AppJobPositionResponsibility } from 'entities/appJobPositionResponsibility'
import React, { FC, useCallback } from 'react'
import { useResponsibilitiesContext, useAppContext } from 'hooks'
import { Row, Col, Badge} from 'react-bootstrap'
import { TiEdit, TiDelete } from 'react-icons/ti'

type ResponsibilityType={
    responsibility:AppJobPositionResponsibility
}

export const Responsibility:FC<ResponsibilityType> = ({responsibility})=>{
    const responsibilitiesContext       = useResponsibilitiesContext()
    const { deleteResponsibility }      = responsibilitiesContext
    const { upsertResponsibility }      = responsibilitiesContext
    const appContext                    = useAppContext()
    const { ask }                       = appContext
    const { responsibilityForm }        = appContext
    const askDelete=async()=>{
        try{
            await ask(`Desea eliminar la responsabilidad ${responsibility.details}?`,"Confirme la acción")
            deleteResponsibility!(responsibility)
        }catch(error){}
    }
    const editResponsibility=useCallback(async()=>{
        if(!responsibilityForm.current) return
        try{
            const editedResponsibility = await responsibilityForm.current.editar(responsibility)
            upsertResponsibility(editedResponsibility)
        }catch(error){}
        
    },[responsibility, responsibilityForm, upsertResponsibility])
    return (
        <Row>
            <Col xs={3}>{responsibility.details}</Col>
            <Col xs={3}>{responsibility.subProcess}</Col>
            <Col xs={3}>{responsibility.periodicity?.description}</Col>
            <Col xs={3}>
                <Badge pill variant="secondary" onClick={editResponsibility}>
                    <TiEdit/>Editar
                </Badge>
                <Badge pill variant="danger" onClick={()=>{askDelete()}}>
                    <TiDelete/>Borrar
                </Badge>
            </Col>
        </Row>
    )
}

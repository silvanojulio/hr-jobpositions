import React, { FC, useRef }                from 'react'
import { useState, useCallback, useEffect } from 'react'
import { forwardRef, useImperativeHandle }  from 'react'
import { Form, Col, Container, Button }     from 'react-bootstrap'
import { Modal, Spinner }                   from 'react-bootstrap'

import moment                               from 'moment'
import Select                               from 'react-select'
import { TiDocument }                       from 'react-icons/ti'
import { MdAttachment }                     from 'react-icons/md'
import ReactStars                           from "react-stars";


import { AppAssignment }                    from 'entities/appAssignmnet'
import { AppPerformanceReviewResult }       from 'entities/appPerformanceReview'
import { AppPerformanceReviewDetail }       from 'entities/appPerformanceReviewDetail'
import { AppJobPositionResponsibility }     from 'entities/appJobPositionResponsibility'
import { AppDocument }                      from 'entities/appDocuments'
import { Comment, Roles }                   from 'entities/appUser'

import EmployeesService                     from 'services/employeesService'
import { ResponsibilitiesService }          from 'services/responsibilitiesService'
import { usePerformanceContext }            from 'hooks'
import { useSessionContext }                from 'hooks'
import { useAppContext }                    from 'hooks'

import type { PromiseCallbacks }            from 'types/callbacks'

import './style.scss'
import { PerformanceServices } from 'services'
interface IConfirmResultSave{
    confirm:()=>Promise<boolean>
}
const ConfirmResultSave=forwardRef<IConfirmResultSave>((_,ref)=>{
    const [ callbacks, setCallbacks ] = useState<PromiseCallbacks<boolean>>()
    const confirm = useCallback(async()=>{
        const p = new Promise<boolean>((resolve, reject)=>{
            setCallbacks({resolve, reject})
        })
        p.finally(()=>setCallbacks(undefined))
        return p
    },[])
    useImperativeHandle(ref, ()=>({confirm}), [confirm])
    return(
        <Modal show={!!callbacks} centered backdrop="static" keyboard={false}>
            <Modal.Body>
                <Form.Label>Guardando un resultado de evaluación, esta no podrá ser editada.</Form.Label>
                <Form.Label>Desea confirmar la evaluación</Form.Label>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="success" onClick={()=>{
                    if(!callbacks) return
                    callbacks.resolve(true)
                }}>SI</Button>
                <Button variant="danger" onClick={()=>{
                    if(!callbacks) return
                    callbacks.resolve(false)
                }}>NO</Button>
            </Modal.Footer>
        </Modal>
    )
})
export const PerformanceCompleteForm:FC = ()=>{
    const currentSession            = useSessionContext()
    const { completePerformance, setCompletePerformance }   = usePerformanceContext()
    const { companyJobPositionList }                        = usePerformanceContext()
    const { currentCompanyDocs, employeeList }              = useAppContext()

    const [ bussy, setBussy ]       = useState<boolean>(false)
    const [ loading, setLoading ]   = useState<boolean>(false)
    const [ readOnly, setReadOnly ] = useState<boolean>(false)
    const confirmRef        = useRef<IConfirmResultSave>(null)
    const resultCommentRef  = useRef<HTMLTextAreaElement>(null)

    const jobPosition = companyJobPositionList.find(job=>job._id===completePerformance?.jobPositionID)
    const employee    = employeeList.find(emp=>emp._id===completePerformance?.employeeID)
    const evaluator   = employeeList.find(emp=>emp._id===completePerformance?.evaluatorID)

    const [responsibilies, setResponsibilities] = useState<AppJobPositionResponsibility[]>([])
    const [assignment, setAssignment]           = useState<AppAssignment>()
    const [details, setDetails]                 = useState<{det:AppPerformanceReviewDetail,res:AppJobPositionResponsibility}[]>([])
    const getRelatedDocuments=useCallback(():AppDocument[]=>{
        const globals = currentCompanyDocs.filter(doc=>doc.global)
        const employeeDocs = currentCompanyDocs.filter(doc=>(employee?.documentIDs??[]).some(id=>id===doc._id))
        const responsibiliesDocs = currentCompanyDocs.filter(doc=>responsibilies.some(res=>(res.documentIDs??[]).some(id=>doc._id===id)))

        return [...globals, ...employeeDocs, ...responsibiliesDocs].reduce<AppDocument[]>(
                (result,doc)=>result.some(d=>d._id===doc._id)?result:[...result, doc] ,[]
            )
    },[currentCompanyDocs,responsibilies, employee])
    const loadData=useCallback(async()=>{
        if(!completePerformance) return
        setReadOnly(
            AppPerformanceReviewResult.NotEvaluated.id!==completePerformance?.result?.id
        )
        setLoading(true)
        completePerformance.result = AppPerformanceReviewResult.getAll().find(r=>r.id===(completePerformance.result?.id??-1))??AppPerformanceReviewResult.NotEvaluated
        if(resultCommentRef.current) resultCommentRef.current.value=completePerformance.resultComment??''
        const responsibilities=(await ResponsibilitiesService.getList(completePerformance.companyID)).filter(res=>res.appJobPositionID===completePerformance.jobPositionID || !res.appJobPositionID)
        setResponsibilities(responsibilities)
        
        const currentDetails = await PerformanceServices.getCurrentDetails(completePerformance._id)
        setDetails(responsibilities.map<{det:AppPerformanceReviewDetail,res:AppJobPositionResponsibility}>(res=>({
            det:currentDetails.find(d=>d.responsibilityID===res._id)??{
                _id:"",
                axis:"",
                employeeAssessment:0,
                employeeComments:"",
                evaluatorAssessment:0,
                evaluatorComments:"",
                performanceReviewID:completePerformance._id,
                reference:res.referenceComments,
                skill:res.details,
                weighing:res.weighing,
                responsibilityID:res._id
            },res})))

        EmployeesService.getAssignments(completePerformance.employeeID)
        .then(arr=>setAssignment(arr.find(assign=>assign.jobPositionID===completePerformance.jobPositionID)))
        .catch(_err=>setAssignment(undefined))
        
        setLoading(false)
    },[completePerformance ])
    const saveChanges=useCallback(async()=>{
        console.log(completePerformance, details)
        if(!completePerformance) return
        if(completePerformance.result && !completePerformance.result.isEqual(AppPerformanceReviewResult.NotEvaluated)) {
            if(!confirmRef.current) return
            if(!(await confirmRef.current.confirm())) return
        }
        setBussy(true)
        const resultComment=resultCommentRef.current?.value??""
        await PerformanceServices.completePerformanceDetails(
            completePerformance._id,            
            details.map(det=>det.det),
            completePerformance.result??AppPerformanceReviewResult.NotEvaluated,
            resultComment
        )
        setBussy(false)
    },[details, completePerformance])
    useEffect(()=>{loadData()},[loadData])
    
    const openCommentLink=useCallback(async (comment:Comment)=>{
        window.open(comment.link,"_blank")
    },[])
    return (
        <Container fluid className="performance-complete-form">
            <Form.Row>
                <Col xs={8}>
                    <Container fluid className="header-fields">
                        <Form.Row>
                            <Col xs={4}>Colaborador</Col>
                            <Col xs={4}>Posición</Col>
                            <Col xs={2}>Fecha asignado</Col>
                            <Col xs={2}>Fecha sugerida</Col>
                        </Form.Row>
                        <Form.Row>
                            <Col xs={4}>{employee?.fullName}</Col>
                            <Col xs={4}>{jobPosition?.name}</Col>
                            <Col xs={2}>{assignment?.assignmentDate}</Col>
                            <Col xs={2}>{assignment?.suggestedReviewDate}</Col>
                        </Form.Row>        
                    </Container>            
                    <Container fluid className="header-fields">
                        <Form.Row>
                            <Col xs={4}>Evaluador</Col>
                            <Col xs={2}>Período</Col>
                            <Col xs={2}>Resultado</Col>
                            <Col xs={2}>Fecha</Col>
                        </Form.Row>
                        <Form.Row>
                            <Col xs={4}>{evaluator?.fullName}</Col>
                            <Col xs={2}>{completePerformance?.period}</Col>
                            <Col xs={2}>{completePerformance?.result?.description??""}</Col>
                            <Col xs={2}>{moment().format("YYYY-MM-DD")}</Col>
                        </Form.Row>          
                    </Container>            
                </Col>
                <Col xs={4} className="pb-2">
                    <Container fluid className="header-fields h-100">
                        <Form.Row><Col>Documentos relacionados</Col></Form.Row>
                        <Form.Row><Col>
                            {getRelatedDocuments().map(doc=>(
                                <Form.Label className="d-block" key={doc._id} as="a" href={doc.link} target="_blank">
                                    <TiDocument/>{doc.title}
                                </Form.Label>
                            ))}
                        </Col></Form.Row>
                    </Container>

                </Col>
            </Form.Row>
            <Container fluid className="skills-evaluation">
                <Form.Row >
                    <Col xs={2}>Responsabilidad</Col>
                    <Col xs={3}>Referencia</Col>
                    <Col xs={1}>Eje</Col>
                    <Col xs={1}>Calificación</Col>
                    <Col xs={1}>Remoto</Col>
                    <Col xs={4}>Comentarios</Col>
                </Form.Row>
                {loading && (<Form.Row className="justify-content-center">
                    <Spinner animation="border" variant="info"/>
                </Form.Row>)}
                {details.filter(d=>!!d.res.appJobPositionID).map(({det:detail, res:responsibility},index)=>(
                    <Form.Row key={index} >
                        <Col xs={2}>{detail.skill}</Col>
                        <Col xs={3}>{detail.reference}</Col>
                        <Col xs={1}>
                            <Form.Control disabled={bussy || readOnly} type="text" name="axis" value={detail.axis} onChange={e=>{detail.axis=e.target.value; setDetails([...details]);}} />
                        </Col>
                        <Col xs={1}>
                            <ReactStars 
                                value={currentSession?.appUser._id===evaluator?._id?detail.evaluatorAssessment:detail.employeeAssessment} 
                                edit={!bussy && !readOnly}
                                color1="lightgray"
                                color2="seagreen"
                                onChange={
                                    (value:number)=>{
                                        if(currentSession?.appUser._id===evaluator?._id)
                                            detail.evaluatorAssessment=value;
                                        else
                                            detail.employeeAssessment=value
                                        setDetails([...details]);
                                    }
                                }
                                half={false}
                            />
                        </Col>
                        <Col xs={1}>{responsibility.isHomeOffice?"SI":"NO"}</Col>
                        <Col xs={4}><Form.Control disabled={bussy || readOnly} as="textarea" value={currentSession?.appUser._id===evaluator?._id?detail.evaluatorComments:detail.employeeComments} onChange={e=>{
                            if(currentSession?.appUser._id===evaluator?._id)
                                detail.evaluatorComments=e.target.value;
                            else
                                detail.employeeComments=e.target.value;

                            setDetails([...details]);
                        }} /></Col>
                    </Form.Row>
                ))}
            </Container>
            <Container fluid className="skills-evaluation">
                <Form.Row >
                    <Col xs={2}>Competencia</Col>
                    <Col xs={3}>Referencia</Col>
                    <Col xs={1}>Eje</Col>
                    <Col xs={1}>Calificación</Col>
                    <Col xs={1}>Remoto</Col>
                    <Col xs={4}>Comentarios</Col>
                </Form.Row>
                {loading && (<Form.Row className="justify-content-center">
                    <Spinner animation="border" variant="info"/>
                </Form.Row>)}
                {details.filter(d=>!d.res.appJobPositionID).map(({det:detail, res:responsibility},index)=>(
                    <Form.Row key={index} >
                        <Col xs={2}>{detail.skill}</Col>
                        <Col xs={3}>{detail.reference}</Col>
                        <Col xs={1}><Form.Control disabled={bussy || readOnly} type="text" name="axis" value={detail.axis} onChange={e=>{detail.axis=e.target.value; setDetails([...details]);}} /></Col>
                        <Col xs={1}>                            
                            <ReactStars 
                                value={currentSession?.appUser._id===evaluator?._id?detail.evaluatorAssessment:detail.employeeAssessment} 
                                edit={!bussy && !readOnly}
                                color1="lightgray"
                                color2="seagreen"
                                onChange={
                                    (value:number)=>{
                                        if(currentSession?.appUser._id===evaluator?._id)
                                            detail.evaluatorAssessment=value;
                                        else
                                            detail.employeeAssessment=value
                                        setDetails([...details]);
                                    }
                                }
                                half={false}
                            />
                        </Col>
                        <Col xs={1}>{responsibility.isHomeOffice?"SI":"NO"}</Col>
                        <Col xs={4}><Form.Control disabled={bussy || readOnly} as="textarea" value={currentSession?.appUser._id===evaluator?._id?detail.evaluatorComments:detail.employeeComments} onChange={e=>{
                            if(currentSession?.appUser._id===evaluator?._id)
                                detail.evaluatorComments=e.target.value;
                            else
                                detail.employeeComments=e.target.value;

                            setDetails([...details]);
                        }} /></Col>
                    </Form.Row>
                ))}
            </Container>
            <Container fluid className="employee-comments">
                <Form.Row>
                    <Col xs={2}>Fecha Hora</Col>
                    <Col xs={2}>Autor</Col>
                    <Col xs={8}>Comentario</Col>
                </Form.Row>
                {(employee?.comments??[]).map((comment, index)=>(
                    <Form.Row key={index}>
                        <Col xs={2}>{moment(comment.timestamp).format("YYYY-MM-DD hh:mm:ss")}</Col>
                        <Col xs={2}>{employeeList.find(e=>e._id===comment.author)?.fullName??""}</Col>
                        <Col xs={8}>
                            {comment.link!=="" && <MdAttachment style={{color:"blue", cursor:"pointer"}} onClick={()=>{openCommentLink(comment)}}/>}
                            {comment.comment}
                        </Col>
                    </Form.Row>                    
                ))}
            </Container>
            <Container className="skills-evaluation" hidden={currentSession?.appUser._id!==evaluator?._id && !Roles.CompanyAdmin.isEqual(currentSession?.appUser.role)}>
                <Form.Row>
                    <Col xs={3}>Resultado de evaluación</Col>
                    <Col xs={9}>Comentario de evaluación</Col>
                </Form.Row>
                <Form.Row>
                    <Col xs={3}>
                        {readOnly?(
                            <Form.Label>{completePerformance!.result?.description??"Not Evaluated"}</Form.Label>
                        ):(                            
                            <Select<{label:string, value:AppPerformanceReviewResult}> className="react-select"
                                disable={bussy}
                                options={
                                    AppPerformanceReviewResult.getAll()
                                    .map<{label:string, value:AppPerformanceReviewResult}>(perRevRes=>({label:perRevRes.description, value:perRevRes}))
                                }
                                onChange={option=>{
                                    const {value}=option as {label:string, value:AppPerformanceReviewResult}
                                    completePerformance!.result=value
                                }}
                                defaultValue={completePerformance?.result?{label:completePerformance.result.description, value:completePerformance.result}:undefined}
                                menuPlacement="top"
                            />
                            
                        )}
                    </Col>
                    <Col xs={9}><Form.Control as="textarea" disabled={bussy || readOnly} ref={resultCommentRef}/></Col>
                </Form.Row>
            </Container>
            <Container className="footer">
                <ConfirmResultSave ref={confirmRef} />
                <Form.Row>
                    {readOnly?(
                        <Col><Button variant="secondary" onClick={()=>setCompletePerformance!(null)}>Cerrar</Button></Col>
                    ):(
                        <Col>
                            <Button variant="success" disabled={bussy} onClick={saveChanges}>{bussy && (<Spinner size="sm" variant="info" animation="border"/>)}Guardar</Button>
                            <Button variant="danger" disabled={bussy} onClick={()=>setCompletePerformance!(null)}>Cancelar</Button>
                        </Col>
                    )}
                </Form.Row>
            </Container>
        </Container>
    )
}

import React, { FC }                    from 'react'
import { Row, Col, Badge }              from 'react-bootstrap'
import { TiEdit, TiThList, TiDelete }   from 'react-icons/ti'
import moment                           from 'moment'

import { AppPerformanceReview }     from 'entities/appPerformanceReview'
import { Roles }                    from 'entities/appUser'
import { usePerformanceContext }    from 'hooks/usePerformancesContext'
import { useSessionContext }        from 'hooks/useSessionContext'

export const Performance:FC<{performance:AppPerformanceReview}> = ({performance})=>{
    const currentSession=useSessionContext()
    const {
        companyEmployeeList, 
        companyJobPositionList,
        processing, 
        deletePerformance,
        editPerformance,
        setCompletePerformance
    } = usePerformanceContext()
    return (
        <Row hidden={!companyEmployeeList.some(employee=>employee._id===performance.employeeID) || !companyJobPositionList.some(jobPosition=>jobPosition._id===performance.jobPositionID)}>
            <Col xs={4} md={3} xl={2}>{companyEmployeeList.find(employee=>employee._id===performance.employeeID)?.fullName??"Undefined"}</Col>
            <Col xs={2}>{companyJobPositionList.find(jobPosition=>jobPosition._id===performance.jobPositionID)?.name??"Undefined"}</Col>
            <Col lg={2} className="d-none d-lg-block">{performance.result?.description}</Col>
            <Col md={2} lg={1} className="d-none d-md-block" >{moment(performance.date).format('YYYY-MM-DD')}</Col>
            <Col xl={2} className="d-none d-xl-block">{performance.period}</Col>
            <Col xs={6} md={5} lg={4} xl={3} >
                {(performance.evaluatorID===currentSession?.appUser._id || Roles.CompanyAdmin.isEqual(currentSession?.appUser.role) || Roles.SuperAdmin.isEqual(currentSession?.appUser.role)) && (<>
                    <Badge pill variant="primary" className={processing?"disabled":""} onClick={()=>setCompletePerformance!(performance)}>
                        <TiThList/>{performance.result?.id!==0?"Ver":"Completar"}
                    </Badge>
                </>)}
                {!Roles.Employee.isEqual(currentSession?.appUser.role) && (<>
                    <Badge pill variant="secondary" hidden={performance.result?.id!==0} className={processing?"disabled":""} onClick={()=>editPerformance!(performance)}>
                        <TiEdit/>Editar
                    </Badge>
                    <Badge pill variant="danger" className={processing?"disabled":""} onClick={()=>deletePerformance!(performance)}>
                        <TiDelete/>Borrar
                    </Badge>
                </>)}
            </Col>
        </Row>
    )
}
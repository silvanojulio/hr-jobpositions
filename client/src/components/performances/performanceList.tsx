import React, { FC }                        from 'react'
import { Form, Col, Container, Row, Badge } from 'react-bootstrap'
import { TiDocumentAdd }                    from 'react-icons/ti'

import { usePerformanceContext }    from 'hooks/usePerformancesContext'
import { useSessionContext }        from 'hooks/useSessionContext'
import { Performance }              from './performance'
import { PerformanceCompleteForm  } from './performanceComplete'
import { AppPerformanceReview }     from 'entities/appPerformanceReview'
import { Roles }                    from 'entities/appUser'

export const PerformanceList:FC = ()=>{
    const {  
        currentCompany, 
        processing, 
        performanceList,
        completePerformance, 
        addPerformance
    } = usePerformanceContext()
    const currentSession=useSessionContext()
    if(completePerformance) return (
        <PerformanceCompleteForm />
    )
    return (
        <Container fluid>
            <Row className="h1">Evaluaciones</Row>
            <Row hidden={!!currentCompany}><Form.Label>Debe seleccionar una companía</Form.Label></Row>
            {!Roles.Employee.isEqual(currentSession?.appUser.role??null) && (<Row>
                <Col>
                    <Badge pill variant="success" className={processing?"disabled":""} hidden={currentCompany===null} onClick={addPerformance}>
                        <TiDocumentAdd />Agregar
                    </Badge>
                </Col>
            </Row>)}
            
            <Container hidden={currentCompany===null} fluid className="grid-striped">
                <Row className="grid-head">
                    <Col xs={4} md={3} xl={2}>Colaborador</Col>
                    <Col xs={2}>Posición</Col>
                    <Col lg={2} className="d-none d-lg-block">Resultado</Col>
                    <Col md={2} lg={1} className="d-none d-md-block" >Fecha</Col>
                    <Col xl={2} className="d-none d-xl-block">Período</Col>
                    <Col></Col> 
                </Row>
                {performanceList.map((performance:AppPerformanceReview)=>( 
                    <Performance key={performance._id} performance={performance} />
                ))}
            </Container>   
        </Container>
    )

}
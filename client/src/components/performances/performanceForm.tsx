import React, { FC, useState, useEffect, useCallback } from 'react'
import { usePerformanceContext } from 'hooks/usePerformancesContext'
import { Modal, Form, Col, Button, Spinner } from 'react-bootstrap'
import Select from 'react-select'
import DatePicker from 'react-datepicker'
import moment from 'moment'


export const PerformanceForm:FC = ()=>{
    const [show, setShow] = useState<boolean>(false)
    const [employee, setEmployee] = useState<any>(null)
    const [evaluator, setEvaluator] = useState<any>(null)
    const [jobPosition, setJobPosition] = useState<any>(null)
    const [period, setPeriod] = useState<string>("")
    const [date, setDate] = useState<Date>(new Date())
    const { 
        currentPerformance, companyEmployeeList, companyJobPositionList, processing, saveErrors,
        setCurrentPerformance, savePerformance
    } = usePerformanceContext()

    const handleClose=useCallback(()=>{
        setCurrentPerformance && setCurrentPerformance(null)
    },[setCurrentPerformance])
    useEffect(()=>{
        setShow(currentPerformance!==null) 
        if(!currentPerformance) return
        setDate(moment(currentPerformance.date).toDate())
        setPeriod(currentPerformance.period)
        setEvaluator({value:currentPerformance.evaluatorID, label:companyEmployeeList.find(e=>e._id===currentPerformance.evaluatorID)?.fullName??""})
        setEmployee({value:currentPerformance.employeeID, label:companyEmployeeList.find(e=>e._id===currentPerformance.employeeID)?.fullName??""})
        setJobPosition({value:currentPerformance.jobPositionID, label:companyJobPositionList.find(j=>j._id===currentPerformance.jobPositionID)?.name??""})
    },[currentPerformance,companyEmployeeList,companyJobPositionList])
    useEffect(()=>{
        if(!currentPerformance) return
        currentPerformance.date=date??new Date()
        currentPerformance.period=period
        currentPerformance.jobPositionID=jobPosition?.value??""
        currentPerformance.employeeID=employee?.value??""
        currentPerformance.evaluatorID=evaluator?.value??""
    },[date, jobPosition, employee, evaluator, period, currentPerformance])
    return (
        <Modal show={show} onHide={handleClose} className="performance-modal-form">            
            <Modal.Header >
                <Modal.Title>Evaluación</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Col xs={6}>
                        <Form.Label column className={saveErrors.some(e=>e.param==="employeeID")?"text-danger":""}>Colaborador</Form.Label>
                        <Select isDisabled={processing}
                            options={companyEmployeeList.map(employee=>({
                                label:employee.fullName,
                                value:employee._id
                            }))}
                            value={employee}
                            onChange={(value:any)=>setEmployee(value)}
                        />
                    </Col>
                    <Col xs={6}>
                        <Form.Label column className={saveErrors.some(e=>e.param==="evaluatorID")?"text-danger":""}>Evaluador</Form.Label>
                        <Select isDisabled={processing}
                            options={companyEmployeeList.map(employee=>({
                                label:employee.fullName,
                                value:employee._id
                            }))}
                            value={evaluator}
                            onChange={(value:any)=>setEvaluator(value)}
                        />
                    </Col>
                    <Col xs={3}>
                        <Form.Label column>Fecha</Form.Label>
                        <DatePicker disabled={processing}
                            onChange={(d:Date)=>setDate(d)}
                            selected={date}
                            dateFormat="yyyy-MM-dd" 
                        />
                    </Col>
                    <Col xs={3}>
                        <Form.Label column className={saveErrors.some(e=>e.param==="period")?"text-danger":""}>Período</Form.Label>
                        <Form.Control disabled={processing}
                            type={"text"} 
                            onChange={(ev)=>setPeriod(ev.target.value)}
                            value={period}
                        />
                    </Col>
                    <Col xs={6}>
                        <Form.Label column className={saveErrors.some(e=>e.param==="jobPositionID")?"text-danger":""}>Posición</Form.Label>
                        <Select isDisabled={processing}
                            options={companyJobPositionList.map(jobPosition=>({
                                label:jobPosition.name,
                                value:jobPosition._id
                            }))}
                            value={jobPosition}
                            onChange={(value:any)=>setJobPosition(value)}
                        />
                    </Col>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button 
                    style={{marginRight:"10px"}} 
                    variant="secondary" 
                    disabled={processing} 
                    onClick={handleClose}
                    >Cerrar</Button>
                <Button 
                    style={{marginRight:"10px"}} 
                    variant="primary" 
                    disabled={processing} 
                    onClick={savePerformance}
                    >{processing?(<Spinner animation="border" size="sm"/>):"Guardar"}</Button>
            </Modal.Footer>
        </Modal>
    )
}
import React, { FC }   from 'react'
import { Container, Form }          from 'react-bootstrap'
import { Row, Col, Badge }          from 'react-bootstrap'
import { TiDocumentAdd }            from 'react-icons/ti'
import { useAppContext }            from 'hooks/useAppContext'
import { Document }                 from './document'
import { useDocumentsContext }      from 'hooks'


export const DocumentsList:FC=()=>{
    const { currentCompany } = useAppContext()
    const { documentList, newDocument }    = useDocumentsContext()
    return (
        <Container fluid>
            <Row className="h1">Documentos</Row>
            <Row hidden={!!currentCompany}><Form.Label>Debe seleccionar una companía</Form.Label></Row>
            <Row>
                <Col>
                    <Badge pill variant="success" hidden={!currentCompany} onClick={newDocument}>
                        <TiDocumentAdd />Agregar
                    </Badge>
                </Col>
            </Row>
            <Container hidden={!currentCompany} fluid className="grid-striped">
                <Row className="grid-head">
                    <Col xs={2}>Última actualización</Col>
                    <Col xs={3}>Actualizado por</Col>
                    <Col xs={4}>Título</Col>
                    <Col xs={3}></Col>
                </Row>
                {documentList.map(d=>(<Document key={d._id} document={d}/>))}
            </Container>   
        </Container>
    )
    
}
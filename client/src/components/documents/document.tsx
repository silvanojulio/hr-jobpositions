import React, { FC }                    from 'react'
import { Form, Row, Col, Badge,  }      from 'react-bootstrap'
import { TiEdit, TiDelete }             from 'react-icons/ti'
import moment                           from 'moment'


import { AppDocument }                  from 'entities/appDocuments'
import { useDocumentsContext }          from 'hooks'



export const Document:FC<{document:AppDocument}> = ({document})=>{
    const { delDocument, editDocument } = useDocumentsContext()
    return (  
        <Row >
            <Col xs={2}>{moment(document.last_update).format("YYYY-MM-DD")}</Col>
            <Col xs={3}>{document.responsableFullName}</Col>
            <Col xs={4}>
                <Form.Label as="a" href={document.link} target="_blank" >{document.title}</Form.Label>
            </Col>
            <Col xs={3} dir="rtl">
                <Badge pill variant="danger" onClick={()=>delDocument(document)} >
                    <TiDelete/>Borrar
                </Badge>
                <Badge pill variant="secondary" onClick={()=>editDocument(document)} >
                    <TiEdit/>Editar
                </Badge>
            </Col>
        </Row>
    )
}
import React, { FC, useState } from 'react'
import { Modal, Form, Col, Button, Spinner } from 'react-bootstrap'


import { useDocumentsContext } from 'hooks'

export const DocumentForm:FC = ()=>{
    const { saveDocument, exitDocument, currentDocument} = useDocumentsContext()
    const [bussy, setBussy] = useState<boolean>(false)
    const save=async ()=>{
        setBussy(true)
        await saveDocument().catch(error=>{console.log(error)}).finally(()=>{setBussy(false)})
    }
    return (
        <Modal show={!!currentDocument} onHide={()=>{}}>
            <Modal.Header>
                <Modal.Title>{`${currentDocument?._id?"Editar":"Nuevo"} Documento`}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Col>
                        <Form.Label column>Title</Form.Label>
                        <Form.Control type="text" name="doc_title" 
                            disabled={bussy}
                            defaultValue={currentDocument?.title??""} 
                            onChange={e=>{if(!!currentDocument) currentDocument.title=e.target.value}}
                        />
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col>
                        <Form.Label column>Link</Form.Label>
                        <Form.Control type="text" name="doc_link" 
                            disabled={bussy}
                            defaultValue={currentDocument?.link??""} 
                            onChange={e=>{if(!!currentDocument) currentDocument.link=e.target.value}}
                        />
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col className="d-flex ml-3 mt-2">
                        <Form.Label className="mr-3">Global</Form.Label>
                        <Form.Switch
                            id="global-doc"
                            defaultChecked={currentDocument?.global??false}
                            onChange={e=>{if(currentDocument) currentDocument.global=e.currentTarget.checked}}
                        />
                    </Col>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" disabled={bussy} onClick={exitDocument}>Cancel</Button>
                <Button variant="primary" disabled={bussy} onClick={save} >
                    {bussy?(<Spinner animation="border" size="sm"/>):"Guardar"}
                </Button>
            </Modal.Footer>
        </Modal>
    )
}
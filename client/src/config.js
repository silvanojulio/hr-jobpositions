const config = {
    getApiUrl: () => {
        return window.location.port==="8080" || window.location.port === ""?
        window.location.origin:
        "http://localhost:8080"
    }
}

export default config;
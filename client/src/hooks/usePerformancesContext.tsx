import { useContext } from 'react'
import { PerformanceContext } from 'context/performanceContext'


export const usePerformanceContext = ()=>useContext(PerformanceContext)
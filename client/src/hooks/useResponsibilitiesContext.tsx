import {useContext} from 'react'
import { ResponsibilitiesContext } from 'context/responsibilitiesContext'

export const useResponsibilitiesContext=()=>useContext(ResponsibilitiesContext)

import {useContext} from 'react'
import {DocumentsContext} from 'context/documentsContext'

export const useDocumentsContext=()=>useContext(DocumentsContext)
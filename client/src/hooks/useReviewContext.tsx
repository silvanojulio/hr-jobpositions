import { useContext } from 'react'
import { ReviewContext } from 'context/reviewContext'

export const useReviewContext = ()=>useContext(ReviewContext)
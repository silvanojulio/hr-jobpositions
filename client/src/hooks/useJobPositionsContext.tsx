import {useContext} from 'react'
import { JobPositionsContext } from 'context/jobPositionsContext'

export const useJobPositionsContext=()=>useContext(JobPositionsContext)

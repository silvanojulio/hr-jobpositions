import { useContext }   from 'react'
import { HomeContext }  from 'context/homeContext'

export const useHomeContext=()=>useContext(HomeContext)
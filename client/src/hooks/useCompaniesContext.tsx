import {useContext} from 'react'
import {CompanyContext} from 'context/companiesContext'

export const useCompaniesContext=()=>useContext(CompanyContext)
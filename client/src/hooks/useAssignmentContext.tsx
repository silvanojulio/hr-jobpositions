import {useContext} from 'react'
import {AssignmentsContext} from 'context/assignmentsContext'

export const useAssignmentsContext=()=>useContext(AssignmentsContext)
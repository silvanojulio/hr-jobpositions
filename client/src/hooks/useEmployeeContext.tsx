import {useContext} from 'react'
import {EmployeeContext} from 'context/employeesContext'

export const useEmployeeContext=()=>useContext(EmployeeContext)
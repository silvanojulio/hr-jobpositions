import {useEffect, useState} from 'react'

export function usePromise<T>(promiseOrFunction:any, defaultValue:T) {
    const [state, setState] = useState<{value:T,error:Error|null,isPending:Boolean}>({ value: defaultValue, error: null, isPending: true })
  
    useEffect(() => {
      const promise = (typeof promiseOrFunction === 'function')
        ? promiseOrFunction()
        : promiseOrFunction
  
      let isSubscribed = true
      promise
        .then((value:T) => isSubscribed ? setState({ value, error: null, isPending: false }) : null)
        .catch((error:Error) => isSubscribed ? setState({ value: defaultValue, error: error, isPending: false }) : null)
  
        return () => {(isSubscribed = false)}
    }, [promiseOrFunction, defaultValue])
  
    const { value, error, isPending } = state
    return [value, error, isPending]
  }
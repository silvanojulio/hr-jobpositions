import { useContext } from 'react'
import { SessionContext } from 'context/sessionContext'

export const useSessionContext=()=>useContext(SessionContext)
import React, { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback }            from 'react'
import { Modal, Form, Col, Button}          from 'react-bootstrap'
import { Dropdown, Spinner }                from 'react-bootstrap'
import Select                               from 'react-select'

import { useAppContext }        from 'hooks'
import { PromiseCallbacks }     from 'types'
import { AppUser, Roles }       from 'entities/appUser'
import { DocumentSelectOption } from 'entities/appDocuments'
import { AppError }             from 'utils/apiClient'
import { EmployeeService }      from 'services'



type ValidDataType={
    fullName?:Boolean, 
    email?:Boolean,
}
type SchemaError={value:string, msg: string, param: string, location: string}

export interface IEmployeeForm{
    editar:(employee:AppUser)=>Promise<AppUser>
}

export const EmployeeForm = forwardRef<IEmployeeForm>((_,ref)=>{
    const { currentCompanyDocs }        = useAppContext()
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<AppUser>>()
    const [ employee, setEmployee ]     = useState<AppUser>()
    const [ bussy, setBussy ]           = useState<boolean>(false)
    const [ error, setError ]           = useState<AppError>()    
    const [ validData, setValidData ]   = useState<ValidDataType>({fullName:true,email:true})


    const editar = useCallback(async(employee:AppUser)=>{
        setBussy(true)
        const p = new Promise<AppUser>((resolve, reject)=>{
            setError(undefined)
            setValidData({
                fullName:true,
                email:true,
            })
            setEmployee({...employee})
            setCallbacks({resolve, reject})
        })
        p.finally(()=>{
            setEmployee(undefined)
            setCallbacks(undefined)
        })
        setBussy(false)
        return p
    },[])
    useImperativeHandle(ref, ()=>({editar}),[editar])
    const guardar = useCallback(async()=>{
        if(!callbacks || !employee) return
        try{
            setBussy(true)
            setError(undefined)
            callbacks.resolve(await EmployeeService.saveEmployee(employee))
        }catch(error){
            setError(error as AppError)                
            switch(error?.code){
                case 800:
                    setValidData({
                        fullName:!error.extradata.errors.some((err:SchemaError)=>err.param==="fullName"),
                        email:!error.extradata.errors.some((err:SchemaError)=>err.param==="email")
                    })
                    break
                case 801:
                    setValidData({
                        email:!error.extradata.newUserEmail,
                        fullName:!error.extradata.fullName
                    })
                    break
                default:console.log(error)
            }
        }finally{
            setBussy(false)
        }
    },[callbacks, employee])
    return (
        <Modal show={!!callbacks}>
            <Modal.Header><Modal.Title>{employee?._id?'Editar':'Agregar'} Colaborador</Modal.Title></Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Row className="align-items-center mb-2">
                        <Col sm={3}>
                            <Form.Label column className={!validData.fullName?"text-danger":""}>Nombre</Form.Label>
                        </Col>
                        <Col sm={9}>
                            <Form.Control 
                                disabled={bussy} 
                                className={!validData.fullName?"border-danger":""}
                                defaultValue={employee?.fullName??""}
                                onChange={ev=>{console.log(ev)}}
                            />
                        </Col>
                    </Form.Row> 
                    <Form.Row className="align-items-center mb-2">
                        <Col sm={3}>
                            <Form.Label column className={!validData.email?"text-danger":""}>Email</Form.Label>
                        </Col>
                        <Col sm={9}>
                            <Form.Control 
                                disabled={bussy}
                                className={!validData.email?"border-danger":""} 
                                defaultValue={employee?.email??""}
                            />
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col sm={3}>
                            <Form.Label column>Rol</Form.Label>
                        </Col>
                        <Col sm={9}>                            
                            <Dropdown>
                                <Dropdown.Toggle disabled={bussy} variant="info" size="sm" id="dropdown-basic">
                                    {employee?.role?.description??"Select Rol"}
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    {Roles.getAll().filter((role:Roles)=>!Roles.SuperAdmin.isEqual(role)).map((role:Roles)=>{
                                        return (                        
                                            <Dropdown.Item key={role.id} onClick={()=>{
                                                setEmployee({...employee, ...{role:role}} as AppUser)
                                            }} >{role.description}</Dropdown.Item>
                                        )
                                    })}
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                    </Form.Row>
                    <Form.Row className="align-items-center">
                        <Col sm={3}>
                            <Form.Label column>Activo</Form.Label>
                        </Col>
                        <Col sm={9}>
                            <Form.Switch id="ativeSwitch" label=""
                                defaultChecked={employee?.active??false}
                                onChange={()=>{
                                    if(!employee) return
                                    employee.active=!employee.active
                                }}
                                disabled={bussy}   />
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col xs={3}>
                            <Form.Label column>Documentos</Form.Label>
                        </Col>
                        <Col xs={9}>
                            <Select<DocumentSelectOption> placeholder="Documentos relacionados" isMulti
                                isDisabled={bussy}
                                defaultValue={
                                    currentCompanyDocs
                                    .filter(doc=>employee?.documentIDs.some(id=>id===doc._id))
                                    .map(doc=>({doc,value:doc._id, label:doc.title}))
                                }
                                options={
                                    currentCompanyDocs.map(doc=>({doc,value:doc._id, label:doc.title}))
                                }
                                onChange={(options)=>{
                                    if(!employee)return 
                                    if(!options) {
                                        employee.documentIDs=[]
                                        return
                                    }
                                    employee.documentIDs=(options as DocumentSelectOption[]).map(o=>o.value)
                                }}
                            />
                        </Col>
                    </Form.Row>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                {error && <Form.Label>{error.message}</Form.Label>}
                <Button disabled={bussy}
                    variant="secondary" 
                    onClick={()=>callbacks!.reject()}
                    >Cerrar</Button>
                <Button disabled={bussy}
                    variant="primary" 
                    onClick={guardar}
                    >{bussy?(<Spinner animation="border" size="sm"/>):"Guardar"}</Button>
            </Modal.Footer>
        </Modal>
    )
    
})
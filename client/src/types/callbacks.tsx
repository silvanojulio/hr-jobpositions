export type PromiseCallbacks<T> = {
    resolve:(value: T | PromiseLike<T>) => void
    reject: (reason?: any) => void
}
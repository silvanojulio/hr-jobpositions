import React, {FC} from 'react';
import { BrowserRouter } from 'react-router-dom';
import AppRoutes from './pages/appRoutes';
import './App.scss';

const App : FC = () => {
  
  return (
      <BrowserRouter basename={'/'}>
          <AppRoutes />
      </BrowserRouter>
  );
}

export default App;

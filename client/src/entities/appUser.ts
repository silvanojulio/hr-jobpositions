import { AppCompany } from "./appCompany";
import { EnumEntityBase } from "./appEnumEntityBase";

export class AppUser {
    constructor(
        public _id:string,
        public fullName: string, 
        public email: string,
        public active: boolean,
        public company: AppCompany|null, 
        public role:Roles|null,
        public documentIDs:string[]=[],
        public comments:Comment[]=[]
    ){}
}
export class Comment{
    constructor(
        public timestamp:number,
        public comment:string,
        public link:string,
        public author:string
    ){}
}


export class Roles extends EnumEntityBase{
    public static SuperAdmin= new Roles(1, "Super Admin");
    public static CompanyAdmin= new Roles(2, "Company Administrator");
    public static Employee = new Roles(3, "Employee");

    public static getAll = ():Array<Roles> => [Roles.SuperAdmin, Roles.CompanyAdmin, Roles.Employee];
    public static getById = (_id:number):Roles|null => Roles.getAll().find(x=>x.id === _id)??null;
    public isEqual = (rol:Roles|null|undefined):boolean => rol!==null && rol!==undefined && rol.id===this.id;
}
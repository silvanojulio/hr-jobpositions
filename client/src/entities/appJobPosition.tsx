
export class AppJobPosition {
    constructor(
        public _id:string,
        public name:string, 
        public process:string,
        public companyID:string,
        public managerID:string,
        public helperID:string,
    ){}
}
export class AppDocument {
    constructor(
        public _id:string,
        public link:string,
        public title:string,
        public last_update:Date,
        public responsableFullName:string,
        public companyID:string,
        public global:boolean
    ){}
}
export type DocumentSelectOption={
    doc:AppDocument, 
    value:string, 
    label:string
}
import { IEntityBase } from 'entities/entityList'

export class AppAssignment implements IEntityBase{
    constructor(
        public employeeID:string,
        public active:Boolean=true,
        public _id:string="",
        public assignmentDate:string=new Date().toISOString().split('T')[0],
        public jobPositionID?:string,
        public suggestedReviewDate?:string,
        public employeeEvaluatorID?:string,
        public inactiveDate?:Date,
    ){}
}
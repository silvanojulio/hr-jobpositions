export class AppPerformanceReviewDetail{
    constructor(
        public _id:string,
        public performanceReviewID:string,
        public weighing:number,
        public skill:string,
        public reference:string,
        public axis:string,
        public employeeAssessment:number,
        public employeeComments:string,
        public evaluatorAssessment:number,
        public evaluatorComments:string,
        public responsibilityID:string
    ){}
}
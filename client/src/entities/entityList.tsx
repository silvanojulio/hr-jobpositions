export interface IEntityBase {
    _id:string
}
export class EntityListw<T extends IEntityBase> extends Array<T>{
    public updateOnList(item:T):Array<T>{
        const index=this.findIndex(_item=>_item._id===item._id)
        if(index>=0) this[index]=item; else this.push(item);
        return this
    }
    public removeFromList(item:T):T|null{
        const index=this.findIndex(_item=>_item._id===item._id)
        return index>=0?this.splice(index,1)[0]:null
    }
}
import { AppUser } from './appUser'
export class AppSession {
    constructor(
        public appUser: AppUser, 
        public securityToken:string,
        public expiration:string
        ){}
}
